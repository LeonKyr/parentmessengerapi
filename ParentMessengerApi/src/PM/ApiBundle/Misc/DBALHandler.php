<?php

namespace PM\ApiBundle\Misc;

use Doctrine\DBAL\Connection;
use Monolog\Handler\AbstractProcessingHandler;
use Monolog\Logger;

class DBALHandler extends AbstractProcessingHandler
{
    private $initialized = false;
    private $conn;
    private $statement;

    /**
     * @param \Doctrine\DBAL\Connection $dbal
     * @param bool|int $level
     * @param bool $bubble
     */
    public function __construct(Connection $dbal, $level = Logger::DEBUG, $bubble = true)
    {
        $this->conn = $dbal;
        parent::__construct($level, $bubble);
    }

    /**
     * @param array $record
     */
    protected function write(array $record)
    {
        if (!$this->initialized)
        {
            $this->initialize();
        }

        $hostName = array_key_exists('SERVER_NAME', $_SERVER) ? $_SERVER['SERVER_NAME'] : gethostname();
        $hostPort = array_key_exists('SERVER_PORT', $_SERVER) ? $_SERVER['SERVER_PORT'] : '';

        $debug = debug_backtrace();
        $this->statement->execute(
            array(
                'channel' => $record['channel'],
                'level' => $record['level'],
                'levelname' => $record['level_name'],
                'host' => $hostName,
                'port' => $hostPort,
                'file' => $debug[3]['file'] . ':' . $debug[3]['line'],
                'message' => $record['message'],
                'data' => (!empty($record['context']) ? json_encode($record['context']) : null),
                'time' => $record['datetime']->format('Y-m-d h:i:s'),
            )
        );
    }

    /**
     *
     */
    private function initialize()
    {
        $this->conn->prepare(
            'CREATE TABLE IF NOT EXISTS log ('
            . 'id INTEGER UNSIGNED NOT NULL PRIMARY KEY AUTO_INCREMENT, '
            . 'channel VARCHAR(255), '
            . 'level INTEGER, '
            . 'levelname VARCHAR(9), '
            . 'message TEXT, '
            . 'data LONGTEXT, '
            . 'file VARCHAR(500), '
            . 'host VARCHAR(50), '
            . 'port VARCHAR(4), '
            . 'time DATETIME '
            . ')'
        )->execute();

        $this->statement = $this->conn->prepare(
            'INSERT INTO log (channel, level, levelname, message, data, file, host, port, time) VALUES (:channel, :level, :levelname, :message, :data, :file, :host, :port, :time)'
        );

        $this->initialized = true;
    }
}