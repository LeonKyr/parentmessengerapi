<?php

namespace PM\ApiBundle\Entity\Factory;

use PM\ApiBundle\Entity\Kindergarten;

interface IKindergartenFactory
{
    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @param string $externalId
     * @param string $phone
     * @param string $contactPerson
     * @param string $address
     * @return Kindergarten
     */
    public function create($name, $email, $password, $externalId, $phone, $contactPerson, $address);
}