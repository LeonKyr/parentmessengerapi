<?php
namespace PM\ApiBundle\Entity\Factory;

use PM\ApiBundle\Entity\Kindergarten;

final class KindergartenFactory
    implements IKindergartenFactory
{
    public function create($name, $email, $password, $externalId, $phone, $contactPerson, $address)
    {
        return new Kindergarten($name, $email, $password, $externalId, $phone, $contactPerson, $address);
    }
}