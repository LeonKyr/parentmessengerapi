<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;
/**
 * ActionPropertyValue
 *
 * @ORM\Entity
 * @ORM\Table(name="action_property_type")
 */
class ActionPropertyType implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_selection", type="boolean")
     */
    private $isSelection;

    function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
        $this->isSelection = false;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return boolean
     */
    public function getIsSelection()
    {
        return $this->isSelection;
    }

    public function jsonSerialize()
    {
        return $this->getName();
    }
}