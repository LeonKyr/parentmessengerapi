<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activity2Child
 *
 * @ORM\Entity
 * @ORM\Table(name="activity2child")
 */
class Activity2Child
{
    /**
     * @var integer
     *
     * @ORM\Column(name="activity_id", type="integer")
     * @ORM\Id
     */
    private $activityId;
    /**
     * @var integer
     *
     * @ORM\Column(name="child_id", type="integer")
     * @ORM\Id
     */
    private $childId;
    /**
     * @var Child
     *
     * @ORM\ManyToOne(targetEntity="Child")
     * @ORM\JoinColumn(name="child_id", referencedColumnName="id")
     */
    private $child;

    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="integer")
     */
    private $createdAt;
    /**
     * @var int
     *
     * @ORM\Column(name="updated_at", type="integer")
     */
    private $updatedAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    function __construct()
    {
        $this->setIsActive(true);
    }

    /**
     * @param int $childId
     */
    public function setChildId($childId)
    {
        $this->childId = $childId;
    }

    /**
     * @return int
     */
    public function getChildId()
    {
        return $this->childId;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $activityId
     */
    public function setActivityId($activityId)
    {
        $this->activityId = $activityId;
    }

    /**
     * @return int
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @return \PM\ApiBundle\Entity\Child
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * @param \PM\ApiBundle\Entity\Child $child
     */
    public function setChild($child)
    {
        $this->child = $child;
        if ($child != null)
        {
            $this->setChildId($child->getId());
        }
    }

    function __toString()
    {
        return 'ActivityId=' . $this->getActivityId() . ',ChildId=' . $this->getChildId();
    }
}