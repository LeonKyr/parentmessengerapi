<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Event2Child
 *
 * @ORM\Entity
 * @ORM\Table(name="event2child")
 */
class Event2Child
{
    /**
     * @var integer
     *
     * @ORM\Column(name="event_id", type="integer")
     * @ORM\Id
     */
    private $eventId;
    /**
     * @var integer
     *
     * @ORM\Column(name="child_id", type="integer")
     * @ORM\Id
     */
    private $childId;

    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="integer")
     */
    private $createdAt;

    /**
     * @param int $childId
     */
    public function setChildId($childId)
    {
        $this->childId = $childId;
    }

    /**
     * @return int
     */
    public function getChildId()
    {
        return $this->childId;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $eventId
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
    }

    /**
     * @return int
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    function __toString()
    {
        return 'EventId='.$this->getEventId().',ChildId='.$this->getChildId();
    }
}