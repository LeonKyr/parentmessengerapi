<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Platform;

interface IPlatformRepository
{
    /**
     * @param Platform $platform
     */
    public function save(Platform $platform);

    /**
     * @param string $name
     * @return Platform
     */
    public function findByName($name);
}