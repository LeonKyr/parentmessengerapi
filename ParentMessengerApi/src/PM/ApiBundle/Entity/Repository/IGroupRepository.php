<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Group;

interface IGroupRepository
{
    /**
     * @param Group $group
     */
    public function save(Group $group);

    /**
     * @param string $externalId
     * @return Group
     */
    public function findByExternalId($externalId);

    /**
     * @param int $kindergartenId
     * @param int $deviceId
     * @param $updatedAt
     * @return Group[]
     */
    public function findByKindergartenIdWithDifferentDeviceIdAndUpdatedAt($kindergartenId, $deviceId, $updatedAt);
}