<?php
namespace PM\ApiBundle\Entity\Repository;

interface IReportingRepository
{
    public function groupPresentceStatus($time, $groupId);
}