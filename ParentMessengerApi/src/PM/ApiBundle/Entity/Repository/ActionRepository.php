<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Action;

final class ActionRepository
    extends RepositoryBase
    implements IActionRepository
{

    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param Action $action
     */
    public function save(Action $action)
    {
        parent::baseSave($action);
    }

    /**
     * @param string $name
     * @return Action
     */
    public function findByName($name)
    {
        $query = $this->getEntityManager()->createQuery(
            'select a
            from PMApiBundle:Action a
            where a.name = :name
            '
        )
            ->setParameter('name', $name);

        $result = $query->getOneOrNullResult();

        return $result;
    }
}