<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Teacher;

interface ITeacherRepository
{
    /**
     * @param Teacher $teacher
     */
    public function save(Teacher $teacher);

    /**
     * @param string $externalId
     * @return Teacher
     */
    public function findByExternalId($externalId);

    /**
     * @param int $kindergartenId
     * @param int $deviceId
     * @param $updatedAt
     * @return Teacher[]
     */
    public function findByKindergartenIdWithDifferentDeviceIdAndUpdatedAt($kindergartenId, $deviceId, $updatedAt);
}
