<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\ActionProperty;

class ActionPropertyRepository
    extends RepositoryBase
    implements IActionPropertyRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param int $id
     * @return ActionProperty
     */
    public function findById($id)
    {
        $query = $this->getEntityManager()->createQuery(
            'select ep
            from PMApiBundle:ActionProperty ep
            where ep.id = :eventPropertyId
            '
        )
            ->setParameter('eventPropertyId', $id);

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param $name
     * @return ActionProperty
     */
    public function findByName($name)
    {
        $query = $this->getEntityManager()->createQuery(
            'select ep
            from PMApiBundle:ActionProperty ep
            where ep.name = :name
            '
        )
            ->setParameter('name', $name);

        $result = $query->getOneOrNullResult();

        return $result;
    }
}