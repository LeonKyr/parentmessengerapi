<?php

namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Child;
use PM\ApiBundle\Entity\Event2Child;
use PM\ApiBundle\Entity\Kindergarten;

final class KindergartenRepository
    extends RepositoryBase
    implements IKindergartenRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param Kindergarten $kindergarten
     */
    public function save(Kindergarten $kindergarten)
    {
        parent::baseSave($kindergarten);
    }

    /**
     * @param int $id
     * @return Kindergarten
     */
    public function findById($id)
    {
        $query = $this->getEntityManager()->createQuery(
            'select k
            from PMApiBundle:Kindergarten k
            where k.id = :id
            '
        )
            ->setParameter('id', $id)
        ;

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param string $externalId
     * @return Kindergarten
     */
    public function findByExternalId($externalId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select k
            from PMApiBundle:Kindergarten k
            where k.externalId = :externalId
            '
        )
            ->setParameter('externalId', $externalId)
        ;

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param string $email
     * @param string $password
     * @return Kindergarten
     */
    public function findByEmailAndPassword($email, $password)
    {
        $query = $this->getEntityManager()->createQuery(
            'select k
            from PMApiBundle:Kindergarten k
            where k.email = :email
              and k.password = :password
            '
        )
            ->setParameter('email', $email)
            ->setParameter('password', $password)
        ;

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param string $email
     * @return Kindergarten
     */
    public function findByEmail($email)
    {
        $query = $this->getEntityManager()->createQuery(
            'select k
            from PMApiBundle:Kindergarten k
            where k.email = :email
            '
        )
            ->setParameter('email', $email)
        ;

        $result = $query->getOneOrNullResult();

        return $result;
    }
}


