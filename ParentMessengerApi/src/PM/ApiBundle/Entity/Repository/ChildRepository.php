<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Child;


class ChildRepository
    extends RepositoryBase
    implements IChildRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param Child $child
     */
    public function save(Child $child)
    {
        parent::baseSave($child);
    }

    /**
     * @param string $externalId
     * @return Child
     */
    public function findByExternalId($externalId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select c
            from PMApiBundle:Child c
            where c.externalId = :externalId
            '
        )
            ->setParameter('externalId', $externalId);

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param string $code
     * @return Child
     */
    public function findByCode($code)
    {
        $query = $this->getEntityManager()->createQuery(
            'select c
            from PMApiBundle:Child c
            where c.code = :code
            '
        )
            ->setParameter('code', $code);

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param int $groupId
     * @return Child[]
     */
    public function findByGroupId($groupId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select c
            from PMApiBundle:Child c
            where c.groupId = :groupId
            '
        )
            ->setParameter('groupId', $groupId);

        $result = $query->execute();

        return $result;
    }

    /**
     * @param int $kindergartenId
     * @param int $deviceId
     * @param int $updatedAt
     * @return Child[]
     */
    public function findByKindergartenIdWithDifferentDeviceIdAndUpdatedAt($kindergartenId, $deviceId, $updatedAt)
    {
        $query = $this->getEntityManager()->createQuery(
            'select c
            from PMApiBundle:Child c
            where c.kindergartenId = :kindergartenId
              and c.lastUpdatedByDeviceId != :deviceId
              and c.updatedAt >= :updatedAt
            '
        )
            ->setParameter('kindergartenId', $kindergartenId)
            ->setParameter('deviceId', $deviceId)
            ->setParameter('updatedAt', $updatedAt);

        $result = $query->execute();

        return $result;
    }

    /**
     * @param int $id
     * @return Child
     */
    public function findById($id)
    {
        $query = $this->getEntityManager()->createQuery(
            'select c
            from PMApiBundle:Child c
            where c.id = :id
            '
        )
            ->setParameter('id', $id);

        $result = $query->getOneOrNullResult();

        return $result;
    }
}