<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\EventPropertyValue;
use PM\ApiBundle\Misc\UUID;

class EventPropertyValueRepository
    extends RepositoryBase
    implements IEventPropertyValueRepository
{
    /**
     * @var IEventPropertyValueBagRepository
     */
    private $eventPropertyValueBagRepository;

    function __construct(EntityManager $em,
                         IEventPropertyValueBagRepository $eventPropertyValueBagRepository)
    {
        parent::__construct($em);
        $this->eventPropertyValueBagRepository = $eventPropertyValueBagRepository;
    }

    /**
     * @param EventPropertyValue $value
     * @return void
     */
    public function save(EventPropertyValue $value)
    {
        try
        {
            $this->getEntityManager()->beginTransaction();

            parent::baseSave($value);

            if ($value->getIsBag())
            {
                foreach($value->getEventPropertyValueBags() as $bag)
                {
                    $bag->setEventPropertyValue($value);

                    $this->eventPropertyValueBagRepository->save($bag);
                }
            }
            $this->getEntityManager()->commit();
        }
        catch(\Exception $ex)
        {
            $this->getEntityManager()->rollback();

            echo $ex;
        }
    }

    /**
     * @param int $eventId
     * @return EventPropertyValue[]
     */
    public function findByEventId($eventId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select epv
            from PMApiBundle:EventPropertyValue epv
            where epv.eventId = :eventId
            '
        )
            ->setParameter('eventId', $eventId)
        ;

        $result = $query->getResult();

        /** @var EventPropertyValue $epv */
        foreach ($result as $epv)
        {
            $epvb = $this->eventPropertyValueBagRepository->findByEventPropertyValueId($epv->getId());

            $epv->setEventPropertyValueBags($epvb);
        }

        return $result;
    }
}

