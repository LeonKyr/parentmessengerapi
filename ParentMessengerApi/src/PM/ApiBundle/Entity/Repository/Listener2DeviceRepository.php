<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Child2Listener;
use PM\ApiBundle\Entity\Listener2Device;

class Listener2DeviceRepository
    extends RepositoryBase
    implements IListener2DeviceRepository
{
    public function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param Listener2Device $listener2device
     */
    public function save(Listener2Device $listener2device)
    {
        parent::baseSave($listener2device);
    }

    /**
     * @param string $listenerExternalId
     * @return Listener2Device[]
     */
    public function findByListenerExternalId($listenerExternalId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select l2d
            from PMApiBundle:Listener2Device l2d
            where l2d.listenerId in (
              select l.id
              from PMApiBundle:Listener l
              where l.externalId = :externalId
            )
            '
        )
            ->setParameter('externalId', $listenerExternalId);

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param int $listenerId
     * @return Listener2Device[]
     */
    public function findByListenerId($listenerId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select l2d
            from PMApiBundle:Listener2Device l2d
            where l2d.listenerId = :listenerId
            '
        )
            ->setParameter('listenerId', $listenerId);

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param int $listenerId
     * @param int $deviceId
     * @return Listener2Device[]
     */
    public function findByListenerIdAndDeviceId($listenerId, $deviceId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select l2d
            from PMApiBundle:Listener2Device l2d
            where l2d.listenerId = :listenerId
              and l2d.deviceId = :deviceId
            '
        )
            ->setParameter('listenerId', $listenerId)
            ->setParameter('deviceId', $deviceId);

        $result = $query->getResult();

        return $result;
    }
}