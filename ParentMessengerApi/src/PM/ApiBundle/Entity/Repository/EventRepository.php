<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Event;
use PM\ApiBundle\Entity\Event2Child;
use PM\ApiBundle\Entity\EventPropertyValue;

class EventRepository
    extends RepositoryBase
    implements IEventRepository
{
    /**
     * @var IEventPropertyValueRepository
     */
    private $eventPropertyValueRepository;
    /**
     * @var IEvent2ChildRepository
     */
    private $event2childRepository;

    function __construct(EntityManager $em,
                         IEventPropertyValueRepository $eventPropertyValueRepository,
                         IEvent2ChildRepository $event2childRepository)
    {
        parent::__construct($em);

        $this->eventPropertyValueRepository = $eventPropertyValueRepository;
        $this->event2childRepository = $event2childRepository;
    }

    /**
     * @param Event $event
     * @param EventPropertyValue[] $eventPropertyValues
     * @param Event2Child[] $event2children
     */
    public function save(Event $event, array $eventPropertyValues ,array $event2children)
    {
        try
        {
            $this->getEntityManager()->beginTransaction();

            parent::baseSave($event);

            /** @var EventPropertyValue */
            foreach ($eventPropertyValues as $value)
            {
                $value->setEvent($event);
                $this->eventPropertyValueRepository->save($value);
            }

            /** @var Event2Child */
            foreach ($event2children as $event2child)
            {
                $event2child->setEventId($event->getId());

                $this->event2childRepository->save($event2child);
            }

            $this->getEntityManager()->commit();
        }
        catch(\Exception $ex)
        {
            $this->getEntityManager()->rollback();

            echo $ex;
        }

        $event->setEventPropertyValues($eventPropertyValues);
    }

    /**
     * @param int $childId
     * @param int $createdAt
     * @param int $limit
     * @return Event[]
     */
    public function findByChildIdAndCreatedAt($childId, $createdAt, $limit)
    {
        $query = $this->getEntityManager()->createQuery(
            'select e
            from PMApiBundle:Event e
              join PMApiBundle:Action a
                        with e.actionId = a.id
            where e.id in (select distinct e2c.eventId
            from PMApiBundle:Event2Child e2c
            where e2c.childId = :childId
            order by e2c.eventId desc)
                and e.createdAt >= :createdAt
                and a.isKindergartenOnly = 0
            order by e.id desc
            '
        )
            ->setMaxResults($limit)
            ->setParameter('childId', $childId)
            ->setParameter('createdAt', $createdAt);

        $result = $query->getResult();

        /** @var Event $e */
        foreach ($result as $e)
        {
            $epv = $this->eventPropertyValueRepository->findByEventId($e->getId());

            $e->setEventPropertyValues($epv);
        }

        return $result;
    }

    /**
     * @param int $createdAt
     * @return Device[]
     */
    public function findAllDevicesThanShouldBeNotified($createdAt)
    {
            $query = $this->getEntityManager()->createQuery(
                'select distinct d
                from PMApiBundle:Event e
                    join PMApiBundle:Event2Child e2c
                        with e.id = e2c.eventId
                    join PMApiBundle:Child2Listener c2l
                        with e2c.childId = c2l.childId
                    join PMApiBundle:Listener l
                        with l.id = c2l.listenerId
                    join PMApiBundle:Device d
                        with l.deviceId = d.id
                where e.createdAt >= :createdAt
                '
        )
            ->setParameter('createdAt', $createdAt)
            ;

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param int $eventId
     * @return Device[]
     */
    public function findAllDevicesThanShouldBeNotifiedForEvent($eventId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select distinct d
            from PMApiBundle:Event e
                join PMApiBundle:Event2Child e2c
                    with e.id = e2c.eventId
                join PMApiBundle:Child2Listener c2l
                    with e2c.childId = c2l.childId
                join PMApiBundle:Listener2Device l2d
                    with l2d.listenerId = c2l.listenerId
                join PMApiBundle:Device d
                    with l2d.deviceId = d.id
            where e.id = :eventId
            '
        )
            ->setParameter('eventId', $eventId)
        ;

        $result = $query->getResult();

        return $result;
    }
}

