<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\ActivityType;

interface IActivityTypeRepository
{
    /**
     * @param int $id
     * @return ActivityType
     */
    public function findById($id);
}