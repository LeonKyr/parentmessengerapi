<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Image;

interface IImageRepository
{
    /**
     * @param Image $image
     */
    public function save(Image $image);

    /**
     * @param string $externalId
     * @return Image
     */
    public function findByExternalId($externalId);
}