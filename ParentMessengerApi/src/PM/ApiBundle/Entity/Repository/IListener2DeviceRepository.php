<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Listener2Device;

interface IListener2DeviceRepository
{
    /**
     * @param Listener2Device $listener2device
     */
    public function save(Listener2Device $listener2device);

    /**
     * @param string $listenerExternalId
     * @return Listener2Device[]
     */
    public function findByListenerExternalId($listenerExternalId);

    /**
     * @param int $listenerId
     * @return Listener2Device[]
     */
    public function findByListenerId($listenerId);

    /**
     * @param int $listenerId
     * @param int $deviceId
     * @return Listener2Device[]
     */
    public function findByListenerIdAndDeviceId($listenerId, $deviceId);
}