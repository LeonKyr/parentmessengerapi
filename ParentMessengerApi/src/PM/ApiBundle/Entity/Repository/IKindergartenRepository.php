<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Kindergarten;

interface IKindergartenRepository
{
    /**
     * @param Kindergarten $kindergarten
     */
    public function save(Kindergarten $kindergarten);

    /**
     * @param int $id
     * @return Kindergarten
     */
    public function findById($id);

    /**
     * @param string $externalId
     * @return Kindergarten
     */
    public function findByExternalId($externalId);

    /**
     * @param string $email
     * @param string $password
     * @return Kindergarten
     */
    public function findByEmailAndPassword($email, $password);

    /**
     * @param string $email
     * @return Kindergarten
     */
    public function findByEmail($email);
}


