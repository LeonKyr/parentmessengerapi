<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Teacher;

final class TeacherRepository
    extends RepositoryBase
    implements ITeacherRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param Teacher $teacher
     */
    public function save(Teacher $teacher)
    {
        parent::baseSave($teacher);
    }

    /**
     * @param string $externalId
     * @return Teacher
     */
    public function findByExternalId($externalId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select t
            from PMApiBundle:Teacher t
            where t.externalId = :externalId
            '
        )
            ->setParameter('externalId', $externalId);

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param int $kindergartenId
     * @param int $deviceId
     * @param int $updatedAt
     * @return Teacher[]
     */
    public function findByKindergartenIdWithDifferentDeviceIdAndUpdatedAt($kindergartenId, $deviceId, $updatedAt)
    {
        $query = $this->getEntityManager()->createQuery(
            'select t
            from PMApiBundle:Teacher t
            where t.kindergartenId = :kindergartenId
              and t.lastUpdatedByDeviceId != :deviceId
              and t.updatedAt >= :updatedAt
            '
        )
            ->setParameter('kindergartenId', $kindergartenId)
            ->setParameter('deviceId', $deviceId)
            ->setParameter('updatedAt', $updatedAt);

        $result = $query->execute();

        return $result;
    }
}

