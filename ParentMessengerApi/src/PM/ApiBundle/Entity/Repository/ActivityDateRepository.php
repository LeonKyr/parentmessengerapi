<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\ActivityDate;

class ActivityDateRepository
    extends RepositoryBase
    implements IActivityDateRepository
{
    /**
     * @param ActivityDate $activityDate
     */
    public function save(ActivityDate $activityDate)
    {
        parent::baseSave($activityDate);
    }

    /**
     * @param ActivityDate $activityDate
     */
    public function remove(ActivityDate $activityDate)
    {
        $this->getEntityManager()->remove($activityDate);
    }

    /**
     * @param int $activityId
     * @return ActivityDate[]
     */
    public function findByActivityId($activityId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select ad
            from PMApiBundle:ActivityDate ad
            where ad.activityId = :activityId
            order by ad.id desc
            '
        )
            ->setParameter('activityId', $activityId);

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param string $externalId
     * @return ActivityDate
     */
    public function findByExternalId($externalId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select ad
            from PMApiBundle:ActivityDate ad
            where ad.externalId = :externalId
            '
        )
            ->setParameter('externalId', $externalId);

        $result = $query->getOneOrNullResult();

        return $result;
    }
}