<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Child;

interface IChildRepository
{
    /**
     * @param Child $child
     */
    public function save(Child $child);

    /**
     * @param string $externalId
     * @return Child
     */
    public function findByExternalId($externalId);

    /**
     * @param string $code
     * @return Child
     */
    public function findByCode($code);

    /**
     * @param int $id
     * @return Child
     */
    public function findById($id);

    /**
     * @param int $groupId
     * @return Child[]
     */
    public function findByGroupId($groupId);

    /**
     * @param int $kindergartenId
     * @param int $deviceId
     * @param int $updatedAt
     * @return Child[]
     */
    public function findByKindergartenIdWithDifferentDeviceIdAndUpdatedAt($kindergartenId, $deviceId, $updatedAt);
}

