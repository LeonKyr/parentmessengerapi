<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Image;

final class ImageRepository
    extends RepositoryBase
    implements IImageRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param Image $image
     */
    public function save(Image $image)
    {
        parent::baseSave($image);
    }

    /**
     * @param string $externalId
     * @return Image
     */
    public function findByExternalId($externalId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select i
            from PMApiBundle:Image i
            where i.externalId = :externalId
            '
        )
            ->setParameter('externalId', $externalId);

        $result = $query->getOneOrNullResult();

        return $result;
    }
}