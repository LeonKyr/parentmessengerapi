<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Platform;

final class PlatformRepository
    extends RepositoryBase
    implements IPlatformRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param Platform $platform
     */
    public function save(Platform $platform)
    {
        parent::baseSave($platform);
    }

    /**
     * @param string $name
     * @return Platform
     */
    public function findByName($name)
    {
        $query = $this->getEntityManager()->createQuery(
            'select p
            from PMApiBundle:Platform p
            where p.name = :name
            '
        )
            ->setParameter('name', $name);

        $result = $query->getOneOrNullResult();

        return $result;
    }
}