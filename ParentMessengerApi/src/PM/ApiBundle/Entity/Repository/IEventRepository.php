<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Event;
use PM\ApiBundle\Entity\Device;

interface IEventRepository
{
    /**
     * @param Event $event
     * @param array $eventPropertyValues
     * @param array $children
     * @return
     */
    public function save(Event $event, array $eventPropertyValues, array $children);

    /**
     * @param int $childId
     * @param int $createdAt
     * @param int $limit
     * @return Event[]
     */
    public function findByChildIdAndCreatedAt($childId, $createdAt, $limit);

    /**
     * @param int $createdAt
     * @return Device[]
     */
    public function findAllDevicesThanShouldBeNotified($createdAt);

    /**
     * @param int $eventId
     * @return Device[]
     */
    public function findAllDevicesThanShouldBeNotifiedForEvent($eventId);
}

