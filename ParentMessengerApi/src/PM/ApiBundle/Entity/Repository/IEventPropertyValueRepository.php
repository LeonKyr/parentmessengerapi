<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\EventPropertyValue;

interface IEventPropertyValueRepository
{
    /**
     * @param EventPropertyValue $epv
     * @return void
     */
    public function save(EventPropertyValue $epv);

    /**
     * @param int $eventId
     * @return EventPropertyValue[]
     */
    public function findByEventId($eventId);
}