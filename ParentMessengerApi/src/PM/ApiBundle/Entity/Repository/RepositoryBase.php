<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;

abstract class RepositoryBase
{
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    protected function getEntityManager()
    {
        return $this->em;
    }

    protected function baseSave($object)
    {
        $this->getEntityManager()
            ->persist($object);
        $this->getEntityManager()
            ->flush();
    }
}