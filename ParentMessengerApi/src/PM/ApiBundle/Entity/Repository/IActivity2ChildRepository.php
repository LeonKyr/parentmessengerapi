<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Activity2Child;

interface IActivity2ChildRepository
{
    /**
     * @param Activity2Child $activity2child
     */
    public function save(Activity2Child $activity2child);

    /**
     * @param int $activityId
     * @param int $childId
     * @return Activity2Child
     */
    public function findByActivityIdAndChildId($activityId, $childId);

    /**
     * @param int $activityId
     * @return Activity2Child[]
     */
    public function findByActivityId($activityId);
}