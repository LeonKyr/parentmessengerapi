<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\ActionProperty;

interface IActionPropertyRepository
{
    /**
     * @param int $id
     * @return ActionProperty
     */
    public function findById($id);

    /**
     * @param $name
     * @return ActionProperty
     */
    public function findByName($name);
}