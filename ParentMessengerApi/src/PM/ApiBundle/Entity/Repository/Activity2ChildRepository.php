<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Activity2Child;

class Activity2ChildRepository
    extends RepositoryBase
    implements IActivity2ChildRepository
{
    /**
     * @param Activity2Child $activity2child
     */
    public function save(Activity2Child $activity2child)
    {
        parent::baseSave($activity2child);
    }

    /**
     * @param int $activityId
     * @param int $childId
     * @return Activity2Child
     */
    public function findByActivityIdAndChildId($activityId, $childId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select a2c
            from PMApiBundle:Activity2Child a2c
            where a2c.childId = :childId
              and a2c.activityId = :activityId
            '
        )
            ->setParameter('activityId', $activityId)
            ->setParameter('childId', $childId);

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param int $activityId
     * @return Activity2Child[]
     */
    public function findByActivityId($activityId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select a2c
            from PMApiBundle:Activity2Child a2c
            where a2c.activityId = :activityId
            '
        )
            ->setParameter('activityId', $activityId);

        $result = $query->getResult();

        return $result;
    }
}