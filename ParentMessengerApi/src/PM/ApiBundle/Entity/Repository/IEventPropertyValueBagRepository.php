<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\EventPropertyValueBag;

interface IEventPropertyValueBagRepository
{
    /**
     * @param EventPropertyValueBag $epv
     * @return void
     */
    public function save(EventPropertyValueBag $epv);

    /**
     * @param int $eventPropertyValueId
     * @return EventPropertyValueBag[]
     */
    public function findByEventPropertyValueId($eventPropertyValueId);
}