<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Listener;

interface IListenerRepository
{
    /**
     * @param Listener $listener
     */
    public function save(Listener $listener);

    /**
     * @param int $id
     * @return Listener
     */
    public function findById($id);

    /**
     * @param string $externalId
     * @return Listener
     */
    public function findByExternalId($externalId);

    /**
     * @param string $email
     * @param string $password
     * @return Listener
     */
    public function findByEmailAndPassword($email, $password);

    /**
     * @param string $email
     * @return Listener
     */
    public function findByEmail($email);
}