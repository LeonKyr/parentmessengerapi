<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\EventPropertyValueBag;

class EventPropertyValueBagRepository
    extends RepositoryBase
    implements IEventPropertyValueBagRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param EventPropertyValueBag $value
     * @return void
     */
    public function save(EventPropertyValueBag $value)
    {
        parent::baseSave($value);
    }

    /**
     * @param int $eventPropertyValueId
     * @return EventPropertyValueBag[]
     */
    public function findByEventPropertyValueId($eventPropertyValueId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select epv
            from PMApiBundle:EventPropertyValueBag epv
            where epv.eventPropertyValueId = :eventPropertyValueId
            '
        )
            ->setParameter('eventPropertyValueId', $eventPropertyValueId);

        $result = $query->getResult();

        return $result;
    }
}