<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Device;

interface IDeviceRepository
{
    /**
     * @param Device $device
     */
    public function save(Device $device);

    /**
     * @param string $externalId
     * @return Device
     */
    public function findByExternalId($externalId);
}

