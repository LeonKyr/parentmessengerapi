<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Action;

interface IActionRepository
{
    /**
     * @param Action $action
     */
    public function save(Action $action);

    /**
     * @param string $name
     * @return Action
     */
    public function findByName($name);
}

