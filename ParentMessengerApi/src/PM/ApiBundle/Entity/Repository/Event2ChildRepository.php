<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Event2Child;

class Event2ChildRepository
    extends RepositoryBase
    implements IEvent2ChildRepository
{
    /**
     * @param Event2Child $event2child
     */
    public function save(Event2Child $event2child)
    {
        parent::baseSave($event2child);
    }
}