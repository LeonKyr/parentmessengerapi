<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Activity;
use PM\ApiBundle\Entity\Activity2Child;

class ActivityRepository
    extends RepositoryBase
    implements IActivityRepository
{
    /**
     * @var IActivity2ChildRepository
     */
    private $activity2childRepository;
    /**
     * @var IActivityDateRepository
     */
    private $activityDateRepository;

    function __construct(EntityManager $em,
                         IActivity2ChildRepository $activity2childRepository,
                         IActivityDateRepository $activityDateRepository)
    {
        parent::__construct($em);

        $this->activity2childRepository = $activity2childRepository;
        $this->activityDateRepository = $activityDateRepository;
    }

    /**
     * @param Activity $activity
     */
    public function save(Activity $activity)
    {
        try
        {
            $this->getEntityManager()->beginTransaction();

            parent::baseSave($activity);

            foreach ($activity->getActivityDates() as $date)
            {
                $date->setActivity($activity);
                $this->activityDateRepository->save($date);
            }

            /** @var Activity2Child */
            foreach ($activity->getActivity2children() as $activity2child) {
                $activity2child->setActivityId($activity->getId());
                $activity2child->setUpdatedAt(time());

                $this->activity2childRepository->save($activity2child);
            }

            $this->getEntityManager()->commit();
        }
        catch(\Exception $ex)
        {
            $this->getEntityManager()->rollback();

            echo $ex;
        }
    }

    /**
     * @param int $kindergartenId
     * @param int $updatedAt
     * @param int $limit
     * @return Activity[]
     */
    public function findByKindergartenIdAndAndUpdatedAt($kindergartenId, $updatedAt, $limit)
    {
        $query = $this->getEntityManager()->createQuery(
            'select a
            from PMApiBundle:Activity a
               join PMApiBundle:Teacher t
                   with a.teacherId = t.id
               join PMApiBundle:Activity2Child a2c
                   with a.id = a2c.activityId
               join PMApiBundle:ActivityDate ad
                   with a.id = ad.activityId
            where t.kindergartenId = :kindergartenId
              and (a2c.updatedAt >= :updatedAt
                  or a2c.updatedAt is null)
              and (ad.updatedAt >= :updatedAt
                  or ad.updatedAt is null)
            order by a.id desc
            '
        )
//            ->setMaxResults($limit)
            ->setParameter('kindergartenId', $kindergartenId)
            ->setParameter('updatedAt', $updatedAt);

        $result = $query->getResult();

        /** @var Activity $activity  */
        foreach ($result as $activity)
        {
            $this->enrichActivity($activity);
        }

        return $result;
    }

    /**
     * @param int $listenerId
     * @param int $updatedAt
     * @param int $limit
     * @return Activity[]
     */
    public function findByListenerIdAndAndUpdatedAt($listenerId, $updatedAt, $limit)
    {
        $query = $this->getEntityManager()->createQuery(
            'select a
            from PMApiBundle:Listener l
               join PMApiBundle:Child2Listener c2l
                   with c2l.listenerId = l.id
               join PMApiBundle:Activity2Child a2c
                   with a2c.childId = c2l.childId
               join PMApiBundle:Activity a
                   with a.id = a2c.activityId
               join PMApiBundle:ActivityDate ad
                   with a.id = ad.activityId
            where l.id = :listenerId
              and (a2c.updatedAt >= :updatedAt
                  or a2c.updatedAt is null)
              and (ad.updatedAt >= :updatedAt
                  or ad.updatedAt is null)
            order by a.id desc
            '
        )
            ->setMaxResults($limit)
            ->setParameter('listenerId', $listenerId)
            ->setParameter('updatedAt', $updatedAt);

        $result = $query->getResult();

        /** @var Activity $activity  */
        foreach ($result as $activity)
        {
            $this->enrichActivity($activity);
        }

        return $result;
    }

    /**
     * @param int $teacherId
     * @return Activity[]
     */
    public function findByTeacherId($teacherId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select a
            from PMApiBundle:Activity a
            where a.teacherId = :teacherId
            order by a.id desc
            '
        )
            ->setParameter('teacherId', $teacherId);

        $result = $query->getResult();

        /** @var Activity $activity  */
        foreach ($result as $activity)
        {
            $this->enrichActivity($activity);
        }

        return $result;
    }

    /**
     * @param string $externalId
     * @return Activity
     */
    public function findByExternalId($externalId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select a
            from PMApiBundle:Activity a
            where a.externalId = :externalId
            '
        )
            ->setParameter('externalId', $externalId);

        /** @var Activity $result */
        $result = $query->getOneOrNullResult();

        $this->enrichActivity($result);

        return $result;
    }

    private function enrichActivity(Activity &$activity)
    {
        if ($activity != null)
        {
            // dates
            $activity->setActivityDates($this->activityDateRepository->findByActivityId($activity->getId()));
            // children
            $activity->setActivity2Children($this->activity2childRepository->findByActivityId($activity->getId()));
        }

        return $activity;
    }
}
