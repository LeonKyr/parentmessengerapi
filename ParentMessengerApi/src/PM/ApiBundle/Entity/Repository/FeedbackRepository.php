<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Feedback;

class FeedbackRepository
    extends RepositoryBase
    implements IFeedbackRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param Feedback $feedback
     */
    public function save(Feedback $feedback)
    {
        parent::baseSave($feedback);
    }
}