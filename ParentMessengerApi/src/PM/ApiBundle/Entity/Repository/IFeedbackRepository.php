<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Feedback;

interface IFeedbackRepository
{
    /**
     * @param Feedback $feedback
     */
    public function save(Feedback $feedback);
}