<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\ActivityType;

class ActivityTypeRepository
    extends RepositoryBase
    implements IActivityTypeRepository
{

    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param int $id
     * @return ActivityType
     */
    public function findById($id)
    {
        $query = $this->getEntityManager()->createQuery(
            'select at
            from PMApiBundle:ActivityType at
            where at.id = :id
            '
        )
            ->setParameter('id', $id);

        $result = $query->getOneOrNullResult();

        return $result;
    }
}