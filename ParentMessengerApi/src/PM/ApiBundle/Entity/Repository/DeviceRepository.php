<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Device;

final class DeviceRepository
    extends RepositoryBase
    implements IDeviceRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param Device $device
     */
    public function save(Device $device)
    {
        parent::baseSave($device);
    }

    /**
     * @param string $externalId
     * @return Device
     */
    public function findByExternalId($externalId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select d
            from PMApiBundle:Device d
            where d.externalId = :externalId
            '
        )
            ->setParameter('externalId', $externalId);

        $result = $query->getOneOrNullResult();

        return $result;
    }
}