<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\ActivityDate;

interface IActivityDateRepository
{
    /**
     * @param ActivityDate $activityDate
     * @return
     */
    public function save(ActivityDate $activityDate);

    /**
     * @param ActivityDate $activityDate
     * @return
     */
    public function remove(ActivityDate $activityDate);

    /**
     * @param int $activityId
     * @return ActivityDate[]
     */
    public function findByActivityId($activityId);

    /**
     * @param string $externalId
     * @return ActivityDate
     */
    public function findByExternalId($externalId);
}