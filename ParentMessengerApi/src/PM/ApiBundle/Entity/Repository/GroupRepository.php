<?php

namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Device;
use PM\ApiBundle\Entity\Group;


final class GroupRepository
    extends RepositoryBase
    implements IGroupRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param Group $group
     */
    public function save(Group $group)
    {
        parent::baseSave($group);
    }

    /**
     * @param string $externalId
     * @return Group
     */
    public function findByExternalId($externalId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select g
            from PMApiBundle:Group g
            where g.externalId = :externalId
            '
        )
            ->setParameter('externalId', $externalId);

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param int $kindergartenId
     * @param int $deviceId
     * @param $updatedAt
     * @return Group[]
     */
    public function findByKindergartenIdWithDifferentDeviceIdAndUpdatedAt($kindergartenId, $deviceId, $updatedAt)
    {
        $query = $this->getEntityManager()->createQuery(
            'select g
            from PMApiBundle:Group g
            where g.kindergartenId = :kindergartenId
              and g.lastUpdatedByDeviceId != :deviceId
              and g.updatedAt >= :updatedAt
            '
        )
            ->setParameter('kindergartenId', $kindergartenId)
            ->setParameter('deviceId', $deviceId)
            ->setParameter('updatedAt', $updatedAt);

        $result = $query->execute();

        return $result;
    }
}

