<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\ChildContact;

class ChildContactRepository
    extends RepositoryBase
    implements IChildContactRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param ChildContact $childContact
     */
    public function save(ChildContact $childContact)
    {
        parent::baseSave($childContact);
    }

    /**
     * @param string $externalId
     * @return ChildContact
     */
    public function findByExternalId($externalId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select c
            from PMApiBundle:ChildContact c
            where c.externalId = :externalId
            '
        )
            ->setParameter('externalId', $externalId);

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param int $childId
     * @return ChildContact[]
     */
    public function findByChildId($childId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select c
            from PMApiBundle:ChildContact c
            where c.childId = :childId
            '
        )
            ->setParameter('childId', $childId);

        $result = $query->execute();

        return $result;
    }

    /**
     * @param int $childId
     * @param int $deviceId
     * @param $updatedAt
     * @return ChildContact[]
     */
    public function findByChildIdWithDifferentDeviceIdAndUpdatedAt($childId, $deviceId, $updatedAt)
    {
        $query = $this->getEntityManager()->createQuery(
            'select c
            from PMApiBundle:ChildContact c
            where c.childId = :childId
              and c.lastUpdatedByDeviceId != :deviceId
              and c.updatedAt >= :updatedAt
            '
        )
            ->setParameter('childId', $childId)
            ->setParameter('deviceId', $deviceId)
            ->setParameter('updatedAt', $updatedAt);

        $result = $query->execute();

        return $result;
    }
}