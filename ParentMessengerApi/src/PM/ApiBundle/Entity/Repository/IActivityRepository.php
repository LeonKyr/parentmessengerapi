<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Activity;

interface IActivityRepository
{
    /**
     * @param Activity $activity
     * @return
     */
    public function save(Activity $activity);

    /**
     * @param int $kindergartenId
     * @param int $updatedAt
     * @param int $limit
     * @return Activity[]
     */
    public function findByKindergartenIdAndAndUpdatedAt($kindergartenId, $updatedAt, $limit);

    /**
     * @param int $listenerId
     * @param int $updatedAt
     * @param int $limit
     * @return Activity[]
     */
    public function findByListenerIdAndAndUpdatedAt($listenerId, $updatedAt, $limit);

    /**
     * @param int $teacherId
     * @return Activity[]
     */
    public function findByTeacherId($teacherId);

    /**
     * @param string $externalId
     * @return Activity
     */
    public function findByExternalId($externalId);
}

