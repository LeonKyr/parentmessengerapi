<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\ChildContact;

interface IChildContactRepository
{
    /**
     * @param ChildContact $childContact
     */
    public function save(ChildContact $childContact);

    /**
     * @param string $externalId
     * @return ChildContact
     */
    public function findByExternalId($externalId);

    /**
     * @param int $childId
     * @return ChildContact[]
     */
    public function findByChildId($childId);

    /**
     * @param int $childId
     * @param int $deviceId
     * @param $updatedAt
     * @return ChildContact[]
     */
    public function findByChildIdWithDifferentDeviceIdAndUpdatedAt($childId, $deviceId, $updatedAt);
}