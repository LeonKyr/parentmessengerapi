<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Child2Listener;

class Child2ListenerRepository
    extends RepositoryBase
    implements IChild2ListenerRepository
{
    /**
     * @var IChildRepository
     */
    private $childRepository;

    function __construct
    (
        EntityManager $em,
        IChildRepository $childRepository)
    {
        parent::__construct($em);

        $this->childRepository = $childRepository;
    }

    /**
     * @param Child2Listener $child2listener
     */
    public function save(Child2Listener $child2listener)
    {
        parent::baseSave($child2listener);

        $this->updateHasListenersInChild($child2listener);
    }

    private function updateHasListenersInChild(Child2Listener $child2listener)
    {
        $child = $child2listener->getChild();
        $child->setHasListeners(true);
        $this->childRepository->save($child);
    }

    /**
     * @param string $listenerExternalId
     * @return Child2Listener[]
     */
    public function findByListenerExternalId($listenerExternalId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select c2l
            from PMApiBundle:Child2Listener c2l
            where c2l.listenerId in (
              select l.id
              from PMApiBundle:Listener l
              where l.externalId = :externalId
            )
            '
        )
            ->setParameter('externalId', $listenerExternalId);

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param int $listenerId
     * @return Child2Listener[]
     */
    public function findByListenerId($listenerId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select c2l
            from PMApiBundle:Child2Listener c2l
            where c2l.listenerId = :listenerId
            '
        )
            ->setParameter('listenerId', $listenerId);

        $result = $query->getResult();

        return $result;
    }

    /**
     * @param int $listenerId
     * @param int $childId
     * @return Child2Listener[]
     */
    public function findByListenerIdAndChildId($listenerId, $childId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select c2l
            from PMApiBundle:Child2Listener c2l
            where c2l.listenerId = :listenerId
              and c2l.childId = :childId
            '
        )
            ->setParameter('listenerId', $listenerId)
            ->setParameter('childId', $childId);

        $result = $query->getOneOrNullResult();

        return $result;
    }
}