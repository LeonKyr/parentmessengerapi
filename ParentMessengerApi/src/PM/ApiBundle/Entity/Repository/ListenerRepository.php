<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use PM\ApiBundle\Entity\Listener;

final class ListenerRepository
    extends RepositoryBase
    implements IListenerRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param Listener $listener
     */
    public function save(Listener $listener)
    {
        parent::baseSave($listener);
    }

    /**
     * @param int $id
     * @return Listener
     */
    public function findById($id)
    {
        $query = $this->getEntityManager()->createQuery(
            'select l
            from PMApiBundle:Listener l
            where l.id = :id
            '
        )
            ->setParameter('id', $id);

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param string $externalId
     * @return Listener
     */
    public function findByExternalId($externalId)
    {
        $query = $this->getEntityManager()->createQuery(
            'select l
            from PMApiBundle:Listener l
            where l.externalId = :externalId
            '
        )
            ->setParameter('externalId', $externalId);

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param string $email
     * @param string $password
     * @return Listener
     */
    public function findByEmailAndPassword($email, $password)
    {
        $query = $this->getEntityManager()->createQuery(
            'select l
            from PMApiBundle:Listener l
            where l.email = :email
              and l.password = :password
            '
        )
            ->setParameter('email', $email)
            ->setParameter('password', $password);

        $result = $query->getOneOrNullResult();

        return $result;
    }

    /**
     * @param string $email
     * @return Listener
     */
    public function findByEmail($email)
    {
        $query = $this->getEntityManager()->createQuery(
            'select l
            from PMApiBundle:Listener l
            where l.email = :email
            '
        )
            ->setParameter('email', $email);

        $result = $query->getOneOrNullResult();

        return $result;
    }
}