<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Child2Listener;

interface IChild2ListenerRepository
{
    /**
     * @param Child2Listener $child2listener
     */
    public function save(Child2Listener $child2listener);

    /**
     * @param string $listenerExternalId
     * @return Child2Listener[]
     */
    public function findByListenerExternalId($listenerExternalId);

    /**
     * @param int $listenerId
     * @return Child2Listener[]
     */
    public function findByListenerId($listenerId);

    /**
     * @param int $listenerId
     * @param int $childId
     * @return Child2Listener[]
     */
    public function findByListenerIdAndChildId($listenerId, $childId);
}