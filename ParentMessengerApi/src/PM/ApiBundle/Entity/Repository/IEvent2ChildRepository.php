<?php
namespace PM\ApiBundle\Entity\Repository;

use PM\ApiBundle\Entity\Child2Parent;
use PM\ApiBundle\Entity\Event2Child;

interface IEvent2ChildRepository
{
    /**
     * @param Event2Child $event2child
     */
    public function save(Event2Child $event2child);
}