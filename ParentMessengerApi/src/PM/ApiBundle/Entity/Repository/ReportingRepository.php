<?php
namespace PM\ApiBundle\Entity\Repository;

use Doctrine\ORM\EntityManager;
use Doctrine\ORM\Query\ResultSetMapping;
use PM\ApiBundle\Entity\Child;

final class ReportingRepository
    extends RepositoryBase
    implements IReportingRepository
{
    function __construct(EntityManager $em)
    {
        parent::__construct($em);
    }

    /**
     * @param $time
     * @param $groupId
     * @return Child[]
     */
    public function groupPresentceStatus($time, $groupId)
    {
        /*
select c.*
from event ein
	join event2child e2cin
		on ein.id = e2cin.event_id
	join event2child e2cout
		on e2cout.child_id = e2cin.child_id
	left join event eout
		on eout.id = e2cout.event_id
	join child c
		on e2cin.child_id = c.id
	join `group` g
		on g.id = c.group_id
	-- event properties
	join event_property_value epvin
		on epvin.event_id = ein.id
	join event_property_value epvout
		on epvout.event_id = eout.id
where ein.action_id = 2 -- sign in
	and eout.action_id = 4 -- sign out
	and epvin.action_property_id = 3 -- time-and-date
	and epvout.action_property_id = 3 -- time-and-date
	and g.id = group_id
	and (eout.id is null OR cast(epvout.value as unsigned) >= @time)
	and cast(epvin.value as unsigned) < @time;
        */
        $rsm = new ResultSetMapping();
        $rsm->addEntityResult('User', 'u');
        $rsm->addFieldResult('u', 'id', 'id');
        $rsm->addFieldResult('u', 'name', 'name');

        $query = $this->getEntityManager()->createNativeQuery('select c.*
from event ein
	join event2child e2cin
		on ein.id = e2cin.event_id
	join event2child e2cout
		on e2cout.child_id = e2cin.child_id
	left join event eout
		on eout.id = e2cout.event_id
	join child c
		on e2cin.child_id = c.id
	join `group` g
		on g.id = c.group_id
	-- event properties
	join event_property_value epvin
		on epvin.event_id = ein.id
	join event_property_value epvout
		on epvout.event_id = eout.id
where ein.action_id = 2 -- sign in
	and eout.action_id = 4 -- sign out
	and epvin.action_property_id = 3 -- time-and-date
	and epvout.action_property_id = 3 -- time-and-date
	and g.id = group_id
	and (eout.id is null OR cast(epvout.value as unsigned) >= @time)
	and cast(epvin.value as unsigned) < @time', $rsm)
            ->setParameter('time', $time)
            ->setParameter('groupId', $groupId);

        $result = $query->execute();

        return $result;
    }
}