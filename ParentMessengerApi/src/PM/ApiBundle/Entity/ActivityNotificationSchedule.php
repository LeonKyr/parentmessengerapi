<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * ActivityNotificationSchedule
 *
 * @ORM\Entity
 * @ORM\Table(name="activity_notification_schedule")
 */
class ActivityNotificationSchedule implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string")
     */
    private $description;

    /**
     * @var integer
     *
     * @ORM\Column(name="notify_hours_in_advance", type="integer")
     */
    private $notifyHoursInAdvance;

    function __construct($id, $name)
    {
        $this->id = $id;
        $this->name = $name;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @return int
     */
    public function getNotifyHoursInAdvance()
    {
        return $this->notifyHoursInAdvance;
    }

    public function jsonSerialize()
    {
        return $this->getName();
    }
}