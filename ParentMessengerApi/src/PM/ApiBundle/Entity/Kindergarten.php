<?php
namespace PM\ApiBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Kindergarten
 *
 * @ORM\Entity
 * @ORM\Table(name="kindergarten")
 */
class Kindergarten
    implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string")
     */
    private $externalId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string")
     */
    private $email;

    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     */
    private $phone;

    /**
     * @var string
     *
     * @ORM\Column(name="address", type="string")
     */
    private $address;

    /**
     * @var string
     *
     * @ORM\Column(name="contact_person", type="string")
     */
    private $contactPerson;

    /**
     * @var string
     *
     * @ORM\Column(name="password", type="string")
     */
    private $password;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="integer")
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_at", type="integer")
     */
    private $updatedAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_updated_by_device_id", type="integer")
     */
    private $lastUpdateByDeviceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @param string $name
     * @param string $email
     * @param string $password
     * @param string $externalId
     * @param string $phone
     * @param string $contactPerson
     * @param string $address
     */
    public function __construct(
        $name, $email, $password, $externalId,
        $phone, $contactPerson, $address)
    {
        $this->name = $name;
        $this->email = $email;
        $this->password = $password;
        $this->externalId = $externalId;

        $this->phone = $phone;
        $this->contactPerson = $contactPerson;
        $this->address = $address;

        $this->createdAt = time();
        $this->updatedAt = time();

        $this->setStatus(0);
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param string $password
     */
    public function setPassword($password)
    {
        $this->password = $password;
    }

    /**
     * @return string
     */
    public function getPassword()
    {
        return $this->password;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $address
     */
    public function setAddress($address)
    {
        $this->address = $address;
    }

    /**
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * @param string $contactPerson
     */
    public function setContactPerson($contactPerson)
    {
        $this->contactPerson = $contactPerson;
    }

    /**
     * @return string
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }

    /**
     * @param int $lastUpdateByDeviceId
     */
    public function setLastUpdateByDeviceId($lastUpdateByDeviceId)
    {
        $this->lastUpdateByDeviceId = $lastUpdateByDeviceId;
    }

    /**
     * @return int
     */
    public function getLastUpdateByDeviceId()
    {
        return $this->lastUpdateByDeviceId;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param int $status
     */
    public function setStatus($status)
    {
        $this->status = $status;
    }

    /**
     * @return int
     */
    public function getStatus()
    {
        return $this->status;
    }

    function __toString()
    {
        return 'Name: '.$this->getName().',Email:'.$this->getEmail().',Id='.$this->getId().',Status='.$this->getStatus();
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->getExternalId(),
            'name' => $this->getName(),
            'email' => $this->getEmail(),
            'phone' => $this->getPhone(),
            'address' => $this->getAddress(),
            'contact_person' => $this->getContactPerson(),
            'status' => $this->getStatusAsString(),
        );
    }

    private function getStatusAsString()
    {
        switch($this->getStatus())
        {
            case 0: return 'not-active';
            case 1: return 'active';
            case 2: return 'disabled';
        }
    }

    public function setStatusFromString($statusAsString)
    {
        switch($statusAsString)
        {
            case 'not-active': $this->setStatus(0); break;
            case 'active': $this->setStatus(1); break;
            case 'disabled': $this->setStatus(2); break;
        }
    }
}