<?php

namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Teacher
 *
 * @ORM\Entity
 * @ORM\Table(name="teacher")
 */
class Teacher
    implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string")
     */
    private $externalId;

    /**
     * @var Kindergarten
     *
     * @ORM\ManyToOne(targetEntity="Kindergarten")
     * @ORM\JoinColumn(name="kindergarten_id", referencedColumnName="id")
     */
    private $kindergarten;

    /**
     * @var int
     *
     * @ORM\Column(name="kindergarten_id", type="integer")
     **/
    private $kindergartenId;

    /**
     * @var Image
     *
     * @ORM\ManyToOne(targetEntity="Image")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    private $image;
    /**
     * @var int
     *
     * @ORM\Column(name="image_id", type="integer")
     **/
    private $imageId;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="integer")
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_at", type="integer")
     */
    private $updatedAt;

    /**
     * @var int
     *
     * @ORM\Column(name="last_updated_by_device_id", type="integer")
     **/
    private $lastUpdatedByDeviceId;

    function __construct()
    {
        $this->createdAt = time();
    }

    /**
     * @param integer $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return integer
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param integer $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return integer
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return \PM\ApiBundle\Entity\Kindergarten
     */
    public function getKindergarten()
    {
        return $this->kindergarten;
    }

    /**
     * @return int
     */
    public function getKindergartenId()
    {
        return $this->kindergartenId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @param int $kindergartenId
     */
    public function setKindergartenId($kindergartenId)
    {
        $this->kindergartenId = $kindergartenId;
    }

    /**
     * @param \PM\ApiBundle\Entity\Kindergarten $kindergarten
     */
    public function setKindergarten($kindergarten)
    {
        $this->kindergarten = $kindergarten;
        if ($kindergarten != null)
            $this->setKindergartenId($kindergarten->getId());
    }

    /**
     * @param \PM\ApiBundle\Entity\Image $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        if ($image != null)
            $this->setImageId($image->getId());
    }

    /**
     * @return \PM\ApiBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @param int $imageId
     */
    public function setImageId($imageId)
    {
        $this->imageId = $imageId;
    }

    /**
     * @return int
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * @param int $lastUpdatedByDeviceId
     */
    public function setLastUpdatedByDeviceId($lastUpdatedByDeviceId)
    {
        $this->lastUpdatedByDeviceId = $lastUpdatedByDeviceId;
    }

    /**
     * @return int
     */
    public function getLastUpdatedByDeviceId()
    {
        return $this->lastUpdatedByDeviceId;
    }

    function __toString()
    {
        return 'Id='.$this->getId().',ExternalId='.$this->getExternalId().',KindergartenId='.$this->getKindergartenId().
            ',Name='.$this->getName();
    }

    public function jsonSerialize()
    {
        $result = array(
            'id' => $this->getExternalId(),
            'name' => $this->getName(),
            'kindergartenId' => $this->getKindergarten()->getExternalId()
        );

        if ($this->getImage() != null)
        {
            $result['image'] = $this->getImage();
        }

        return $result;
    }
}