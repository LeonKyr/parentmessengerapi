<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Listener2Device
 *
 * @ORM\Entity
 * @ORM\Table(name="listener2device")
 */
class Listener2Device implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="device_id", type="integer")
     * @ORM\Id
     */
    private $deviceId;
    /**
     * @var integer
     *
     * @ORM\Column(name="listener_id", type="integer")
     * @ORM\Id
     */
    private $listenerId;

    /**
     * @var Listener
     *
     * @ORM\ManyToOne(targetEntity="Listener")
     * @ORM\JoinColumn(name="listener_id", referencedColumnName="id")
     */
    private $listener;

    /**
     * @var Device
     *
     * @ORM\ManyToOne(targetEntity="Device")
     * @ORM\JoinColumn(name="device_id", referencedColumnName="id")
     */
    private $device;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @param int $deviceId
     */
    public function setDeviceId($deviceId)
    {
        $this->deviceId = $deviceId;
    }

    /**
     * @return int
     */
    public function getDeviceId()
    {
        return $this->deviceId;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \PM\ApiBundle\Entity\Device
     */
    public function getDevice()
    {
        return $this->device;
    }

    /**
     * @param \PM\ApiBundle\Entity\Device $device
     */
    public function setDevice($device)
    {
        $this->device = $device;
        if ($device != null) {
            $this->setDeviceId($device->getId());
        }
    }

    /**
     * @param \PM\ApiBundle\Entity\Listener $listener
     */
    public function setListener($listener)
    {
        $this->listener = $listener;
        if ($listener != null) {
            $this->setListenerId($listener->getId());
        }
    }

    /**
     * @return \PM\ApiBundle\Entity\Listener
     */
    public function getListener()
    {
        return $this->listener;
    }

    /**
     * @param int $listenerId
     */
    public function setListenerId($listenerId)
    {
        $this->listenerId = $listenerId;
    }

    /**
     * @return int
     */
    public function getListenerId()
    {
        return $this->listenerId;
    }

    function __toString()
    {
        return 'deviceId=' . $this->getDeviceId().',listenerId='.$this->getListenerId();
    }

    public function jsonSerialize()
    {
        return array(
            'device' => $this->getDevice(),
            'listener' => $this->getListener(),
        );
    }
}