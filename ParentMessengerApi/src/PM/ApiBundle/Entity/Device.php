<?php

namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * Device
 *
 * @ORM\Entity
 * @ORM\Table(name="device")
 */
class Device
    implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string")
     **/
    private $externalId;

    /**
     * @var integer
     *
     * @ORM\Column(name="platform_id", type="integer")
     */
    private $platformId;

    /**
     * @var Platform
     *
     * @ORM\ManyToOne(targetEntity="Platform")
     * @ORM\JoinColumn(name="platform_id", referencedColumnName="id")
     */
    private $platform;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="token", type="string")
     **/
    private $token;

    /**
     * @var boolean
     *
     * @ORM\Column(name="notification_enabled", type="boolean")
     **/
    private $isNotificationEnabled;

    function __construct()
    {
        $this->isNotificationEnabled = 0;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $platformId
     */
    public function setPlatformId($platformId)
    {
        $this->platformId = $platformId;
    }

    /**
     * @return int
     */
    public function getPlatformId()
    {
        return $this->platformId;
    }

    /**
     * @param \PM\ApiBundle\Entity\boolean $isNotificationEnabled
     */
    public function setIsNotificationEnabled($isNotificationEnabled)
    {
        $this->isNotificationEnabled = $isNotificationEnabled;
    }

    /**
     * @return \PM\ApiBundle\Entity\boolean
     */
    public function getIsNotificationEnabled()
    {
        return $this->isNotificationEnabled;
    }

    /**
     * @param string $token
     */
    public function setToken($token)
    {
        $this->token = $token;
    }

    /**
     * @return string
     */
    public function getToken()
    {
        return $this->token;
    }

    function __toString()
    {
        return 'DeviceId=' . $this->getId() . ',ExternalId=' . $this->getExternalId().',Token='.$this->getToken().'Platform='.$this->getPlatformId();
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->getExternalId(),
            'token' => $this->getToken(),
        );
    }
}