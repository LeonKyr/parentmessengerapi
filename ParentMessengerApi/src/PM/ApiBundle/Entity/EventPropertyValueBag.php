<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * EventPropertyValueBag
 *
 * @ORM\Entity
 * @ORM\Table(name="event_property_value_bag")
 */
class EventPropertyValueBag
    implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;
    /**
     * @var integer
     *
     * @ORM\Column(name="event_property_value_id", type="integer")
     */
    private $eventPropertyValueId;

    /**
     * @var EventPropertyValue
     *
     * @ORM\ManyToOne(targetEntity="EventPropertyValue", inversedBy="eventPropertyValueBags")
     * @ORM\JoinColumn(name="event_property_value_id", referencedColumnName="id")
     */
    private $eventPropertyValue;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string")
     */
    private $externalId;

    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="integer")
     */
    private $createdAt;

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $eventPropertyValueId
     */
    public function setEventPropertyValueId($eventPropertyValueId)
    {
        $this->eventPropertyValueId = $eventPropertyValueId;
    }

    /**
     * @return int
     */
    public function getEventPropertyValueId()
    {
        return $this->eventPropertyValueId;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \PM\ApiBundle\Entity\EventPropertyValue $eventPropertyValue
     */
    public function setEventPropertyValue($eventPropertyValue)
    {
        $this->eventPropertyValue = $eventPropertyValue;

        if ($eventPropertyValue != null)
            $this->setEventPropertyValueId($eventPropertyValue->getId());
    }

    /**
     * @return \PM\ApiBundle\Entity\EventPropertyValue
     */
    public function getEventPropertyValue()
    {
        return $this->eventPropertyValue;
    }

    function __toString()
    {
        return 'id='.$this->getId();
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->getExternalId(),
        );
    }
}