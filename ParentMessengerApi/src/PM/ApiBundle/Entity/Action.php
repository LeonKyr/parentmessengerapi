<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Action
 *
 * @ORM\Entity
 * @ORM\Table(name="action")
 */
class Action implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     **/
    private $name;

    /**
     * @var bool
     *
     * @ORM\Column(name="is_kindergarten_only", type="boolean")
     **/
    private $isKindergartenOnly;

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @return bool
     */
    public function getIsKindergartenOnly()
    {
        return $this->isKindergartenOnly;
    }

    public function jsonSerialize()
    {
        return $this->getName();
    }
}