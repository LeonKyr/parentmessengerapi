<?php

namespace PM\ApiBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Feedback
 *
 * @ORM\Entity
 * @ORM\Table(name="feedback")
 */
class Feedback
    implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;
    /**
     * @var string
     *
     * @ORM\Column(name="text", type="string")
     **/
    private $text;
    /**
     * @var string
     *
     * @ORM\Column(name="source", type="string")
     **/
    private $source;
    /**
     * @var string
     *
     * @ORM\Column(name="entity", type="string")
     **/
    private $entity;
    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string")
     **/
    private $externalId;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $source
     */
    public function setSource($source)
    {
        $this->source = $source;
    }

    /**
     * @return string
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * @param string $text
     */
    public function setText($text)
    {
        $this->text = $text;
    }

    /**
     * @return string
     */
    public function getText()
    {
        return $this->text;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $entity
     */
    public function setEntity($entity)
    {
        $this->entity = $entity;
    }

    /**
     * @return string
     */
    public function getEntity()
    {
        return $this->entity;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    public function jsonSerialize()
    {
        return array(
            'id' => $this->getId(),
            'source' => $this->getSource(),
            'text' => $this->getText(),
        );
    }
}
