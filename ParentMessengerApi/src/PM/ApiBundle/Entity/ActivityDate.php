<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActivityDate
 *
 * @ORM\Entity
 * @ORM\Table(name="activity_date")
 */
class ActivityDate implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string")
     **/
    private $externalId;

    /**
     * @var Activity
     *
     * @ORM\ManyToOne(targetEntity="Activity")
     * @ORM\JoinColumn(name="activity_id", referencedColumnName="id")
     */
    private $activity;

    /**
     * @var int
     *
     * @ORM\Column(name="activity_id", type="integer")
     **/
    private $activityId;

    /**
     * @var ActivityNotificationSchedule
     *
     * @ORM\ManyToOne(targetEntity="ActivityNotificationSchedule")
     * @ORM\JoinColumn(name="activity_notification_schedule_id", referencedColumnName="id")
     */
//    private $activityNotificationSchedule;

    /**
     * @var int
     *
     * @ORM\Column(name="activity_notification_schedule_id", type="integer")
     **/
    private $activityNotificationScheduleId;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string")
     **/
    private $description;

    /**
     * @var int
     *
     * @ORM\Column(name="scheduled_at", type="integer")
     */
    private $scheduledAt;
    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="integer")
     */
    private $createdAt;
    /**
     * @var int
     *
     * @ORM\Column(name="updated_at", type="integer")
     */
    private $updatedAt;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    function __construct()
    {
        $this->setActivity(null);
        $this->setIsActive(true);
    }

    /**
     * @param \PM\ApiBundle\Entity\Activity $activity
     */
    public function setActivity($activity)
    {
        $this->activity = $activity;
    }

    /**
     * @return \PM\ApiBundle\Entity\Activity
     */
    public function getActivity()
    {
        return $this->activity;
    }

    /**
     * @param int $activityId
     */
    public function setActivityId($activityId)
    {
        $this->activityId = $activityId;
    }

    /**
     * @return int
     */
    public function getActivityId()
    {
        return $this->activityId;
    }

//    /**
//     * @param \PM\ApiBundle\Entity\ActivityNotificationSchedule $activityNotificationSchedule
//     */
//    public function setActivityNotificationSchedule($activityNotificationSchedule)
//    {
//        $this->activityNotificationSchedule = $activityNotificationSchedule;
//    }
//
//    /**
//     * @return \PM\ApiBundle\Entity\ActivityNotificationSchedule
//     */
//    public function getActivityNotificationSchedule()
//    {
//        return $this->activityNotificationSchedule;
//    }

    /**
     * @param int $activityNotificationScheduleId
     */
    public function setActivityNotificationScheduleId($activityNotificationScheduleId)
    {
        $this->activityNotificationScheduleId = $activityNotificationScheduleId;
    }

    /**
     * @return int
     */
    public function getActivityNotificationScheduleId()
    {
        return $this->activityNotificationScheduleId;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param int $scheduledAt
     */
    public function setScheduledAt($scheduledAt)
    {
        $this->scheduledAt = $scheduledAt;
    }

    /**
     * @return int
     */
    public function getScheduledAt()
    {
        return $this->scheduledAt;
    }

    function __toString()
    {
        return 'ActivityDateId=' . $this->getId() . ', ActivityId=' . $this->getActivityId() . ', ExternalId=' . $this->getExternalId();
    }

    public function jsonSerialize()
    {
        $result = array(
            'id' => $this->getExternalId(),
            'activity' => $this->getActivity()->getExternalId(),
            'scheduled_at' => $this->getScheduledAt(),
            'created_at' => $this->getCreatedAt(),
            'description' => $this->getDescription(),
            'notification_schedule' => $this->getActivityNotificationScheduleId(),
        );

        return $result;
    }
}