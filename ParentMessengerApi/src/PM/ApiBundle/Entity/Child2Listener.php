<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Child2Listener
 *
 * @ORM\Entity
 * @ORM\Table(name="child2listener")
 */
class Child2Listener implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="child_id", type="integer")
     * @ORM\Id
     */
    private $childId;
    /**
     * @var integer
     *
     * @ORM\Column(name="listener_id", type="integer")
     * @ORM\Id
     */
    private $listenerId;

    /**
     * @var Listener
     *
     * @ORM\ManyToOne(targetEntity="Listener")
     * @ORM\JoinColumn(name="listener_id", referencedColumnName="id")
     */
    private $listener;

    /**
     * @var Child
     *
     * @ORM\ManyToOne(targetEntity="Child")
     * @ORM\JoinColumn(name="child_id", referencedColumnName="id")
     */
    private $child;
    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @param int $childId
     */
    public function setChildId($childId)
    {
        $this->childId = $childId;
    }

    /**
     * @return int
     */
    public function getChildId()
    {
        return $this->childId;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @return \PM\ApiBundle\Entity\Child
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * @param \PM\ApiBundle\Entity\Child $child
     */
    public function setChild($child)
    {
        $this->child = $child;
        if ($child != null)
        {
            $this->setChildId($child->getId());
        }
    }

    /**
     * @param \PM\ApiBundle\Entity\Listener $listener
     */
    public function setListener($listener)
    {
        $this->listener = $listener;
        if ($listener != null)
        {
            $this->setListenerId($listener->getId());
        }
    }

    /**
     * @return \PM\ApiBundle\Entity\Listener
     */
    public function getListener()
    {
        return $this->listener;
    }

    /**
     * @param int $listenerId
     */
    public function setListenerId($listenerId)
    {
        $this->listenerId = $listenerId;
    }

    /**
     * @return int
     */
    public function getListenerId()
    {
        return $this->listenerId;
    }

    function __toString()
    {
        return 'ChildId='.$this->getChildId();
    }

    public function jsonSerialize()
    {
        return array(
            'child' => $this->getChild()
        );
    }
}