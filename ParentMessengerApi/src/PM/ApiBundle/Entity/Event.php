<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Event
 *
 * @ORM\Entity
 * @ORM\Table(name="event")
 */
class Event implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var Teacher
     *
     * @ORM\ManyToOne(targetEntity="Teacher")
     * @ORM\JoinColumn(name="teacher_id", referencedColumnName="id")
     */
    private $teacher;

    /**
     * @var int
     *
     * @ORM\Column(name="teacher_id", type="integer")
     **/
    private $teacherId;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string")
     **/
    private $externalId;

    /**
     * @var Action
     *
     * @ORM\ManyToOne(targetEntity="Action")
     * @ORM\JoinColumn(name="action_id", referencedColumnName="id")
     */
    private $action;

    /**
     * @var int
     *
     * @ORM\Column(name="action_id", type="integer")
     **/
    private $actionId;

    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="integer")
     */
    private $createdAt;
    /**
     * @var EventPropertyValue[]
     *
     * @ORM\OneToMany(targetEntity="EventPropertyValue", mappedBy="event")
     **/
    private $eventPropertyValues;

    /**
     * @param int $actionId
     */
    public function setActionId($actionId)
    {
        $this->actionId = $actionId;
    }

    /**
     * @return int
     */
    public function getActionId()
    {
        return $this->actionId;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $teacherId
     */
    public function setTeacherId($teacherId)
    {
        $this->teacherId = $teacherId;
    }

    /**
     * @return int
     */
    public function getTeacherId()
    {
        return $this->teacherId;
    }

    /**
     * @return \PM\ApiBundle\Entity\Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * @param \PM\ApiBundle\Entity\Teacher $teacher
     */
    public function setTeacher(Teacher $teacher)
    {
        $this->teacher = $teacher;
        $this->setTeacherId($teacher->getId());
    }

    /**
     * @param \PM\ApiBundle\Entity\EventPropertyValue[] $eventPropertyValues
     */
    public function setEventPropertyValues(array $eventPropertyValues)
    {
        $this->eventPropertyValues = $eventPropertyValues;
    }

    /**
     * @return \PM\ApiBundle\Entity\EventPropertyValue[]
     */
    public function getEventPropertyValues()
    {
        return $this->eventPropertyValues;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param \PM\ApiBundle\Entity\Action $action
     */
    public function setAction($action)
    {
        $this->action = $action;
        if ($action != null)
        {
            $this->setActionId($action->getId());
        }
    }

    /**
     * @return \PM\ApiBundle\Entity\Action
     */
    public function getAction()
    {
        return $this->action;
    }

    function __toString()
    {
        return 'EventId='.$this->getId().',ActionId='.$this->getActionId().',TeacherId='.$this->getTeacherId().', '.
            $this->getExternalId().',Properties.Count='.$this->getEventPropertyValues();
    }

    public function jsonSerialize()
    {
        $result = array(
            'id' => $this->getExternalId(),
            'action' => $this->getAction(),
            'created_at' => $this->getCreatedAt(),
            'properties' => $this->getEventPropertyValues(),
            'teacher' =>
            [
                'id' => $this->getTeacher()->getExternalId(),
                'name' => $this->getTeacher()->getName(),
            ],
        );

        return $result;
    }
}