<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * ActionProperty
 *
 * @ORM\Entity
 * @ORM\Table(name="action_property")
 */
class ActionProperty implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="action_property_type_id", type="integer")
     */
    private $actionPropertyTypeId;

    /**
     * @var ActionPropertyType
     *
     * @ORM\ManyToOne(targetEntity="ActionPropertyType")
     * @ORM\JoinColumn(name="action_property_type_id", referencedColumnName="id")
     */
    private $actionPropertyType;

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param \PM\ApiBundle\Entity\ActionPropertyType $actionPropertyType
     */
    public function setActionPropertyType($actionPropertyType)
    {
        $this->actionPropertyType = $actionPropertyType;
    }

    /**
     * @return \PM\ApiBundle\Entity\ActionPropertyType
     */
    public function getActionPropertyType()
    {
        return $this->actionPropertyType;
    }

    /**
     * @param int $actionPropertyTypeId
     */
    public function setActionPropertyTypeId($actionPropertyTypeId)
    {
        $this->actionPropertyTypeId = $actionPropertyTypeId;
    }

    /**
     * @return int
     */
    public function getActionPropertyTypeId()
    {
        return $this->actionPropertyTypeId;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * (PHP 5 &gt;= 5.4.0)<br/>
     * Specify data which should be serialized to JSON
     * @link http://php.net/manual/en/jsonserializable.jsonserialize.php
     * @return mixed data which can be serialized by <b>json_encode</b>,
     * which is a value of any type other than a resource.
     */
    public function jsonSerialize()
    {
        return array(
            'name' => $this->getName(),
            'type' => $this->getActionPropertyType(),
        );
    }
}