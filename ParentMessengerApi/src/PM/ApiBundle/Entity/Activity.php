<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Activity
 *
 * @ORM\Entity
 * @ORM\Table(name="activity")
 */
class Activity implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var Teacher
     *
     * @ORM\ManyToOne(targetEntity="Teacher")
     * @ORM\JoinColumn(name="teacher_id", referencedColumnName="id")
     */
    private $teacher;

    /**
     * @var int
     *
     * @ORM\Column(name="teacher_id", type="integer")
     **/
    private $teacherId;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string")
     **/
    private $externalId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     **/
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="description", type="string")
     **/
    private $description;

    /**
     * @var ActivityType
     *
     * @ORM\ManyToOne(targetEntity="ActivityType")
     * @ORM\JoinColumn(name="activity_type_id", referencedColumnName="id")
     */
    private $activityType;

    /**
     * @var int
     *
     * @ORM\Column(name="activity_type_id", type="integer")
     **/
    private $activityTypeId;

    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="integer")
     */
    private $createdAt;
    /**
     * @var int
     *
     * @ORM\Column(name="updated_at", type="integer")
     */
    private $updatedAt;
    /**
     * @var ActivityDate[]
     *
     **/
    //     * @ORM\OneToMany(targetEntity="ActivityDate", mappedBy="activity")
    private $activityDates;

    /**
     * @var Activity2Child[]
     *
     **/
    private $activity2Children;

    /**
     * @var boolean
     *
     * @ORM\Column(name="is_active", type="boolean")
     */
    private $isActive;

    function __construct()
    {
        $this->setIsActive(false);
    }

    /**
     * @param \PM\ApiBundle\Entity\ActivityDate[] $activityDates
     */
    public function setActivityDates(array $activityDates)
    {
        $this->activityDates = $activityDates;
    }

    /**
     * @return \PM\ApiBundle\Entity\ActivityDate[]
     */
    public function getActivityDates()
    {
        return $this->activityDates;
    }

    /**
     * @param \PM\ApiBundle\Entity\ActivityType $activityType
     */
    public function setActivityType(ActivityType $activityType)
    {
        $this->activityType = $activityType;
        if ($activityType != null)
            $this->setActivityTypeId($activityType->getId());
    }

    /**
     * @return \PM\ApiBundle\Entity\ActivityType
     */
    public function getActivityType()
    {
        return $this->activityType;
    }

    /**
     * @param int $activityTypeId
     */
    public function setActivityTypeId($activityTypeId)
    {
        $this->activityTypeId = $activityTypeId;
    }

    /**
     * @return int
     */
    public function getActivityTypeId()
    {
        return $this->activityTypeId;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return int
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    }

    /**
     * @return string
     */
    public function getDescription()
    {
        return $this->description;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param int $id
     */
    public function setId($id)
    {
        $this->id = $id;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param boolean $isActive
     */
    public function setIsActive($isActive)
    {
        $this->isActive = $isActive;
    }

    /**
     * @return boolean
     */
    public function getIsActive()
    {
        return $this->isActive;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param \PM\ApiBundle\Entity\Teacher $teacher
     */
    public function setTeacher($teacher)
    {
        $this->teacher = $teacher;
    }

    /**
     * @return \PM\ApiBundle\Entity\Teacher
     */
    public function getTeacher()
    {
        return $this->teacher;
    }

    /**
     * @param int $teacherId
     */
    public function setTeacherId($teacherId)
    {
        $this->teacherId = $teacherId;
    }

    /**
     * @return int
     */
    public function getTeacherId()
    {
        return $this->teacherId;
    }

    /**
     * @param int $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return int
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @param \PM\ApiBundle\Entity\Activity2Child[] $activity2Children
     */
    public function setActivity2Children($activity2Children)
    {
        $this->activity2Children = $activity2Children;
    }

    /**
     * @return \PM\ApiBundle\Entity\Activity2Child[]
     */
    public function getActivity2Children()
    {
        return $this->activity2Children;
    }

    function __toString()
    {
        return 'ActivityId=' . $this->getId() . ',ActivityTypeId=' . $this->getActivityTypeId() . ',TeacherId=' . $this->getTeacherId() . ', ' . $this->getExternalId();
    }

    public function jsonSerialize()
    {
        $children = array();

        foreach ($this->getActivity2children() as $a2c)
        {
            if ($a2c->getIsActive() == true)
            {
                array_push($children, $a2c->getChild());
            }
        }
        $result = array(
            'id' => $this->getExternalId(),
            'activity_type_id' => $this->getActivityType()->getId(),
            'is_active' => $this->getIsActive(),
            'name' => $this->getName(),
            'description' => $this->getDescription(),
            'created_at' => $this->getCreatedAt(),
            'updated_at' => $this->getUpdatedAt(),
            'dates' => $this->getActivityDates(),
            'children' => $children,
        );
        if ($this->getTeacher() != null)
        {
            $result['teacher'] =
            [
                'id' => $this->getTeacher()->getExternalId(),
                'name' => $this->getTeacher()->getName(),
            ];
        }

        return $result;
    }
}