<?php

namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;


/**
 * EventPropertyValue
 *
 * @ORM\Entity
 * @ORM\Table(name="event_property_value")
 */
class EventPropertyValue implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var integer
     *
     * @ORM\Column(name="event_id", type="integer")
     */
    private $eventId;

    /**
     * @var Event
     *
     * @ORM\ManyToOne(targetEntity="Event", inversedBy="eventPropertyValues")
     * @ORM\JoinColumn(name="event_id", referencedColumnName="id")
     */
    private $event;

    /**
     * @var ActionProperty
     *
     * @ORM\ManyToOne(targetEntity="ActionProperty")
     * @ORM\JoinColumn(name="action_property_id", referencedColumnName="id")
     */
    private $actionProperty;

    /**
     * @var integer
     *
     * @ORM\Column(name="action_property_id", type="integer")
     */
    private $actionPropertyId;

    /**
     * @var int
     *
     * @ORM\Column(name="created_at", type="integer")
     */
    private $createdAt;

    /**
     * @var string
     *
     * @ORM\Column(name="value", type="string")
     */
    private $value;

    /**
     * @var EventPropertyValueBag[]
     **/
    private $eventPropertyValueBags;
    //    * @ORM\OneToMany(targetEntity="EventPropertyValueBag", mappedBy="eventPropertyValue")

    /** @var boolean
     * @ORM\Column(name="is_bag", type="boolean")
     * */
    private $isBag;

    /** @var boolean
     * @ORM\Column(name="is_selection", type="boolean")
     * */
    private $isSelection;

    /** @var integer
     * @ORM\Column(name="selected_value", type="integer")
     * */
    private $selectedValue;

    function __construct($actionProperty, $isBag = false)
    {
        $this->setActionProperty($actionProperty);
        $this->isBag = $isBag;
        $this->isSelection = false;
        $this->eventPropertyValueBags = array();
    }

    /**
     * @param \PM\ApiBundle\Entity\EventPropertyValueBag[] $eventPropertyValueBags
     */
    public function setEventPropertyValueBags($eventPropertyValueBags)
    {
        $this->eventPropertyValueBags = $eventPropertyValueBags;
    }

    /**
     * @return \PM\ApiBundle\Entity\EventPropertyValueBag[]
     */
    public function getEventPropertyValueBags()
    {
        return $this->eventPropertyValueBags;
    }

    /**
     * @param boolean $isBag
     */
    public function setIsBag($isBag)
    {
        $this->isBag = $isBag;
    }

    /**
     * @return boolean
     */
    public function getIsBag()
    {
        return $this->isBag;
    }

    /**
     * @param int $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param int $actionPropertyId
     */
    public function setActionPropertyId($actionPropertyId)
    {
        $this->actionPropertyId = $actionPropertyId;
    }

    /**
     * @return int
     */
    public function getActionPropertyId()
    {
        return $this->actionPropertyId;
    }

    /**
     * @param string $value
     */
    public function setValue($value)
    {
        $this->value = $value;
    }

    /**
     * @return string
     */
    public function getValue()
    {
        return $this->value;
    }

    /**
     * @param boolean $isSelection
     */
    public function setIsSelection($isSelection)
    {
        $this->isSelection = $isSelection;
    }

    /**
     * @return boolean
     */
    public function getIsSelection()
    {
        return $this->isSelection;
    }

    /**
     * @param int $selectedValue
     */
    public function setSelectedValue($selectedValue)
    {
        $this->selectedValue = $selectedValue;
    }

    /**
     * @return int
     */
    public function getSelectedValue()
    {
        return $this->selectedValue;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \PM\ApiBundle\Entity\Event $event
     */
    public function setEvent($event)
    {
        $this->event = $event;
        $this->eventId = $event->getId();
    }

    /**
     * @return \PM\ApiBundle\Entity\Event
     */
    public function getEvent()
    {
        return $this->event;
    }

    /**
     * @return \PM\ApiBundle\Entity\ActionProperty
     */
    public function getActionProperty()
    {
        return $this->actionProperty;
    }

    /**
     * @param \PM\ApiBundle\Entity\ActionProperty $actionProperty
     */
    public function setActionProperty($actionProperty)
    {
        $this->actionProperty = $actionProperty;

        if ($actionProperty != null)
        {
            $this->actionPropertyId = $actionProperty->getId();
        }
    }

    /**
     * @param int $eventId
     */
    public function setEventId($eventId)
    {
        $this->eventId = $eventId;
    }

    /**
     * @return int
     */
    public function getEventId()
    {
        return $this->eventId;
    }

    function __toString()
    {
        return 'Id='.$this->getId().',ActionPropertyId='.$this->getActionPropertyId()
            .',Value='.$this->getValue().',SelectedValue='.$this->getSelectedValue();
    }

    public function jsonSerialize()
    {
        $result = array(
            'type' => $this->getActionProperty(),
            'is_bag' => $this->getIsBag(),
            'is_selection' => $this->getIsSelection(),
        );

        if ($this->getIsBag())
        {
            $result['value'] = $this->getEventPropertyValueBags();
        }
        else if ($this->getIsSelection())
        {
            $result['value'] = $this->getSelectedValue().'';
            $result['selected'] = $this->getSelectedValue().'';
        }
        else
        {
            $result['value'] = $this->getValue();
        }

        return $result;
    }
}