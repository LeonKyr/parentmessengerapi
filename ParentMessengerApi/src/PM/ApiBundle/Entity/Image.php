<?php
namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Image
 *
 * @ORM\Entity
 * @ORM\Table(name="image")
 */
class Image
    implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string")
     **/
    private $externalId;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     **/
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="path", type="text")
     **/
    private $path;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created_at", type="datetime")
     */
    private $createdAt;

    /**
     * @var Device
     *
     * @ORM\ManyToOne(targetEntity="Device")
     * @ORM\JoinColumn(name="last_updated_by_device_id", referencedColumnName="id")
     */
    private $lastUpdatedByDevice;
    /**
     * @var int
     *
     * @ORM\Column(name="last_updated_by_device_id", type="integer")
     **/
    private $lastUpdatedByDeviceId;

    /**
     * @param \DateTime $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param string $path
     */
    public function setPath($path)
    {
        $this->path = $path;
    }

    /**
     * @return string
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param int $lastUpdatedByDeviceId
     */
    public function setLastUpdatedByDeviceId($lastUpdatedByDeviceId)
    {
        $this->lastUpdatedByDeviceId = $lastUpdatedByDeviceId;
    }

    /**
     * @return int
     */
    public function getLastUpdatedByDeviceId()
    {
        return $this->lastUpdatedByDeviceId;
    }

    /**
     * @param \PM\ApiBundle\Entity\Device $lastUpdatedByDevice
     */
    public function setLastUpdatedByDevice($lastUpdatedByDevice)
    {
        $this->lastUpdatedByDevice = $lastUpdatedByDevice;
        if ($lastUpdatedByDevice != null)
            $this->setLastUpdatedByDeviceId($lastUpdatedByDevice->getId());
    }

    /**
     * @return \PM\ApiBundle\Entity\Device
     */
    public function getLastUpdatedByDevice()
    {
        return $this->lastUpdatedByDevice;
    }

    function __toString()
    {
        return 'Id=' . $this->getId() . ',ExternalId=' . $this->getExternalId() . ',Path='.$this->getPath();
    }

    public function jsonSerialize()
    {
        $result = array(
            'id' => $this->getExternalId(),
            'name' => $this->getName(),
        );

//        if ($this->getLastUpdatedByDevice() != null)
//            $result['lastUpdatedByDevice'] = $this->getLastUpdatedByDevice();

        return $result;
    }
}