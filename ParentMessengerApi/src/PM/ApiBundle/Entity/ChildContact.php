<?php
namespace PM\ApiBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * ChildContact
 *
 * @ORM\Entity
 * @ORM\Table(name="child_contact")
 */
class ChildContact
    implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;
    /**
     * @var Child
     *
     * @ORM\ManyToOne(targetEntity="Child")
     * @ORM\JoinColumn(name="child_id", referencedColumnName="id")
     */
    private $child;
    /**
     * @var int
     *
     * @ORM\Column(name="child_id", type="integer")
     **/
    private $childId;
    /**
     * @var Image
     *
     * @ORM\ManyToOne(targetEntity="Image")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    private $image;
    /**
     * @var int
     *
     * @ORM\Column(name="image_id", type="integer")
     **/
    private $imageId;
    /**
     * @var int
     *
     * @ORM\Column(name="relation_id", type="integer")
     **/
    private $relationId;
    /**
     * @var int
     *
     * @ORM\Column(name="last_updated_by_device_id", type="integer")
     **/
    private $lastUpdatedByDeviceId;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;
    /**
     * @var string
     *
     * @ORM\Column(name="phone", type="string")
     **/
    private $phone;
    /**
     * @var string
     *
     * @ORM\Column(name="email", type="string")
     **/
    private $email;
    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string")
     **/
    private $externalId;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="integer")
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_at", type="integer")
     */
    private $updatedAt;

    function __construct()
    {
        $this->createdAt = time();
        $this->updatedAt = time();
    }

    /**
     * @return \PM\ApiBundle\Entity\Child
     */
    public function getChild()
    {
        return $this->child;
    }

    /**
     * @param int $childId
     */
    public function setChildId($childId)
    {
        $this->childId = $childId;
    }

    /**
     * @param \PM\ApiBundle\Entity\Image $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        if ($image != null)
            $this->setImageId($image->getId());
    }

    /**
     * @return \PM\ApiBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return int
     */
    public function getChildId()
    {
        return $this->childId;
    }

    /**
     * @param integer $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return integer
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param integer $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return integer
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \PM\ApiBundle\Entity\Child $child
     */
    public function setChild($child)
    {
        $this->child = $child;
        if ($child != null)
            $this->setChildId($child->getId());
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }


    /**
     * @param int $imageId
     */
    public function setImageId($imageId)
    {
        $this->imageId = $imageId;
    }

    /**
     * @return int
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * @param int $lastUpdatedByDeviceId
     */
    public function setLastUpdatedByDeviceId($lastUpdatedByDeviceId)
    {
        $this->lastUpdatedByDeviceId = $lastUpdatedByDeviceId;
    }

    /**
     * @return int
     */
    public function getLastUpdatedByDeviceId()
    {
        return $this->lastUpdatedByDeviceId;
    }

    /**
     * @param string $email
     */
    public function setEmail($email)
    {
        $this->email = $email;
    }

    /**
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * @param string $phone
     */
    public function setPhone($phone)
    {
        $this->phone = $phone;
    }

    /**
     * @return string
     */
    public function getPhone()
    {
        return $this->phone;
    }

    /**
     * @param int $relationId
     */
    public function setRelationId($relationId)
    {
        $this->relationId = $relationId;
    }

    /**
     * @return int
     */
    public function getRelationId()
    {
        return $this->relationId;
    }

    public function jsonSerialize()
    {
        $result = array(
            'id' => $this->getExternalId(),
            'name' => $this->getName(),
            'phone' => $this->getPhone(),
            'email' => $this->getEmail(),
            'relation' => $this->getRelation()
        );
        if ($this->getImage() != null)
        {
//            $result['image'] = $this->getImage();
        }

        return $result;
    }

    private function getRelation()
    {
        switch($this->getRelationId())
        {
            case 1: return "father";
            case 2: return "mother";
            case 3: return "sister";
            case 4: return "brother";
            case 5: return "grandmother";
            case 6: return "grandfather";
            default: throw new \Exception("Convertsion for ".$this->getRelationId()." was not implemented.");
        }
    }
}