<?php
namespace PM\ApiBundle\Entity;

use DateTime;
use Doctrine\ORM\Mapping as ORM;

/**
 * Group
 *
 * @ORM\Entity
 * @ORM\Table(name="`group`")
 */
class Group
    implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;
    /**
     * @var Kindergarten
     *
     * @ORM\ManyToOne(targetEntity="Kindergarten")
     * @ORM\JoinColumn(name="kindergarten_id", referencedColumnName="id")
     */
    private $kindergarten;
    /**
     * @var int
     *
     * @ORM\Column(name="kindergarten_id", type="integer")
     **/
    private $kindergartenId;
    /**
     * @var int
     *
     * @ORM\Column(name="color", type="integer")
     **/
    private $color;

    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;

    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string")
     **/
    private $externalId;

    private $device;

    /**
     * @var integer
     *
     * @ORM\Column(name="last_updated_by_device_id", type="integer")
     */
    private $lastUpdatedByDeviceId;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="integer")
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_at", type="integer")
     */
    private $updatedAt;

    function __construct()
    {
        $this->createdAt = time();
        $this->updatedAt = time();
    }


    /**
     * @return \PM\ApiBundle\Entity\Kindergarten
     */
    public function getKindergarten()
    {
        return $this->kindergarten;
    }

    /**
     * @param int $kindergartenId
     */
    public function setKindergartenId($kindergartenId)
    {
        $this->kindergartenId = $kindergartenId;
    }

    /**
     * @return int
     */
    public function getKindergartenId()
    {
        return $this->kindergartenId;
    }

    /**
     * @param integer $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return integer
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param integer $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return integer
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param int $color
     */
    public function setColor($color)
    {
        $this->color = $color;
    }

    /**
     * @return int
     */
    public function getColor()
    {
        return $this->color;
    }

    /**
     * @param int $lastUpdatedByDeviceId
     */
    public function setLastUpdatedByDeviceId($lastUpdatedByDeviceId)
    {
        $this->lastUpdatedByDeviceId = $lastUpdatedByDeviceId;
    }

    /**
     * @return int
     */
    public function getLastUpdatedByDeviceId()
    {
        return $this->lastUpdatedByDeviceId;
    }

    /**
     * @param \PM\ApiBundle\Entity\Kindergarten $kindergarten
     */
    public function setKindergarten($kindergarten)
    {
        $this->kindergarten = $kindergarten;
        if ($kindergarten != null)
            $this->setKindergartenId($kindergarten->getId());
    }

    /**
     * @param \PM\ApiBundle\Entity\Device $device
     */
    public function setDevice($device)
    {
        $this->device = $device;
        if ($device != null)
            $this->setLastUpdatedByDeviceId($device->getId());
    }

    /**
     * @return \PM\ApiBundle\Entity\Device
     */
    public function getDevice()
    {
        return $this->device;
    }

    public function jsonSerialize()
    {
        $result = array(
            'id' => $this->getExternalId(),
            'name' => $this->getName(),
            'color' => $this->getColor(),
        );

        return $result;
    }
}