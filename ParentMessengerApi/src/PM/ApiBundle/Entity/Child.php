<?php

namespace PM\ApiBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Child
 *
 * @ORM\Entity
 * @ORM\Table(name="child")
 */
class Child
    implements \JsonSerializable
{
    /**
     * @var integer
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     * @ORM\Id
     */
    private $id;
    /**
     * @var Kindergarten
     *
     * @ORM\ManyToOne(targetEntity="Kindergarten")
     * @ORM\JoinColumn(name="kindergarten_id", referencedColumnName="id")
     */
    private $kindergarten;
    /**
     * @var int
     *
     * @ORM\Column(name="kindergarten_id", type="integer")
     **/
    private $kindergartenId;
    /**
     * @var Image
     *
     * @ORM\ManyToOne(targetEntity="Image")
     * @ORM\JoinColumn(name="image_id", referencedColumnName="id")
     */
    private $image;
    /**
     * @var int
     *
     * @ORM\Column(name="image_id", type="integer")
     **/
    private $imageId;
    /**
     * @var Group
     *
     * @ORM\ManyToOne(targetEntity="Group")
     * @ORM\JoinColumn(name="group_id", referencedColumnName="id")
     */
    private $group;
    /**
     * @var int
     *
     * @ORM\Column(name="group_id", type="integer")
     **/
    private $groupId;

    /**
     * @var int
     *
     * @ORM\Column(name="last_updated_by_device_id", type="integer")
     **/
    private $lastUpdatedByDeviceId;
    /**
     * @var string
     *
     * @ORM\Column(name="name", type="string")
     */
    private $name;
    /**
     * @var Boolean
     *
     * @ORM\Column(name="has_listeners", type="boolean")
     */
    private $hasListeners;
    /**
     * @var string
     *
     * @ORM\Column(name="external_id", type="string")
     **/
    private $externalId;
    /**
     * @var string
     *
     * @ORM\Column(name="code", type="string")
     **/
    private $code;

    /**
     * @var int
     *
     * @ORM\Column(name="dob", type="integer")
     */
    private $dob;

    /**
     * @var integer
     *
     * @ORM\Column(name="created_at", type="integer")
     */
    private $createdAt;

    /**
     * @var integer
     *
     * @ORM\Column(name="updated_at", type="integer")
     */
    private $updatedAt;

    /** @var  ChildContact[] */
    private $childContacts;

    function __construct()
    {
        $this->hasListeners = 0;
        $this->createdAt = time();
        $this->updatedAt = time();
    }

    /**
     * @return \PM\ApiBundle\Entity\Kindergarten
     */
    public function getKindergarten()
    {
        return $this->kindergarten;
    }

    /**
     * @param int $kindergartenId
     */
    public function setKindergartenId($kindergartenId)
    {
        $this->kindergartenId = $kindergartenId;
    }

    /**
     * @param \PM\ApiBundle\Entity\Image $image
     */
    public function setImage($image)
    {
        $this->image = $image;
        if ($image != null)
            $this->setImageId($image->getId());
    }

    /**
     * @return \PM\ApiBundle\Entity\Image
     */
    public function getImage()
    {
        return $this->image;
    }

    /**
     * @return int
     */
    public function getKindergartenId()
    {
        return $this->kindergartenId;
    }

    /**
     * @param int $dob
     */
    public function setDob($dob)
    {
        $this->dob = $dob;
    }

    /**
     * @return integer
     */
    public function getDob()
    {
        return $this->dob;
    }

    /**
     * @param integer $createdAt
     */
    public function setCreatedAt($createdAt)
    {
        $this->createdAt = $createdAt;
    }

    /**
     * @return integer
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * @param string $externalId
     */
    public function setExternalId($externalId)
    {
        $this->externalId = $externalId;
    }

    /**
     * @return string
     */
    public function getExternalId()
    {
        return $this->externalId;
    }

    /**
     * @param int $groupId
     */
    public function setGroupId($groupId)
    {
        $this->groupId = $groupId;
    }

    /**
     * @return int
     */
    public function getGroupId()
    {
        return $this->groupId;
    }

    /**
     * @param string $name
     */
    public function setName($name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * @param integer $updatedAt
     */
    public function setUpdatedAt($updatedAt)
    {
        $this->updatedAt = $updatedAt;
    }

    /**
     * @return integer
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * @return int
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @param \PM\ApiBundle\Entity\Kindergarten $kindergarten
     */
    public function setKindergarten($kindergarten)
    {
        $this->kindergarten = $kindergarten;
        if ($kindergarten != null)
            $this->setKindergartenId($kindergarten->getId());
    }

    /**
     * @param string $code
     */
    public function setCode($code)
    {
        $this->code = $code;
    }

    /**
     * @return string
     */
    public function getCode()
    {
        return $this->code;
    }

    /**
     * @param int $imageId
     */
    public function setImageId($imageId)
    {
        $this->imageId = $imageId;
    }

    /**
     * @return int
     */
    public function getImageId()
    {
        return $this->imageId;
    }

    /**
     * @param int $lastUpdatedByDeviceId
     */
    public function setLastUpdatedByDeviceId($lastUpdatedByDeviceId)
    {
        $this->lastUpdatedByDeviceId = $lastUpdatedByDeviceId;
    }

    /**
     * @return int
     */
    public function getLastUpdatedByDeviceId()
    {
        return $this->lastUpdatedByDeviceId;
    }

    /**
     * @param boolean $hasListeners
     */
    public function setHasListeners($hasListeners)
    {
        $this->hasListeners = $hasListeners;
    }

    /**
     * @return boolean
     */
    public function getHasListeners()
    {
        return $this->hasListeners;
    }

    /**
     * @return \PM\ApiBundle\Entity\Group
     */
    public function getGroup()
    {
        return $this->group;
    }

    /**
     * @param \PM\ApiBundle\Entity\Group $group
     */
    public function setGroup($group)
    {
        $this->group = $group;
        if ($group != null)
            $this->setGroupId($group->getId());
    }

    /**
     * @param \PM\ApiBundle\Entity\ChildContact[] $childContacts
     */
    public function setChildContacts($childContacts)
    {
        $this->childContacts = $childContacts;
    }

    /**
     * @return \PM\ApiBundle\Entity\ChildContact[]
     */
    public function getChildContacts()
    {
        return $this->childContacts;
    }

    public function jsonSerialize()
    {
        $result = array(
            'id' => $this->getExternalId(),
            'name' => $this->getName(),
            'code' => $this->getCode(),
            'hasListeners' => $this->getHasListeners(),
            'dob' => $this->getDob(),
        );
        if ($this->getGroup() != null)
        {
            $result['group_id'] = $this->getGroup()->getExternalId();
        }
        if ($this->getImage() != null)
        {
            $result['image'] = $this->getImage();
        }
        if ($this->getChildContacts() != null)
        {
            $result['contacts'] = $this->getChildContacts();
        }

        return $result;
    }
}
