<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Controller\BaseController;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Log\LoggerInterface;

class ReportingController
    extends BaseController
{
    public function __construct(
        LoggerInterface $logger
    )
    {
        parent::__construct($logger);
    }

    public function getAction(Request $request, $externalId)
    {
        $this->logRequest($request);

//      $result = $this->getChildByExternalId($externalId);

//      $response = new JsonResponse($result);
        $response = new JsonResponse();

//      $this->logResponse($response);

        return $response;
    }
}