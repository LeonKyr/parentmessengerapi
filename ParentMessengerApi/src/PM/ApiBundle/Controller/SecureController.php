<?php
namespace PM\ApiBundle\Controller;


use PM\ApiBundle\Common\Exception\DeviceNotSuppliedException;
use PM\ApiBundle\Entity\Factory\IKindergartenFactory;
use PM\ApiBundle\Entity\Listener2Device;
use PM\ApiBundle\Entity\Repository\IDeviceRepository;
use PM\ApiBundle\Entity\Repository\IKindergartenRepository;
use PM\ApiBundle\Entity\Repository\IListener2DeviceRepository;
use PM\ApiBundle\Entity\Repository\IListenerRepository;
use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Monolog\Logger;

class SecureController
    extends BaseController
{
    /**
    * @var \PM\ApiBundle\Entity\Repository\IListenerRepository
    */
    private $listenerRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IKindergartenRepository
     */
    private $kindergartenRepository;
    /**
     * @var \PM\ApiBundle\Entity\Factory\IKindergartenFactory
     */
    private $kindergartenFactory;
    /**
     * @var DeviceController
     */
    private $deviceController;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IDeviceRepository
     */
    private $deviceRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IListener2DeviceRepository
     */
    private $listener2deviceRepository;

    public function __construct(
        LoggerInterface $logger,
        IListenerRepository $listenerRepository,
        IKindergartenRepository $kindergartenRepository,
        IKindergartenFactory $kindergartenFactory,
        IDeviceRepository $deviceRepository,
        DeviceController $deviceController,
        IListener2DeviceRepository $listener2deviceRepository
    )
    {
        parent::__construct($logger);

        $this->listenerRepository = $listenerRepository;
        $this->kindergartenRepository = $kindergartenRepository;
        $this->kindergartenFactory = $kindergartenFactory;
        $this->deviceController = $deviceController;
        $this->deviceRepository = $deviceRepository;
        $this->listener2deviceRepository = $listener2deviceRepository;
    }

    public function registerAction(Request $request, $entity)
    {
        $this->logRequest($request);

        $data = $this->getJsonData($request);

        if (!isset($data['device']) ||
            $data['device']['id'] == null)
        {
            throw new DeviceNotSuppliedException();
        }

        $device = $this->deviceRepository->findByExternalId($data['device']['id']);

        if ($device == null)
        {
            $device = $this->deviceController->create($data['device']['id']);
        }

        $deviceId = $device->getId();

        $result = null;

        if ($entity === 'kindergarten')
        {
            $dcc = $this->kindergartenFactory->create($data['name'], $data['email'], $data['password'], UUID::v4(),
                $data['phone'], $data['contact_person'], $data['address']);

            $dcc->setStatus(0);
            $dcc->setLastUpdateByDeviceId($deviceId);

            $this->kindergartenRepository->save($dcc);
            $result = $dcc;
        }
        else if ($entity === 'device')
        {
            $device = $this->deviceController->create($data['device']['id']);

            $result = $device;
        }
        else
        {
            throw new \Exception("Entity type [=$entity] is not supported.");
        }
        $httpResponse = new JsonResponse($result);
        $this->logResponse($httpResponse);

        return $httpResponse;
    }

    public function loginAction(Request $request, $entity, $email, $password)
    {
        $this->logRequest($request);

        $data = $this->getJsonData($request);

        if (!isset($data['device'])
            || $data['device']['id'] == null
        )
        {
            throw new DeviceNotSuppliedException();
        }

        $device = $this->deviceRepository->findByExternalId($data['device']['id']);

        if ($device == null)
        {
            $device = $this->deviceController->create($data['device']['id']);
        }

        if ($entity === 'listener')
        {
            $value = $this->listenerRepository->findByEmailAndPassword($email, $password);
            $response = $value != null;

            if ($value != null && $device != null && $value->getId() != 0)
            {
                if ($this->listener2deviceRepository->findByListenerIdAndDeviceId($value->getId(), $device->getId()) == null)
                {
                    $l2d = new Listener2Device();
                    $l2d->setDevice($device);
                    $l2d->setListener($value);

                    $this->listener2deviceRepository->save($l2d);
                }
            }
        }
        else if ($entity === 'kindergarten')
        {
            $value = $this->kindergartenRepository->findByEmailAndPassword($email, $password);
            $response = $value != null;
        }
        else
        {
            throw new \Exception("Entity type [=$entity] is not supported.");
        }
        $httpResponse = new JsonResponse($value, $response ? 200 : 403);
        $this->logResponse($httpResponse);

        return $httpResponse;
    }

    function generatePassword($length = 8) {
        $chars = 'abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789';
        $count = mb_strlen($chars);

        for ($i = 0, $result = ''; $i < $length; $i++) {
            $index = rand(0, $count - 1);
            $result .= mb_substr($chars, $index, 1);
        }

        return $result;
    }


    public function forgotPasswordAction(Request $request, $entity, $email)
    {
        // TODO: implement
        $this->logRequest($request);

        if ($entity === 'listener')
        {
            $value = $this->listenerRepository->findByEmail($email);
            //$response = $value != null;
        }
        else if ($entity === 'kindergarten')
        {
            $value = $this->kindergartenRepository->findByEmail($email);
            //$response = $value != null;
        }
        else
        {
            throw new \Exception("Entity type [=$entity] is not supported.");
        }

        $subject = 'password recovery';
        $message = 'your password is '.$this->generatePassword();
        mail($email, $subject, $message);

        $httpResponse = new JsonResponse('OK', 200);
        $this->logResponse($httpResponse);

        return $httpResponse;
    }
}