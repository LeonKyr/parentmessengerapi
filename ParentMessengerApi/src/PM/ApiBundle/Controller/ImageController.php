<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Common\Exception\DeviceNotFoundException;
use PM\ApiBundle\Common\Exception\ImageNotFoundException;
use PM\ApiBundle\Entity\Image;
use PM\ApiBundle\Entity\Repository\IDeviceRepository;
use PM\ApiBundle\Entity\Repository\IImageRepository;
use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\File\UploadedFile;
use Symfony\Component\HttpFoundation\Response;


class ImageController
    extends BaseController
{
    /**
     * @var \PM\ApiBundle\Entity\Repository\IImageRepository
     */
    private $imageRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IDeviceRepository
     */
    private $deviceRepository;
    /**
     * @var DeviceController
     */
    private $deviceController;

    public function __construct(
        LoggerInterface $logger,
        IImageRepository $imageRepository,
        IDeviceRepository $deviceRepository,
        DeviceController $deviceController
    )
    {
        parent::__construct($logger);
        $this->imageRepository = $imageRepository;
        $this->deviceRepository = $deviceRepository;
        $this->deviceController = $deviceController;
    }

    public function postImageAction(Request $request, $deviceId, $externalId = null)
    {
        $this->logRequest($request);

        if ($request->files->count() != 1)
        {
            throw new \Exception('Should be only 1 file');
        }

        $device = $this->deviceRepository->findByExternalId($deviceId);

        if ($device == null)
        {
            $device = $this->deviceController->create($deviceId);
        }

        $targetDir = 'uploads/images';

        $isNew = false;
        if ($externalId == null)
        {
            $isNew = true;
            $externalId = UUID::v4();
        }

        $newFilePath = "$targetDir/$externalId.jpg";

        move_uploaded_file ($_FILES['upload']['tmp_name'], $newFilePath);

        $result = $this->saveAction($newFilePath, $externalId, $externalId, $device, $isNew);

        $this->resizeImage($result, $newFilePath);

        $response = new JsonResponse($result);

        $this->logResponse($response);

        return $response;
    }

    /**
     * @param Image $image
     * @param $file
     */
    private function resizeImage($image, $file)
    {
        $thumbPath = "uploads/images/thumb/".$image->getExternalId().'.jpg';
        $width=200;
        $size=GetimageSize($file);
        $height=round($width*$size[1]/$size[0]);

        $images_orig = ImageCreateFromJPEG($file);
        $photoX = ImagesX($images_orig);
        $photoY = ImagesY($images_orig);
        $images_fin = ImageCreateTrueColor($width, $height);
        ImageCopyResampled($images_fin, $images_orig, 0, 0, 0, 0, $width+1, $height+1, $photoX, $photoY);

        ImageJPEG($images_fin, $thumbPath, 50);
        ImageDestroy($images_orig);
        ImageDestroy($images_fin);
    }

    public function getImageAction($externalId)
    {
        return $this->getImage($externalId);
    }

    public function getImageThumbAction($externalId)
    {
        return $this->getImage($externalId, true);
    }

    private function getImage($externalId, $isThumb = false)
    {
        $image = $this->imageRepository->findByExternalId($externalId);

        if ($image == null)
        {
            throw new ImageNotFoundException($externalId);
        }

        if (file_exists($image->getPath()))
        {
            $headers = array(
                'Content-Type'          => 'image/jpg',
//                'Content-Disposition'   => 'inline; filename="'+$image->getName()+'.jpg"'
            );

            $path = $image->getPath();
            if ($isThumb)
                $path = str_replace('/images/', '/images/thumb/', $path);

            try{
                $data = @file_get_contents($path);
            }
            catch(\Exception $ex)
            {}

            if ($data === FALSE)
            {
                $this->resizeImage($image, $image->getPath());
                $data = file_get_contents($path);
            }

            if (isset($data) && $data != null)
            {
                return new Response($data, 200, $headers);
            }
        }

        return new Response('Image was not found.', 404);
    }

    private function saveAction($path, $name, $externalId, $device, $isNew)
    {
        if ($isNew)
        {
            $image = new Image();
            $image->setExternalId($externalId);
        }
        else
        {
            $image = $this->imageRepository->findByExternalId($externalId);
            if ($image == null)
            {
                $image = new Image();
                $image->setExternalId($externalId);
            }
        }

        $image->setPath($path);
        $image->setName($name);
        $image->setLastUpdatedByDevice($device);

        $this->imageRepository->save($image);

        return $image;
    }

    public function getAction($externalId)
    {
        $image = $this->imageRepository->findByExternalId($externalId);

        return $image;
    }
}
