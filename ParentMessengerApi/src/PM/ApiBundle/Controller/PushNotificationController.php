<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Common\Exception\DeviceNotFoundException;
use PM\ApiBundle\Entity\Repository\IDeviceRepository;
use PM\ApiBundle\Entity\Repository\IPlatformRepository;
use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class PushNotificationController
    extends BaseController
{
    /**
     * @var DeviceController
     */
    private $deviceController;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IDeviceRepository
     */
    private $deviceRepository;
    /**
     * @var IPlatformRepository
     */
    private $platformRepository;

    public function __construct(
        LoggerInterface $logger,
        IPlatformRepository $platformRepository,
        IDeviceRepository $deviceRepository,
        DeviceController $deviceController
    )
    {
        parent::__construct($logger);

        $this->deviceController = $deviceController;
        $this->deviceRepository = $deviceRepository;
        $this->platformRepository = $platformRepository;
    }

    public function registerDeviceForNotificationAction(Request $request, $deviceId, $platform, $deviceToken, $enabled)
    {
        $this->logRequest($request);

        $device = $this->deviceRepository->findByExternalId($deviceId);
        $platformEntity = $this->platformRepository->findByName($platform);

        if ($device == null)
        {
            throw new DeviceNotFoundException($deviceId);
        }

        $device->setToken($deviceToken);
        $device->setIsNotificationEnabled($enabled == '1' ? true : false);
        $device->setPlatformId($platformEntity->getId());

        $this->deviceRepository->save($device);

        $result = null;

        $httpResponse = new JsonResponse($result);
        $this->logResponse($httpResponse);

        return $httpResponse;
    }
}