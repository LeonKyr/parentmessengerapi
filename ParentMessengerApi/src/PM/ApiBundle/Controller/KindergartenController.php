<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Common\Exception\DeviceNotFoundException;
use PM\ApiBundle\Common\Exception\DeviceNotSuppliedException;
use PM\ApiBundle\Entity\Device;
use PM\ApiBundle\Entity\Kindergarten;
use PM\ApiBundle\Entity\Repository\IDeviceRepository;
use PM\ApiBundle\Entity\Repository\IKindergartenRepository;
use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class KindergartenController
    extends BaseController
{
    /**
     * @var \PM\ApiBundle\Entity\Repository\IKindergartenRepository
     */
    private $kindergartenRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IDeviceRepository
     */
    private $deviceRepository;
    /**
     * @var DeviceController
     */
    private $deviceController;

    public function __construct(LoggerInterface $logger,
                                IKindergartenRepository $kindergartenRepository,
                                IDeviceRepository $deviceRepository,
                                DeviceController $deviceController)
    {
        $this->kindergartenRepository = $kindergartenRepository;
        parent::__construct($logger);
        $this->deviceRepository = $deviceRepository;
        $this->deviceController = $deviceController;
    }

    public function getAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $kindergarten = $this->kindergartenRepository->findByExternalId($externalId);

        $response = new JsonResponse($kindergarten);

        $this->logResponse($response);

        return $response;
    }

    public function changeStatusAction(Request $request, $externalId, $status)
    {
        $this->logRequest($request);

        $kindergarten = $this->kindergartenRepository->findByExternalId($externalId);

        $kindergarten->setStatusFromString($status);

        $this->kindergartenRepository->save($kindergarten);

        $response = new JsonResponse($kindergarten);

        $this->logResponse($response);

        return $response;
    }

    /*
{
"name": "TestKindergarten",
"email": "kinda@garten.mail",
"phone": "123-456-678",
"deviceId": "1",
"address": "Calle de Barcelona 123",
"contact_person" : "KA contact person name"
}

password is optional for change and mandatory for CREATION
    */
    public function changeAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $data = $this->getJsonData($request);

        if (!isset($data['device']) ||
            $data['device']['id'] == null)
        {
            throw new DeviceNotSuppliedException();
        }

        $device = $this->deviceRepository->findByExternalId($data['device']['id']);

        if ($device == null)
        {
            $device = $this->deviceController->create($data['device']['id']);
        }

        $deviceId = $device->getId();

        $kindergarten = $this->changeKindergarten($data, $externalId, $deviceId);
        $kindergarten->setUpdatedAt(time());

        $response = new JsonResponse($kindergarten);

        $this->logResponse($response);

        return $response;
    }

    public function changeKindergarten($data, $externalId, $deviceId)
    {
        if ($externalId == null &&
            isset($data['id']))
        {
            $externalId = $data['id'];
        }

        if ($externalId != null)
        {
            $kindergarten = $this->kindergartenRepository->findByExternalId($externalId);
            $kindergarten->setName($data['name']);
            $kindergarten->setEmail($data['email']);
            $kindergarten->setAddress($data['address']);
            $kindergarten->setContactPerson($data['contact_person']);
            $kindergarten->setPhone($data['phone']);
            $kindergarten->setUpdatedAt(time());

            if (isset($data['password']))
                $kindergarten->setPassword($data['password']);

        }
        else
        {
            $externalId = UUID::v4();

            $kindergarten = new Kindergarten(
                $data['name'], $data['email'], $data['password'], $externalId,
                $data['phone'], $data['contact_person'], $data['address']);
        }

        $kindergarten->setLastUpdateByDeviceId($deviceId);

        $this->kindergartenRepository->save($kindergarten);

        return $kindergarten;
    }
}