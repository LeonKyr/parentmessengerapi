<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Common\Exception\ChildNotFoundException;
use PM\ApiBundle\Common\Exception\DeviceNotSuppliedException;
use PM\ApiBundle\Common\Exception\GroupNotFoundException;
use PM\ApiBundle\Common\Exception\KindergartenNotFoundException;
use PM\ApiBundle\Entity\Child;
use PM\ApiBundle\Entity\ChildContact;
use PM\ApiBundle\Entity\Repository\IChildContactRepository;
use PM\ApiBundle\Entity\Repository\IChildRepository;
use PM\ApiBundle\Entity\Repository\IDeviceRepository;
use PM\ApiBundle\Entity\Repository\IGroupRepository;
use PM\ApiBundle\Entity\Repository\IImageRepository;
use PM\ApiBundle\Entity\Repository\IKindergartenRepository;
use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ChildController
    extends BaseController
{
    /**
     * @var \PM\ApiBundle\Entity\Repository\IChildRepository
     */
    private $childRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IKindergartenRepository
     */
    private $kindergartenRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IGroupRepository
     */
    private $groupRepository;
    /**
     * @var ImageController
     */
    private $imageController;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IDeviceRepository
     */
    private $deviceRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IChildContactRepository
     */
    private $childContactRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IImageRepository
     */
    private $imageRepository;

    public function __construct(
        LoggerInterface $logger,
        IKindergartenRepository $kindergartenRepository,
        IChildRepository $childRepository,
        IGroupRepository $groupRepository,
        IDeviceRepository $deviceRepository,
        IChildContactRepository $childContactRepository,
        ImageController $imageController,
        IImageRepository $imageRepository
    )
    {
        parent::__construct($logger);

        $this->childRepository = $childRepository;
        $this->kindergartenRepository = $kindergartenRepository;
        $this->groupRepository = $groupRepository;
        $this->imageController = $imageController;
        $this->deviceRepository = $deviceRepository;
        $this->childContactRepository = $childContactRepository;
        $this->imageRepository = $imageRepository;
    }

    public function getAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $result = $this->getChildByExternalId($externalId);

        $response = new JsonResponse($result);

        $this->logResponse($response);

        return $response;
    }

    public function getChildByExternalId($externalId)
    {
        $result = $this->childRepository->findByExternalId($externalId);

        if ($result == null)
        {
            throw new ChildNotFoundException($externalId);
        }
        $cc = $this->childContactRepository->findByChildId($result->getId());
        $result->setChildContacts($cc);

        return $result;
    }

    /*
{
    "id": "cce1338a-69b9-4659-9022-19a6b72e52d8",
    "name": "TestChild",
    "kindergartenId": "1",
    "code": "\"2314227308\"",
    "hasListeners": false,
    "dob": 123123123123,
    "contacts": [
        {
            "id": "7f17e97f-88dc-42e7-aa17-5dc66d836de6",
            "name": "name1--changed",
            "phone": "phone1",
            "email": "email1",
            "childId": "cce1338a-69b9-4659-9022-19a6b72e52d8"
        },
        {
            "id": "9273aeb6-8dee-4fb4-8d14-cb3ec6807818",
            "name": "name2",
            "phone": "phone2-changed",
            "email": null,
            "childId": "cce1338a-69b9-4659-9022-19a6b72e52d8"
        }
    ]
}
    */

    public function changeAction(Request $request, $kindergartenExternalId, $externalId)
    {
        $this->logRequest($request);

        $data = $this->getJsonData($request);

        if (!isset($data['device']) ||
            $data['device']['id'] == null)
        {
            throw new DeviceNotSuppliedException();
        }

        $device = $this->deviceRepository->findByExternalId($data['device']['id']);

        $deviceId = $device->getId();

        $child = $this->changeChild($data, $kindergartenExternalId, $deviceId, $externalId);

        $response = new JsonResponse($child);

        $this->logResponse($response);

        return $response;
    }
    /* RESPONSE
{
    "id": "cce1338a-69b9-4659-9022-19a6b72e52d8",
    "name": "TestChild",
    "kindergartenId": "1",
    "code": "\"2314227308\"",
    "hasListeners": false,
    "contacts": [
        {
            "id": "7f17e97f-88dc-42e7-aa17-5dc66d836de6",
            "name": "name1--changed",
            "phone": "phone1",
            "email": "email1",
            "childId": "cce1338a-69b9-4659-9022-19a6b72e52d8"
        },
        {
            "id": "9273aeb6-8dee-4fb4-8d14-cb3ec6807818",
            "name": "name2",
            "phone": "phone2-changed",
            "email": null,
            "childId": "cce1338a-69b9-4659-9022-19a6b72e52d8"
        }
    ]
}
    */

    private function getRelationId($relation)
    {
        switch($relation)
        {
            case "father": $result = 1; break;
            case "mother": $result = 2; break;
            case "sister": $result = 3; break;
            case "brother": $result = 4; break;
            case "grandmother": $result = 5; break;
            case "grandfather": $result = 6; break;
            default: throw new \Exception("Conversion for $relation was not implemented.");
        }
        return $result;
    }

    public function changeChild(array $data, $kindergartenExternalId, $deviceId, $externalId = null)
    {
        if (isset($data['id']))
        {
            $externalId = $data['id'];
        }

        if ($externalId != null)
        {
            $child = $this->childRepository->findByExternalId($externalId);
            if ($child == null)
            {
                throw new ChildNotFoundException($externalId);
            }
        }
        else
        {
            $child = new Child();
            $child->setExternalId(UUID::v4());
            $child->setCode(rand(1000000,10000000000));

            $kindergarten = $this->kindergartenRepository->findByExternalId($kindergartenExternalId);
            if ($kindergarten == null)
            {
                throw new KindergartenNotFoundException($kindergartenExternalId);
            }
            $child->setKindergarten($kindergarten);
        }

        if (isset($data['group_id']))
        {
            $group = $this->groupRepository->findByExternalId($data['group_id']);
            $child->setGroup($group);
        }

        $child->setName($data['name']);
        $child->setDob($data['dob']);
        $child->setLastUpdatedByDeviceId($deviceId);
        $child->setUpdatedAt(time());


        if (isset($data['image']) &&
            $data['image'] != null &&
            isset($data['image']['id']) &&
            $data['image']['id'] != null)
        {
            $image = $this->imageRepository->findByExternalId($data['image']['id']);

            $child->setImage($image);
        }

        $this->childRepository->save($child);

        // save contacts
        if (isset($data['contacts']))
        {
            $childContacts = array();
            $contacts = $data['contacts'];
            foreach ($contacts as $contact)
            {
                if (isset($contact['id']))
                {
                    $ccExternalId = $contact['id'];
                    $cc = $this->childContactRepository->findByExternalId($ccExternalId);

                    if ($cc->getChildId() != $child->getId())
                    {
                        throw new \Exception("Contact does not belong to this child.");
                    }
                }
                else
                {
                    $cc = new ChildContact();
                    $cc->setExternalId(UUID::v4());
                    $cc->setChild($child);
                }

                $cc->setUpdatedAt(time());
                $cc->setLastUpdatedByDeviceId($deviceId);

                if (isset($contact['name'])) $cc->setName($contact['name']);
                if (isset($contact['email'])) $cc->setEmail($contact['email']);
                if (isset($contact['phone'])) $cc->setPhone($contact['phone']);
                $cc->setRelationId($this->getRelationId($contact['relation']));

                $this->childContactRepository->save($cc);

                array_push($childContacts, $cc);
            }
            $child->setChildContacts($childContacts);
        }

        return $child;
    }

    public function putChildIntoGroupAction(Request $request, $childExternalId, $groupExternalId)
    {
        $this->logRequest($request);

        $child = $this->childRepository->findByExternalId($childExternalId);
        if ($child == null)
        {
            throw new ChildNotFoundException($childExternalId);
        }

        $group = $this->groupRepository->findByExternalId($groupExternalId);
        if ($group == null)
        {
            throw new GroupNotFoundException($groupExternalId);
        }

        $child->setGroupId($group->getId());

        $this->childRepository->save($child);

        $response = new JsonResponse(array(
            'childExternalId' => $child->getExternalId(),
            'groupExternalId' => $group->getExternalId(),
            ));

        $this->logResponse($response);

        return $response;
    }

    public function removeChildFromGroupAction(Request $request, $childExternalId, $groupExternalId)
    {
        $this->logRequest($request);

        $child = $this->childRepository->findByExternalId($childExternalId);
        if ($child == null)
        {
            throw new ChildNotFoundException($childExternalId);
        }

        $group = $this->groupRepository->findByExternalId($groupExternalId);
        if ($group == null)
        {
            throw new GroupNotFoundException($groupExternalId);
        }

        $child->setGroupId(null);

        $this->childRepository->save($child);

        $response = new JsonResponse(array(
            'childExternalId' => $child->getExternalId(),
            'groupExternalId' => 'nil',
        ));

        $this->logResponse($response);

        return $response;
    }

    public function childrenAction(Request $request, $groupExternalId)
    {
        $this->logRequest($request);

        $group = $this->groupRepository->findByExternalId($groupExternalId);

        if ($group == null)
        {
            throw new GroupNotFoundException($groupExternalId);
        }

        $result = $this->childRepository->findByGroupId($group->getId());

        $response = new JsonResponse($result);

        $this->logResponse($response);

        return $response;
    }

    public function deleteAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $child = $this->childRepository->findByExternalId($externalId);

        // TODO: delete a teacher

        $response = new JsonResponse(array('externalId' => $child->getExternalId()));

        $this->logResponse($response);

        return $response;
    }
}


