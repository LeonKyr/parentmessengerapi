<?php
namespace PM\ApiBundle\Controller;

class LogController
    extends BaseController
{
    public function __construct(
        LoggerInterface $logger
    )
    {
        parent::__construct($logger);
    }
}