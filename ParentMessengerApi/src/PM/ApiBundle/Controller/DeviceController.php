<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Entity\Device;
//use PM\ApiBundle\Entity\Platform;
use PM\ApiBundle\Entity\Repository\IDeviceRepository;
use PM\ApiBundle\Entity\Repository\IPlatformRepository;
use Psr\Log\LoggerInterface;

class DeviceController
    extends BaseController
{
    /**
     * @var \PM\ApiBundle\Entity\Repository\IDeviceRepository
     */
    private $deviceRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IPlatformRepository
     */
    private $platformRepository;

    public function __construct(
        LoggerInterface $logger,
        IDeviceRepository $deviceRepository,
        IPlatformRepository $platformRepository
    )
    {
        parent::__construct($logger);

        $this->deviceRepository = $deviceRepository;
        $this->platformRepository = $platformRepository;
    }

    public function getAction($externalId)
    {
        $this->logRequest($externalId);

        $device = $this->deviceRepository->findByExternalId($externalId);

        return $device;
    }

    public function create($externalId)
    {
        $device = $this->deviceRepository->findByExternalId($externalId);
        if ($device != null)
        {
            // already registered
            return $device;
        }

//        $platform = $this->platformRepository->findByName($platformName);
//
//        if ($platform == null)
//        {
//            $platform = new Platform($platformName);
//            $this->platformRepository->save($platform);
//        }
//
//        $platformId = $platform->getId();

        $device = new Device();
        $device->setExternalId($externalId);
//        $device->setPlatformId($platformId);

        $this->deviceRepository->save($device);

        return $device;
    }
}