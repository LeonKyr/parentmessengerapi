<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Common\Exception\ChildNotFoundException;
use PM\ApiBundle\Common\Exception\DeviceNotSuppliedException;
use PM\ApiBundle\Common\Exception\GroupNotFoundException;
use PM\ApiBundle\Common\Exception\KindergartenNotFoundException;
use PM\ApiBundle\Entity\Child;
use PM\ApiBundle\Entity\ChildContact;
use PM\ApiBundle\Entity\Repository\IChildContactRepository;
use PM\ApiBundle\Entity\Repository\IChildRepository;
use PM\ApiBundle\Entity\Repository\IDeviceRepository;
use PM\ApiBundle\Entity\Repository\IGroupRepository;
use PM\ApiBundle\Entity\Repository\IKindergartenRepository;
use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ChildContactController
    extends BaseController
{
    /**
     * @var \PM\ApiBundle\Entity\Repository\IChildRepository
     */
    private $childRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IDeviceRepository
     */
    private $deviceRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IChildContactRepository
     */
    private $childContactRepository;

    public function __construct(
        LoggerInterface $logger,
        IChildRepository $childRepository,
        IDeviceRepository $deviceRepository,
        IChildContactRepository $childContactRepository
    )
    {
        parent::__construct($logger);

        $this->childRepository = $childRepository;
        $this->deviceRepository = $deviceRepository;
        $this->childContactRepository = $childContactRepository;
    }

    public function getAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $result = $this->childContactRepository->findByExternalId($externalId);

        $response = new JsonResponse($result);

        $this->logResponse($response);

        return $response;
    }

    public function changeAction(Request $request, $childExternalId, $externalId)
    {
        $this->logRequest($request);

        $data = $this->getJsonData($request);

        if (!isset($data['device']) ||
            $data['device']['id'] == null)
        {
            throw new DeviceNotSuppliedException();
        }

        $device = $this->deviceRepository->findByExternalId($data['device']['id']);

        $deviceId = $device->getId();

        $child = $this->childRepository->findByExternalId($childExternalId);

        if ($child == null)
        {
            throw new ChildNotFoundException($childExternalId);
        }

        if ($externalId != null)
        {
            $cc = $this->childContactRepository->findByExternalId($externalId);

            if ($cc->getChildId() != $child->getId())
            {
                throw new \Exception("Contact does not belong to this child.");
            }
        }
        else
        {
            $cc = new ChildContact();
            $cc->setExternalId(UUID::v4());
            $cc->setChild($child);
        }

        $cc->setUpdatedAt(time());
        $cc->setLastUpdatedByDeviceId($deviceId);

        if (isset($data['name']))
            $cc->setName($data['name']);
        if (isset($data['email']))
            $cc->setEmail($data['email']);
        if (isset($data['phone']))
            $cc->setPhone($data['phone']);
        if (isset($data['relation']))
            $cc->setRelationId($this->getRelationId($data['relation']));

        $this->childContactRepository->save($cc);

        $response = new JsonResponse($cc);

        $this->logResponse($response);

        return $response;
    }

    private function getRelationId($relation)
    {
        switch($relation)
        {
            case "father": $result = 1; break;
            case "mother": $result = 2; break;
            case "sister": $result = 3; break;
            case "brother": $result = 4; break;
            case "grandmother": $result = 5; break;
            case "grandfather": $result = 6; break;
            default: throw new \Exception("Conversion for $relation was not implemented.");
        }
        return $result;
    }

    public function deleteAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $cc = $this->childContactRepository->findByExternalId($externalId);

        // TODO: delete a child ontact

        $response = new JsonResponse($cc);

        $this->logResponse($response);

        return $response;
    }
}