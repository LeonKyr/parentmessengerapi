<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\HttpKernel\Exception\BadRequestHttpException;
use Monolog\Logger;

abstract class BaseController
    extends Controller
{
    /**
     * @var \Psr\Log\LoggerInterface
     */
    private $logger;

    function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    /**
     * @return \Psr\Log\LoggerInterface
     */
    protected function getLogger()
    {
        return $this->logger;
    }

    protected function getJsonData(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        if (json_last_error() || empty($data)) {
            throw new BadRequestHttpException('Invalid JSON received');
        }

        return $data;
    }

    public function __call($method, $args)
    {
        try
        {
            return call_user_func_array(array($this, $method), $args);
        }
        catch (\Exception $e)
        {
            $this->getLogger()->error(__METHOD__ . ' Ex: ' . $e->getMessage());

            return new JsonResponse(
                array(
                    'error' => $e->getMessage()
                ),
                500
            );
        }
    }

    /**
     * @param Request $request
     */
    protected function logRequest(Request $request)
    {
        $this->getLogger()->debug('Url '.$request->getQueryString().$this->getCallerMethod() . ' Request.',
            array(
                $request->getQueryString(),
                $request->getContent()
            )
        );
    }

    /**
     * @param Response $response
     */
    protected function logResponse(Response $response)
    {
        $this->getLogger()->debug($this->getCallerMethod() . ' Response.',
            array(
                $response->getContent()
            ));
    }

    private function getCallerMethod()
    {
        $callers = debug_backtrace();
        return $callers[2]['class'] . '::' . $callers[2]['function'];
    }
}