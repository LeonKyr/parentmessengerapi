<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Entity\Feedback;
use PM\ApiBundle\Entity\Repository\IFeedbackRepository;
use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class FeedbackController
    extends BaseController
{
    /**
     * @var \PM\ApiBundle\Entity\Repository\IFeedbackRepository
     */
    private $feedbackRepository;

    public function __construct(
        LoggerInterface $logger,
        IFeedbackRepository $feedbackRepository
    )
    {
        parent::__construct($logger);
        $this->feedbackRepository = $feedbackRepository;
    }

    public function feedbackAction(Request $request, $entity, $externalId)
    {
        $this->logRequest($request);

        $data = $this->getJsonData($request);

        $feedback = new Feedback();
        $feedback->setText($data['text']);
        $feedback->setSource($data['source']);
        $feedback->setEntity($entity);
        $feedback->setExternalId($externalId != null ? UUID::v4() : $externalId);

        $this->feedbackRepository->save($feedback);

        $response = new JsonResponse(array(
            'id' => $feedback->getExternalId()
        ));

        $this->logResponse($response);

        return $response;
    }

    public function thankYouAction(Request $request, $entity, $externalId, $eventId)
    {
        $this->logRequest($request);

        $response = new JsonResponse();

        $this->logResponse($response);

        return $response;
    }
}