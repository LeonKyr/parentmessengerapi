<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Common\Exception\DeviceNotSuppliedException;
use PM\ApiBundle\Common\Exception\KindergartenNotFoundException;
use PM\ApiBundle\Common\Exception\TeacherNotFoundException;
use PM\ApiBundle\Entity\Repository\IDeviceRepository;
use PM\ApiBundle\Entity\Repository\IImageRepository;
use PM\ApiBundle\Entity\Repository\IKindergartenRepository;
use PM\ApiBundle\Entity\Repository\ITeacherRepository;
use PM\ApiBundle\Entity\Teacher;
use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class TeacherController
    extends BaseController
{
    /**
     * @var \PM\ApiBundle\Entity\Repository\ITeacherRepository
     */
    private $teacherRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IKindergartenRepository
     */
    private $kindergartenRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IDeviceRepository
     */
    private $deviceRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IImageRepository
     */
    private $imageRepository;

    public function __construct(
        LoggerInterface $logger,
        IKindergartenRepository $kindergartenRepository,
        ITeacherRepository $teacherRepository,
        IDeviceRepository $deviceRepository,
        IImageRepository $imageRepository
    )
    {
        parent::__construct($logger);
        $this->teacherRepository = $teacherRepository;
        $this->kindergartenRepository = $kindergartenRepository;
        $this->deviceRepository = $deviceRepository;
        $this->imageRepository = $imageRepository;
    }

    public function getAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $teacher = $this->teacherRepository->findByExternalId($externalId);

        $response = new JsonResponse($teacher);

        $this->logResponse($response);

        return $response;
    }

    public function changeTeacher($data, $kindergartenExternalId, $deviceId, $externalId = null)
    {
        if (isset($data['id']))
        {
            $externalId = $data['id'];
        }

        if ($externalId != null)
        {
            $teacher = $this->teacherRepository->findByExternalId($externalId);
            if ($teacher == null)
                throw new TeacherNotFoundException($externalId);

            if (isset($data['name']) && $data['name'] != null)
            {
                $teacher->setName($data['name']);
            }
        }
        else
        {
            $teacher = new Teacher();

            $teacher->setName($data['name']);
            $teacher->setExternalId(UUID::v4());

            $kindergarten = $this->kindergartenRepository->findByExternalId($kindergartenExternalId);
            if ($kindergarten == null)
            {
                throw new KindergartenNotFoundException($kindergartenExternalId);
            }
            $teacher->setKindergarten($kindergarten);
        }


        $teacher->setUpdatedAt(time());
        $teacher->setLastUpdatedByDeviceId($deviceId);

        if (isset($data['image']) &&
            $data['image'] != null &&
            isset($data['image']['id']) &&
            $data['image']['id'] != null)
        {
            $image = $this->imageRepository->findByExternalId($data['image']['id']);

            $teacher->setImage($image);
        }

        $this->teacherRepository->save($teacher);

        return $teacher;
    }

    public function changeAction(Request $request, $kindergartenExternalId, $externalId)
    {
        $this->logRequest($request);

        $data = $this->getJsonData($request);

        if (!isset($data['device']) ||
            $data['device']['id'] == null)
        {
            throw new DeviceNotSuppliedException();
        }

        $device = $this->deviceRepository->findByExternalId($data['device']['id']);

        $deviceId = $device->getId();

        $teacher = $this->changeTeacher($data, $kindergartenExternalId, $deviceId, $externalId);

        $response = new JsonResponse($teacher);

        $this->logResponse($response);

        return $response;
    }

    public function deleteAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $teacher = $this->teacherRepository->findByExternalId($externalId);

        // TODO: delete a teacher

        $response = new JsonResponse(array('externalId' => $teacher->getExternalId()));

        $this->logResponse($response);

        return $response;
    }
}