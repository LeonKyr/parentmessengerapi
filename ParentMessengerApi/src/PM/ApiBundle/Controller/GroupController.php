<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Common\Exception\DeviceNotFoundException;
use PM\ApiBundle\Common\Exception\DeviceNotSuppliedException;
use PM\ApiBundle\Common\Exception\KindergartenNotFoundException;
use PM\ApiBundle\Entity\Group;
use PM\ApiBundle\Entity\Repository\IDeviceRepository;
use PM\ApiBundle\Entity\Repository\IGroupRepository;
use PM\ApiBundle\Entity\Repository\IKindergartenRepository;
use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class GroupController
    extends BaseController
{
    /**
     * @var \PM\ApiBundle\Entity\Repository\IKindergartenRepository
     */
    private $kindergartenRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IGroupRepository
     */
    private $groupRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IDeviceRepository
     */
    private $deviceRepository;

    public function __construct(
        LoggerInterface $logger,
        IKindergartenRepository $kindergartenRepository,
        IGroupRepository $groupRepository,
        IDeviceRepository $deviceRepository
    )
    {
        parent::__construct($logger);

        $this->kindergartenRepository = $kindergartenRepository;
        $this->groupRepository = $groupRepository;
        $this->deviceRepository = $deviceRepository;
    }

    public function getAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $result = $this->groupRepository->findByExternalId($externalId);

        $response = new JsonResponse($result);

        $this->logResponse($response);

        return $response;
    }

    /*
{
"name": "TestGroup",
"deviceId": "1",
"color": 232323
}
    */
    public function changeAction(Request $request, $kindergartenExternalId, $groupExternalId)
    {
        $this->logRequest($request);

        $data = $this->getJsonData($request);

        if (!isset($data['device']) ||
            $data['device']['id'] == null)
        {
            throw new DeviceNotSuppliedException();
        }

        $device = $this->deviceRepository->findByExternalId($data['device']['id']);

        if ($device == null)
        {
            throw new DeviceNotFoundException($data['device']['id']);
        }

        $group = $this->changeGroup($data, $kindergartenExternalId, $device->getId(), $groupExternalId);

        $response = new JsonResponse($group);
        $this->logResponse($response);

        return $response;
    }

    public function changeGroup($data, $kindergartenExternalId, $deviceId, $externalId = null)
    {
        if (isset($data['id']))
        {
            $externalId = $data['id'];
        }

        if ($externalId != null)
        {
            $value = $this->groupRepository->findByExternalId($externalId);
        }
        else
        {
            $value = new Group();

            $value->setExternalId(UUID::v4());

            $kindergarten = $this->kindergartenRepository->findByExternalId($kindergartenExternalId);
            if ($kindergarten == null)
            {
                throw new KindergartenNotFoundException($kindergartenExternalId);
            }

            $value->setKindergarten($kindergarten);
        }

        $value->setUpdatedAt(time());
        $value->setLastUpdatedByDeviceId($deviceId);

        $value->setName($data['name']);

        if (isset($data['color']))
        {
            $value->setColor($data['color']);
        }

        $this->groupRepository->save($value);

        return $value;
    }

    public function deleteAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $value = $this->groupRepository->findByExternalId($externalId);

        // TODO: delete a group

        $response = new JsonResponse(array('externalId' => $value->getExternalId()));

        $this->logResponse($response);

        return $response;
    }
}