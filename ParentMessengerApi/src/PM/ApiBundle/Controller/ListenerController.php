<?php
namespace PM\ApiBundle\Controller;


use PM\ApiBundle\Common\Exception\ChildNotFoundException;
use PM\ApiBundle\Common\Exception\DeviceNotSuppliedException;
use PM\ApiBundle\Common\Exception\ListenerNotFoundException;
use PM\ApiBundle\Entity\Child2Listener;
use PM\ApiBundle\Entity\Listener;
use PM\ApiBundle\Entity\Listener2Device;
use PM\ApiBundle\Entity\Repository\IChild2ListenerRepository;
use PM\ApiBundle\Entity\Repository\IChildRepository;
use PM\ApiBundle\Entity\Repository\IDeviceRepository;
use PM\ApiBundle\Entity\Repository\IImageRepository;
use PM\ApiBundle\Entity\Repository\IListener2DeviceRepository;
use PM\ApiBundle\Entity\Repository\IListenerRepository;
use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class ListenerController
    extends BaseController
{
    /**
     * @var \PM\ApiBundle\Entity\Repository\IListenerRepository
     */
    private $listenerRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IChild2ListenerRepository
     */
    private $child2listenerRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IChildRepository
     */
    private $childRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IImageRepository
     */
    private $imageRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IDeviceRepository
     */
    private $deviceRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IListener2DeviceRepository
     */
    private $listener2deviceRepository;


    public function __construct(
        LoggerInterface $logger,
        IListenerRepository $listenerRepository,
        IChildRepository $childRepository,
        IChild2ListenerRepository $child2listenerRepository,
        IImageRepository $imageRepository,
        IDeviceRepository $deviceRepository,
        DeviceController $deviceController,
        IListener2DeviceRepository $listener2deviceRepository
    )
    {
        parent::__construct($logger);

        $this->listenerRepository = $listenerRepository;
        $this->child2listenerRepository = $child2listenerRepository;
        $this->childRepository = $childRepository;
        $this->imageRepository = $imageRepository;
        $this->deviceController = $deviceController;
        $this->deviceRepository = $deviceRepository;
        $this->listener2deviceRepository = $listener2deviceRepository;
    }

    public function getAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $listener = $this->listenerRepository->findByExternalId($externalId);
        $children = $this->getChildrenOfListener($externalId);

        $response = new JsonResponse(
            array(
                'listener' => $listener,
                'children' => $children
            ));

        $this->logResponse($response);

        return $response;
    }

    public function changeAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $data = $this->getJsonData($request);

        if (!isset($data['device']) ||
            $data['device']['id'] == null)
        {
            throw new DeviceNotSuppliedException();
        }

        $device = $this->deviceRepository->findByExternalId($data['device']['id']);

        if ($device == null)
        {
            $device = $this->deviceController->create($data['device']['id']);
        }

        if ($externalId != null)
        {
            $listener = $this->listenerRepository->findByExternalId($externalId);

            if (isset($data['name']) && $data['name'] != null)
            {
                $listener->setName($data['name']);
            }

            if (isset($data['email']) && $data['email'] != null)
            {
                $listener->setEmail($data['email']);
            }

            if (isset($data['password']) && $data['password'] != null)
            {
                $listener->setPassword($data['password']);
            }
        }
        else
        {
            if ($this->listenerRepository->findByEmail($data['data']) != null)
            {

            }

            $listener = new Listener($data['name'], $data['email'], $data['password'], UUID::v4());
        }

        if (isset($data['phone']) && $data['phone'] != null)
        {
            $listener->setPhone($data['phone']);
        }

        if (isset($data['image']) &&
            $data['image'] != null &&
            isset($data['image']['id']) &&
            $data['image']['id'] != null)
        {
            $image = $this->imageRepository->findByExternalId($data['image']['id']);

            $listener->setImage($image);
        }

        $deviceId = $device->getId();


        $this->listenerRepository->save($listener);

        if ($this->listener2deviceRepository->findByListenerIdAndDeviceId($listener->getId(), $deviceId) == null)
        {
            $l2d = new Listener2Device();
            $l2d->setDevice($device);
            $l2d->setListener($listener);

            $this->listener2deviceRepository->save($l2d);
        }

        $response = new JsonResponse($listener);

        $this->logResponse($response);

        return $response;
    }

    public function addChildToListenerAction(Request $request, $externalId, $childCode)
    {
        $this->logRequest($request);

        $child = $this->childRepository->findByCode($childCode);
        if ($child == null)
        {
            throw new ChildNotFoundException($childCode);
        }
        $listener = $this->listenerRepository->findByExternalId($externalId);
        if ($listener == null)
        {
            throw new ListenerNotFoundException($externalId);
        }

        $c2l = $this->child2listenerRepository->findByListenerIdAndChildId($listener->getId(), $child->getId());

        if ($c2l == null)
        {
            $c2l = new Child2Listener();
            $c2l->setChild($child);
            $c2l->setListener($listener);
            $this->child2listenerRepository->save($c2l);
        }

        $response = new JsonResponse($c2l->getChild());

        $this->logResponse($response);

        return $response;
    }

    public function getChildrenOfListenerAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $children = $this->getChildrenOfListener($externalId);

        $response = new JsonResponse($children);

        $this->logResponse($response);

        return $response;
    }

    private function getChildrenOfListener($externalId)
    {
        $children2listener = $this->child2listenerRepository->findByListenerExternalId($externalId);

        $children = array();
        foreach($children2listener as $c2l)
        {
            array_push($children, $c2l->getChild());
        }
        return $children;
    }
}