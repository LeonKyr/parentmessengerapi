<?php
namespace PM\ApiBundle\Controller;

use Exception;
use PM\ApiBundle\Common\Exception\ActivityDateNotFoundException;
use PM\ApiBundle\Common\Exception\ActivityNotFoundException;
use PM\ApiBundle\Common\Exception\ActivityTypeNotFoundException;
use PM\ApiBundle\Common\Exception\ChildNotFoundException;
use PM\ApiBundle\Common\Exception\KindergartenNotFoundException;
use PM\ApiBundle\Common\Exception\ListenerNotFoundException;
use PM\ApiBundle\Common\Exception\TeacherNotFoundException;
use PM\ApiBundle\Entity\Activity;
use PM\ApiBundle\Entity\Activity2Child;
use PM\ApiBundle\Entity\ActivityDate;
use PM\ApiBundle\Entity\Repository\IActivity2ChildRepository;
use PM\ApiBundle\Entity\Repository\IActivityDateRepository;
use PM\ApiBundle\Entity\Repository\IActivityRepository;
use PM\ApiBundle\Entity\Repository\IActivityTypeRepository;
use PM\ApiBundle\Entity\Repository\IChild2ListenerRepository;
use PM\ApiBundle\Entity\Repository\IChildRepository;
use PM\ApiBundle\Entity\Repository\IKindergartenRepository;
use PM\ApiBundle\Entity\Repository\IListenerRepository;
use PM\ApiBundle\Entity\Repository\ITeacherRepository;
use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class ActivityController
    extends BaseController
{
    /**
     * @var \PM\ApiBundle\Entity\Repository\IKindergartenRepository
     */
    private $kindergartenRepository;
    /**
     * @var IActivityRepository
     */
    private $activityRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IChildRepository
     */
    private $childRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IChild2ListenerRepository
     */
    private $child2listenerRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\ITeacherRepository
     */
    private $teacherRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IActivity2ChildRepository
     */
    private $activity2childRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IActivityTypeRepository
     */
    private $activityTypeRepository;
    /**
     * @var IActivityDateRepository
     */
    private $activityDateRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IListenerRepository
     */
    private $listenerRepository;

    public function __construct(
        LoggerInterface $logger,
        IKindergartenRepository $kindergartenRepository,
        IActivityRepository $activityRepository,
        IActivity2ChildRepository $activity2childRepository,
        IActivityTypeRepository $activityTypeRepository,
        IActivityDateRepository $activityDateRepository,
        IChildRepository $childRepository,
        IChild2ListenerRepository $child2listenerRepository,
        ITeacherRepository $teacherRepository,
        IListenerRepository $listenerRepository
    )
    {
        parent::__construct($logger);

        $this->kindergartenRepository = $kindergartenRepository;
        $this->activityRepository = $activityRepository;
        $this->childRepository = $childRepository;
        $this->child2listenerRepository = $child2listenerRepository;
        $this->teacherRepository = $teacherRepository;
        $this->activity2childRepository = $activity2childRepository;
        $this->activityTypeRepository = $activityTypeRepository;
        $this->activityDateRepository = $activityDateRepository;
        $this->listenerRepository = $listenerRepository;
    }

    public function getActivitiesForListenerAction(Request $request, $listenerExternalId, $startDate, $limitCount)
    {
        $this->logRequest($request);

        $listener = $this->listenerRepository->findByExternalId($listenerExternalId);

        if ($listener == null)
            throw new ListenerNotFoundException($listenerExternalId);

        $activities = $this->activityRepository->findByListenerIdAndAndUpdatedAt($listener->getId(), $startDate, $limitCount);

        $httpResponse = new JsonResponse($activities);

        $this->logResponse($httpResponse);

        return $httpResponse;
    }

    public function getActivitiesForKindergartenAction(Request $request, $kindergartenExternalId, $startDate, $limitCount)
    {
        $this->logRequest($request);

        $kindergarten = $this->kindergartenRepository->findByExternalId($kindergartenExternalId);

        if ($kindergarten == null)
            throw new KindergartenNotFoundException($kindergartenExternalId);

        $activities = $this->activityRepository->findByKindergartenIdAndAndUpdatedAt($kindergarten->getId(), $startDate, $limitCount);

        $httpResponse = new JsonResponse($activities);

        $this->logResponse($httpResponse);

        return $httpResponse;
    }

    public function getActivityAction(Request $request, $externalId)
    {
        $this->logRequest($request);

        $result = $this->activityRepository->findByExternalId($externalId);

        $httpResponse = new JsonResponse(
            $result
        );

        $this->logResponse($httpResponse);

        return $httpResponse;
    }

    public function createActivityAction(Request $request, $externalId = null)
    {
        date_default_timezone_set("UTC");

        $this->logRequest($request);

        $data = $this->getJsonData($request);

        $activity = null;
        if ($externalId != null)
        {
            $activityExternalId = $externalId;
            $activity = $this->activityRepository->findByExternalId($activityExternalId);

            if ($activity == null)
                throw new ActivityNotFoundException($activityExternalId);
        }

        if ($activity == null)
        {
            $activity = new Activity();
            $activity->setCreatedAt(time());
            $activity->setExternalId(UUID::v4());
        }

        $activity->setUpdatedAt(time());
        $activity->setName($data['name']);
        $activity->setDescription($data['description']);

        if (isset($data['teacher_id']))
        {
            $teacherExternalId = $data['teacher_id'];

            $teacher = $this->teacherRepository->findByExternalId($teacherExternalId);

            $activity->setTeacher($teacher);
        }

        $activityType = $this->activityTypeRepository->findById($data['activity_type_id']);

        if ($activityType == null)
            throw new ActivityTypeNotFoundException($data['activity_type_id']);

        $activity->setActivityType($activityType);

        if (isset($data['is_active']))
        {
            $activity->setIsActive($data['is_active'] === true);
        }

        // delete old activity 2 child
        $a2clist = array();
        if ($activity->getId() > 0)
        {
            $a2clist = $this->activity2childRepository->findByActivityId($activity->getId());
        }

        $activity2children = array();
        $existingChildren = array();
        if (array_key_exists('children', $data))
        {
            $children = $data['children'];
            foreach ($children as $childData)
            {
                $childExternalId = $childData['id'];

                $child = $this->childRepository->findByExternalId($childExternalId);
                if ($child == null)
                {
                    throw new ChildNotFoundException($childExternalId);
                }

                $activity2child = $this->activity2childRepository->findByActivityIdAndChildId($activity->getId(), $child->getId());

                array_push($existingChildren, $child->getId());
                if ($activity2child == null)
                {
                    $activity2child = new Activity2Child();
                    $activity2child->setCreatedAt(time());
                    $activity2child->setChild($child);
                    $activity2child->setActivityId($activity->getId());
                }
                else
                {
                    $activity2child->setUpdatedAt(time());
                }

                if (isset($childData['is_active']))
                {
                    $activity2child->setIsActive($childData['is_active'] === true);
                }

                array_push($activity2children, $activity2child);
            }
            // a child was removed
            // TODO:
            foreach ($a2clist as $a2c)
            {
                if (!in_array($a2c->getChildId(), $existingChildren))
                {
                    // delete
                    $a2c->setIsActive(false);
                    $this->activity2childRepository->save($a2c);
                }
            }

            $activity->setActivity2children($activity2children);
        }

        $datesEntities = array();
        if (array_key_exists('dates', $data))
        {
            $dateIds = array();
            $dates = $data['dates'];
            foreach ($dates as $date)
            {
                $dateEntity = null;
                if (isset($date['id']) && $date['id'] != null)
                {
                    $dateExternalId = $date['id'];

                    $dateEntity = $this->activityDateRepository->findByExternalId($dateExternalId);

                    array_push($dateIds, $dateExternalId);
                }

                if ($dateEntity == null)
                {
                    $dateEntity = new ActivityDate();
                    $dateEntity->setExternalId(UUID::v4());
                    $dateEntity->setCreatedAt(time());
                }
                if (isset($date['description'])){
                    $dateEntity->setDescription($date['description']);
                }
                $dateEntity->setActivityNotificationScheduleId($date['notification_schedule']);
                $dateEntity->setScheduledAt($date['scheduled_at']);
                $dateEntity->setUpdatedAt(time());
                $dateEntity->setActivity($activity);

                array_push($datesEntities, $dateEntity);
            }

            $this->removeActivityDates($activity, $dateIds);

            $activity->setActivityDates($datesEntities);
        }

        $this->activityRepository->save($activity);

        $httpResponse = new JsonResponse($activity, 200);

        $this->logResponse($httpResponse);

        return $httpResponse;
    }

    /**
     * @param Activity $activity
     * @param array $dateIds
     */
    private function removeActivityDates($activity, $dateIds)
    {
        if ($activity->getId() > 0) {
            $dates = $this->activityDateRepository->findByActivityId($activity->getId());
            foreach ($dates as $d) {
                if (!in_array($d->getId(), $dateIds)) {
                    $this->activityDateRepository->remove($d);
                }
            }
        }
    }
}