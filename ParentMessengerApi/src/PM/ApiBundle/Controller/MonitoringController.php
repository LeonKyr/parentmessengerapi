<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Service\Contract\IParentMessengerService;
use Psr\Log\LoggerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;

class MonitoringController
    extends BaseController
{
    public function __construct(
        LoggerInterface $logger
    )
    {
        parent::__construct($logger);
    }

    public function pingAction(Request $request)
    {
        $this->logRequest($request);

        $response = new Response(
            'PING OK',
            200
        );

        $this->logResponse($response);

        return $response;
    }
}