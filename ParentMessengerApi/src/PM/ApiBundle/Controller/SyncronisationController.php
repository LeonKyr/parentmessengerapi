<?php
namespace PM\ApiBundle\Controller;

use PM\ApiBundle\Common\Exception\DeviceNotFoundException;
use PM\ApiBundle\Common\Exception\DeviceNotSuppliedException;
use PM\ApiBundle\Common\Exception\KindergartenNotFoundException;
use PM\ApiBundle\Entity\Device;
use PM\ApiBundle\Entity\Repository\IActivityRepository;
use PM\ApiBundle\Entity\Repository\IChildContactRepository;
use PM\ApiBundle\Entity\Repository\IChildRepository;
use PM\ApiBundle\Entity\Repository\IDeviceRepository;
use PM\ApiBundle\Entity\Repository\IGroupRepository;
use PM\ApiBundle\Entity\Repository\IKindergartenRepository;
use PM\ApiBundle\Entity\Repository\ITeacherRepository;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;

class SyncronisationController
    extends BaseController
{
    /**
     * @var \PM\ApiBundle\Entity\Repository\IKindergartenRepository
     */
    private $kindergartenRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IDeviceRepository
     */
    private $deviceRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IChildRepository
     */
    private $childRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\ITeacherRepository
     */
    private $teacherRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IGroupRepository
     */
    private $groupRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IChildContactRepository
     */
    private $childContactRepository;
    /**
     * @var KindergartenController
     */
    private $kindergartenController;
    /**
     * @var ChildController
     */
    private $childController;
    /**
     * @var TeacherController
     */
    private $teacherController;
    /**
     * @var GroupController
     */
    private $groupController;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IActivityRepository
     */
    private $activityRepository;

    public function __construct(LoggerInterface $logger,
                                IKindergartenRepository $kindergartenRepository,
                                IDeviceRepository $deviceRepository,
                                IChildRepository $childRepository,
                                ITeacherRepository $teacherRepository,
                                IGroupRepository $groupRepository,
                                IChildContactRepository $childContactRepository,
                                KindergartenController $kindergartenController,
                                ChildController $childController,
                                TeacherController $teacherController,
                                GroupController $groupController,
                                IActivityRepository $activityRepository)
    {
        parent::__construct($logger);

        $this->kindergartenRepository = $kindergartenRepository;
        $this->deviceRepository = $deviceRepository;
        $this->childRepository = $childRepository;
        $this->teacherRepository = $teacherRepository;
        $this->groupRepository = $groupRepository;
        $this->childContactRepository = $childContactRepository;
        $this->kindergartenController = $kindergartenController;
        $this->childController = $childController;
        $this->teacherController = $teacherController;
        $this->groupController = $groupController;
        $this->activityRepository = $activityRepository;
    }

    public function syncAction(Request $request, $kindergartenExternalId, $startDate)
    {
        $this->logRequest($request);

        $result = array();

        $data = $this->getJsonData($request);

        $device = $this->extractDevice($data);

//        $this->syncData($request, $kindergartenExternalId, $data,  $device, $result);

        $this->getUnsyncData($kindergartenExternalId, $device, $startDate, $result);

        $response = new JsonResponse($result);

        $this->logResponse($response);

        return $response;
    }

    private function syncData(Request $request, $kindergartenExternalId, $data, $deviceId, array &$result)
    {
        if (isset($data['kindergarten']))
        {
            $this->kindergartenController->changeKindergarten($data['kindergarten'], null, $deviceId);
        }
        if (isset($data['children']))
        {
            $children = $data['children'];
            foreach ($children as $child)
            {
                $childEntity = $this->childController
                    ->changeChild($child, $kindergartenExternalId, $deviceId);
            }
        }
        if (isset($data['teachers']))
        {
            $teachers = $data['teachers'];
            foreach ($teachers as $teacher)
            {
                $teacherEntity = $this->teacherController
                    ->changeTeacher($teacher, $kindergartenExternalId, $deviceId);
            }
        }
        if (isset($data['groups']))
        {
            $groups = $data['groups'];
            foreach ($groups as $group)
            {
                $groupEntity = $this->groupController
                    ->changeGroup($group, $kindergartenExternalId, $deviceId);
            }
        }
    }

    /**
     * @param $kindergartenExternalId
     * @param Device $device
     * @param $startDate
     * @param array $result
     * @throws \PM\ApiBundle\Common\Exception\DeviceNotFoundException
     * @throws \PM\ApiBundle\Common\Exception\KindergartenNotFoundException
     * @return array
     */
    private function getUnsyncData($kindergartenExternalId, $device, $startDate, array &$result)
    {
        $kindergarten = $this->kindergartenRepository->findByExternalId($kindergartenExternalId);

        if ($kindergarten == null) {
            throw new KindergartenNotFoundException($kindergartenExternalId);
        }

        if ($device == null)
        {
            throw new DeviceNotFoundException('NULL');
        }

        $deviceId = $device->getId();

        if ($kindergarten->getLastUpdateByDeviceId() != $deviceId) {
            $result['kindergarten'] = $kindergarten;
        }

        // children
        $children = $this->childRepository
            ->findByKindergartenIdWithDifferentDeviceIdAndUpdatedAt($kindergarten->getId(), $deviceId, $startDate);
        if ($children != null)
        {
            // add contacts
            foreach ($children as $child) {
                $cc = $this->childContactRepository->findByChildIdWithDifferentDeviceIdAndUpdatedAt($child->getId(), $deviceId, $startDate);
                $child->setChildContacts($cc);
            }
        }
        $result['children'] = $children;
        // teachers
        $teachers = $this->teacherRepository
            ->findByKindergartenIdWithDifferentDeviceIdAndUpdatedAt($kindergarten->getId(), $deviceId, $startDate);
        $result['teachers'] = $teachers;
        // groups
        $groups = $this->groupRepository
            ->findByKindergartenIdWithDifferentDeviceIdAndUpdatedAt($kindergarten->getId(), $deviceId, $startDate);
        $result['groups'] = $groups;
        // activities
        $result['activities'] = $this->activityRepository->findByKindergartenIdAndAndUpdatedAt($kindergarten->getId(), $startDate, 0);

        // device
        $result['device'] = $device;
    }

    /**
     * @param $data
     * @return Device
     * @throws \PM\ApiBundle\Common\Exception\DeviceNotFoundException
     * @throws \PM\ApiBundle\Common\Exception\DeviceNotSuppliedException
     */
    private function extractDevice($data)
    {
        if (!(isset($data['device'])) ||
            !(isset($data['device']['id']))
        ) {
            throw new DeviceNotSuppliedException();
        }

        $device = $this->deviceRepository->findByExternalId($data['device']['id']);

        if ($device == null) {
            throw new DeviceNotFoundException($data['device']['id']);
        }
        return $device;
    }
}