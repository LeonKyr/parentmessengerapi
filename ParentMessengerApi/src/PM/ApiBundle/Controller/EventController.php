<?php

namespace PM\ApiBundle\Controller;

use Exception;
use PM\ApiBundle\Common\Exception\ChildNotFoundException;
use PM\ApiBundle\Common\Exception\TeacherNotFoundException;
use PM\ApiBundle\Entity\Event;
use PM\ApiBundle\Entity\Event2Child;
use PM\ApiBundle\Entity\EventPropertyValue;
use PM\ApiBundle\Entity\EventPropertyValueBag;
use PM\ApiBundle\Entity\Repository\IActionPropertyRepository;
use PM\ApiBundle\Entity\Repository\IActionRepository;
use PM\ApiBundle\Entity\Repository\IChild2ListenerRepository;
use PM\ApiBundle\Entity\Repository\IChildRepository;
use PM\ApiBundle\Entity\Repository\IEventRepository;
use PM\ApiBundle\Entity\Repository\IKindergartenRepository;
use PM\ApiBundle\Entity\Repository\IListener2DeviceRepository;
use PM\ApiBundle\Entity\Repository\ITeacherRepository;
use PM\ApiBundle\Misc\UUID;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\JsonResponse;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use PM\ApiBundle\Entity\Child2Listener;

class EventController
    extends BaseController
{
    /**
     * @var \PM\ApiBundle\Entity\Repository\IKindergartenRepository
     */
    private $kindergartenRepository;
    /**
     * @var IEventRepository
     */
    private $eventRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IChildRepository
     */
    private $childRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IChild2ListenerRepository
     */
    private $child2listenerRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\ITeacherRepository
     */
    private $teacherRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IActionRepository
     */
    private $actionRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IActionPropertyRepository
     */
    private $actionPropertyRepository;
    /**
     * @var \PM\ApiBundle\Entity\Repository\IListener2DeviceRepository
     */
    private $listener2deviceRepository;

    public function __construct(
        LoggerInterface $logger,
        IKindergartenRepository $kindergartenRepository,
        IEventRepository $eventRepository,
        IChildRepository $childRepository,
        IChild2ListenerRepository $child2listenerRepository,
        ITeacherRepository $teacherRepository,
        IActionRepository $actionRepository,
        IActionPropertyRepository $actionPropertyRepository,
        IListener2DeviceRepository $listener2deviceRepository
    )
    {
        parent::__construct($logger);

        $this->kindergartenRepository = $kindergartenRepository;
        $this->eventRepository = $eventRepository;
        $this->childRepository = $childRepository;
        $this->child2listenerRepository = $child2listenerRepository;
        $this->teacherRepository = $teacherRepository;
        $this->actionRepository = $actionRepository;
        $this->actionPropertyRepository = $actionPropertyRepository;
        $this->listener2deviceRepository = $listener2deviceRepository;
    }

    public function getEventsAction(Request $request, $listenerExternalId, $startDate, $limitCount)
    {
        $this->logRequest($request);

        /** @var Child2Listener[] $children2listener */
        $children2listener = $this->child2listenerRepository
            ->findByListenerExternalId($listenerExternalId);

        $result = array();
        foreach ($children2listener as $c2l)
        {
            $childEvents = $this->eventRepository
                ->findByChildIdAndCreatedAt($c2l->getChildId(), $startDate, $limitCount);

            array_push($result, array(
                'child' => $c2l->getChild(),
                'events' => $childEvents,
            ));
        }

        $httpResponse = new JsonResponse($result);

        $this->logResponse($httpResponse);

        return $httpResponse;
    }

    /*
    {
        "group": "groupExternalId",
        "children": [
            {
                "childId": "test-child-01"
            },
            {
                "childId": "test-child-02"
            }
        ],
        "properties": [
            {
                "name": "title",
                "value": "some title"
            },
            {
                "name": "message",
                "value": "Hello Joe Doe, Please bring diapers tomorrow.\n We ran out.\n And a cream for small ass - it is red"
            },
            {
            }
        ]
    }
     */
    public function createEventAction(Request $request, $teacherExternalId, $action)
    {
        date_default_timezone_set("UTC");

        $this->logRequest($request);

        $data = $this->getJsonData($request);

        $properties = $data['properties'];
        $eventPropertyValues = array();
        foreach ($properties as $property)
        {
            $name = $property['name'];
            $value = $property['value'];
            $isBag = isset($property['is_bag']) ? $property['is_bag'] : false;

            $ap = $this->actionPropertyRepository->findByName($name);

            $isSelection = $ap->getActionPropertyType()->getIsSelection();

            $epv = new EventPropertyValue($ap, $isBag);

            if ($isBag)
            {
                $bags = array();
                foreach($value as $v)
                {
                    $bag = new EventPropertyValueBag();
                    $bag->setExternalId($v['id']);
                    $bag->setCreatedAt(time());

                    array_push($bags, $bag);
                }

                $epv->setEventPropertyValueBags($bags);
            }
            else if ($isSelection)
            {
                $epv->setIsSelection($isSelection);
                $epv->setSelectedValue($value);
            }
            else
            {
                $epv->setValue($value);
            }

            $epv->setCreatedAt(time());

            array_push($eventPropertyValues, $epv);
        }

        $teacher = $this->teacherRepository->findByExternalId($teacherExternalId);
        if ($teacher == null)
            throw new TeacherNotFoundException($teacherExternalId);

        $actionEntity = $this->actionRepository->findByName($action);

        $event = new Event();
        $event->setAction($actionEntity);
        $event->setTeacher($teacher);
        $event->setCreatedAt(time());
        $event->setExternalId(UUID::v4());

        $event2children = array();
        if (array_key_exists('children', $data))
        {
            $children = $data['children'];
            foreach ($children as $child)
            {
                $childExternalId = $child['childId'];

                $child = $this->childRepository->findByExternalId($childExternalId);
                if ($child == null)
                {
                    throw new ChildNotFoundException($childExternalId);
                }
                $event2child = new Event2Child();
                $event2child->setChildId($child->getId());
                $event2child->setCreatedAt(time());

                array_push($event2children, $event2child);
            }
        }

        $this->eventRepository->save($event, $eventPropertyValues, $event2children);

        $this->pushNotifyParents($event);

        $httpResponse = new JsonResponse( array(
            'id' => $event->getExternalId()
        ), 200);

//        $httpResponse = new JsonResponse( $event, 200);

        $this->logResponse($httpResponse);

        return $httpResponse;
    }

    /**
     * @param Event $event
     */
    private function pushNotifyParents($event)
    {
        $devices = $this->eventRepository->findAllDevicesThanShouldBeNotifiedForEvent($event->getId());

//        $gcm = new GCM();

        foreach( $devices as $device )
        {
            if ($device->getPlatformId() == 1){
//                    $gcm = new GoogleCloudMessagingComponent();
                try
                {
                    $this->sendGCM($device->getToken(), $event->getAction()->getName());
                }
                catch(Exception $ex)
                {
                }
            }
            else{
                try
                {
                    $this->iOsNotification($device->getToken(), $event);
                }
                catch(Exception $ex)
                {
                }
            }
        }
    }

    public function sendGCM($deviceToken, $action)
    {
        // Replace with real GCM browser / server API key from Google APIs
        $apiKey = 'AIzaSyCghvCpW7woDjNCj1K6GvwTfWps-aUVjis';

// Replace with real client registration IDs, most likely stored in your database
        $registrationIDs = array( $deviceToken );

// Payload data to be sent
        $data = array( 'action' => $action );

// Set request URL to GCM endpoint
        $url = 'https://android.googleapis.com/gcm/send';

// Set POST variables (device IDs and payload)
        $fields = array(
            'registration_ids'  => $registrationIDs,
            'data'              => $data,
        );

// Set request headers (authentication and payload type)
        $headers = array(
            'Authorization: key=' . $apiKey,
            'Content-Type: application/json'
        );

// Open connection
        $ch = curl_init();

// Set the url
        curl_setopt( $ch, CURLOPT_URL, $url );

// Set request method to POST
        curl_setopt( $ch, CURLOPT_POST, true );

// Set custom headers
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);

// Get response back as string
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );

// Set post data
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );

// Send the request
        $result = curl_exec($ch);

// Close connection
        curl_close($ch);

// Debug GCM response
        //echo $result;
    }

    private function iOsNotification($deviceToken, Event $event)
    {
        $development = true;

        if($development)
        {
            $apns_url = 'ssl://gateway.sandbox.push.apple.com:2195';
            $apns_cert = '../apn/ck.pem';
        }
        else
        {
            $apns_url = 'ssl://gateway.push.apple.com:2195';
            $apns_cert = '../apn/ck.pem';
        }

// Put your private key's passphrase here:
        $passphrase = 'klio1984';

// Put your alert message here:
        foreach ($event->getEventPropertyValues() as $property)
        {
            if ($property->getActionProperty()->getName() == 'title')
            {
                $title = $property->getValue();
                break;
            }
        }
        $message = $event->getAction()->getName().': '.$title;

        // TODO: change in the future
        $badge = 1;
        $sound = 'default';

        $payload = array();
        $payload['aps'] = array('alert' => $message, 'badge' => intval($badge), 'sound' => $sound);
        $payload = json_encode($payload);

////////////////////////////////////////////////////////////////////////////////

        $ctx = stream_context_create();
        stream_context_set_option($ctx, 'ssl', 'local_cert', $apns_cert);
        stream_context_set_option($ctx, 'ssl', 'passphrase', $passphrase);

// Open a connection to the APNS server
        $fp = stream_socket_client(
            $apns_url, $err,
            $errstr, 60, STREAM_CLIENT_CONNECT|STREAM_CLIENT_PERSISTENT, $ctx);

        if (!$fp)
            exit("Failed to connect: $err $errstr" . PHP_EOL);

//        echo 'Connected to APNS' . PHP_EOL;

// Build the binary notification
        $msg = chr(0) . pack('n', 32) . pack('H*', $deviceToken) . pack('n', strlen($payload)) . $payload;

// Send it to the server
        $result = fwrite($fp, $msg, strlen($msg));

//        if (!$result)
//            echo 'Message not delivered' . PHP_EOL;
//        else
//            echo 'Message successfully delivered' . PHP_EOL;

// Close the connection to the server
        fclose($fp);
    }
}

