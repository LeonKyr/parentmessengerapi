<?php

namespace PM\ApiBundle\Component;

use PM\ApiBundle\Entity\Repository\IEventRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class ActivityPlannerCommand
    extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('activity:planner')
            ->setDescription('Creates events for activities')
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
        /**
         * @var \PM\ApiBundle\Entity\Repository\IActivityRepository
         */
        $repository = $this->getContainer()->get('IActivityRepository');

        $activities = $repository->findByKindergartenIdAndAndUpdatedAt(
            1, 0, 20);

        $httpResponse = new JsonResponse($activities);

        echo $httpResponse;
    }
}