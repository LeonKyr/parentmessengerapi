<?php

namespace PM\ApiBundle\Component;

use PM\ApiBundle\Entity\Repository\IEventRepository;
use Symfony\Bundle\FrameworkBundle\Command\ContainerAwareCommand;
use Symfony\Component\Console\Input\InputArgument;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpFoundation\JsonResponse;

class EventNotificationCommand
    extends ContainerAwareCommand
{
    protected function configure()
    {
        $this
            ->setName('event:notify')
            ->setDescription('Notifies listeners about events')
            ->addArgument(
                'createdAt',
                InputArgument::OPTIONAL,
                'From what time do we need to notify parent about events?'
            )
        ;
    }

    protected function execute(InputInterface $input, OutputInterface $output)
    {
//        $createdAt = $input->getArgument('createdAt');
//        if (isset($createdAt)) {
//
//        } else {
//            $date = new \DateTime();
//            $createdAt = $date->getTimestamp();
//        }
//
//        /**
//         * @var \PM\ApiBundle\Entity\Repository\IEventRepository
//         */
        $repository = $this->getContainer()->get('IActivityRepository');

        $activities = $repository->findByKindergartenIdAndAndUpdatedAt(
            1, 0, 20);

        $httpResponse = new JsonResponse($activities);

        echo $httpResponse;
//
//        $devices = $eventRepository->findAllDevicesThanShouldBeNotified($createdAt);
//
////        $gcm = new GoogleCloudMessagingComponent();
////        $gcm = $this->getContainer()->get('GoogleCloudMessagingComponent');
//        /**
//         * @var @device \PM\ApiBundle\Entity\Device
//         */
//        foreach( $devices as $device )
//        {
////            if ($device->getPlatformId() == $gcm->getSupportedPlatform())
//            {
////                $gcm->sent($device->getToken());
//                // Replace with real GCM browser / server API key from Google APIs
//                $apiKey = 'AIzaSyCghvCpW7woDjNCj1K6GvwTfWps-aUVjis';
//
//// Replace with real client registration IDs, most likely stored in your database
//                echo $device->getToken()."
//                ";
//                $registrationIDs = array( $device->getToken() );
//
//// Payload data to be sent
//                $data = array( 'message' => "Hey Max! You've got a message about your KID" );
//
//// Set request URL to GCM endpoint
//                $url = 'https://android.googleapis.com/gcm/send';
//
//// Set POST variables (device IDs and payload)
//                $fields = array(
//                    'registration_ids'  => $registrationIDs,
//                    'data'              => $data,
//                );
//
//// Set request headers (authentication and payload type)
//                $headers = array(
//                    'Authorization: key=' . $apiKey,
//                    'Content-Type: application/json'
//                );
//
//// Open connection
//                $ch = curl_init();
//
//// Set the url
//                curl_setopt( $ch, CURLOPT_URL, $url );
//
//// Set request method to POST
//                curl_setopt( $ch, CURLOPT_POST, true );
//
//// Set custom headers
//                curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers);
//
//// Get response back as string
//                curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
//
//// Set post data
//                curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode( $fields ) );
//
//// Send the request
//                $result = curl_exec($ch);
//
//// Close connection
//                curl_close($ch);
//
//// Debug GCM response
//                echo $result;
//
//            }
//            $output->writeln($device->getId());
//        }
    }
}