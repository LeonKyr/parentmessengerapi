<?php
namespace PM\ApiBundle\Component;

interface IParentDeviceNotification
{
    public function getSupportedPlatform();
    public function sent($deviceToken, $action);
}