<?php
namespace PM\ApiBundle\Common\Exception;

final class DeviceNotSuppliedException
    extends \Exception
{
    function __construct()
    {
        parent::__construct("Device not supplied.");
    }
}