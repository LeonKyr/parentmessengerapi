<?php
namespace PM\ApiBundle\Common\Exception;

final class ParentNotFoundException
    extends \Exception
{
    function __construct($value)
    {
        parent::__construct("Parent [$value] was not found.");
    }
}