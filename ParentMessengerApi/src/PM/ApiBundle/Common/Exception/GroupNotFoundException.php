<?php
namespace PM\ApiBundle\Common\Exception;

final class GroupNotFoundException
    extends \Exception
{
    function __construct($value)
    {
        parent::__construct("Group [$value] was not found.");
    }
}