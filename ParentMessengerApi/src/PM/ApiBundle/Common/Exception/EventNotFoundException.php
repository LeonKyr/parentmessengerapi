<?php
namespace PM\ApiBundle\Common\Exception;

final class EventNotFoundException
    extends \Exception
{
    function __construct($value)
    {
        parent::__construct("Event [$value] was not found.");
    }
}