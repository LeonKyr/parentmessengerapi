<?php
namespace PM\ApiBundle\Common\Exception;

final class TeacherNotFoundException
    extends \Exception
{
    function __construct($value)
    {
        parent::__construct("Teacher [$value] was not found.");
    }
}