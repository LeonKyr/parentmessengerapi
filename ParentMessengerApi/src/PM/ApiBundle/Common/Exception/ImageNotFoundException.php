<?php
namespace PM\ApiBundle\Common\Exception;

final class ImageNotFoundException
    extends \Exception
{
    function __construct($value)
    {
        parent::__construct("Image [$value] was not found.");
    }
}