<?php
namespace PM\ApiBundle\Common\Exception;

final class KindergartenNotFoundException
    extends \Exception
{
    function __construct($kindergarten)
    {
        parent::__construct("Kindergarten [$kindergarten] was not found.");
    }
}
