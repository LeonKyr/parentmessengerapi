<?php
namespace PM\ApiBundle\Common\Exception;

final class ListenerNotFoundException
    extends \Exception
{
    function __construct($value)
    {
        parent::__construct("Listener [$value] was not found.");
    }
}