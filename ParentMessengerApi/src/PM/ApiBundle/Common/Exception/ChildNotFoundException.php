<?php
namespace PM\ApiBundle\Common\Exception;

final class ChildNotFoundException
    extends \Exception
{
    function __construct($value)
    {
        parent::__construct("Child [$value] was not found.");
    }
}