<?php
namespace PM\ApiBundle\Common\Exception;

final class ActivityNotFoundException
    extends \Exception
{
    function __construct($value)
    {
        parent::__construct("Activity [$value] was not found.");
    }
}