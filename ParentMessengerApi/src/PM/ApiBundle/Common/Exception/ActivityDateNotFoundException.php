<?php
namespace PM\ApiBundle\Common\Exception;

final class ActivityDateNotFoundException
    extends \Exception
{
    function __construct($value)
    {
        parent::__construct("Activity Date [$value] was not found.");
    }
}