<?php
namespace PM\ApiBundle\Common\Exception;

final class DeviceNotFoundException
    extends \Exception
{
    function __construct($value)
    {
        parent::__construct("Device [$value] was not found.");
    }
}