<?php
namespace PM\ApiBundle\Common\Exception;

final class ActivityTypeNotFoundException
    extends \Exception
{
    function __construct($value)
    {
        parent::__construct("ActivityType [$value] was not found.");
    }
}