# ************************************************************
# Sequel Pro SQL dump
# Version 4096
#
# http://www.sequelpro.com/
# http://code.google.com/p/sequel-pro/
#
# Host: 192.168.56.1 (MySQL 5.5.30-30.2-log)
# Database: pm
# Generation Time: 2013-09-26 23:32:13 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump of table action
# ------------------------------------------------------------

DROP TABLE IF EXISTS `action`;

CREATE TABLE `action` (
  `id` smallint(6) NOT NULL,
  `name` varchar(50) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `action` WRITE;
/*!40000 ALTER TABLE `action` DISABLE KEYS */;

INSERT INTO `action` (`id`, `name`)
VALUES
	(1,'note');

/*!40000 ALTER TABLE `action` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table child
# ------------------------------------------------------------

DROP TABLE IF EXISTS `child`;

CREATE TABLE `child` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `group_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `external_id` varchar(100) NOT NULL,
  `photo_id` int(11) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `child` WRITE;
/*!40000 ALTER TABLE `child` DISABLE KEYS */;

INSERT INTO `child` (`id`, `group_id`, `name`, `external_id`, `photo_id`, `dob`, `created_at`, `updated_at`)
VALUES
	(1,1,'Anna Maria','test-child-01',NULL,NULL,'2013-09-26 22:08:40','0000-00-00 00:00:00'),
	(2,1,'Elizabeth','test-child-02',NULL,NULL,'2013-09-26 22:08:40','0000-00-00 00:00:00');

/*!40000 ALTER TABLE `child` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table child2parent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `child2parent`;

CREATE TABLE `child2parent` (
  `child_id` int(11) NOT NULL,
  `parent_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`child_id`,`parent_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `child2parent` WRITE;
/*!40000 ALTER TABLE `child2parent` DISABLE KEYS */;

INSERT INTO `child2parent` (`child_id`, `parent_id`, `created_at`)
VALUES
	(1,1,'2013-09-26 22:23:31'),
	(2,1,'2013-09-26 22:23:31');

/*!40000 ALTER TABLE `child2parent` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table color
# ------------------------------------------------------------

DROP TABLE IF EXISTS `color`;

CREATE TABLE `color` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table event
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event`;

CREATE TABLE `event` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `action_id` smallint(6) NOT NULL,
  `teacher_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `event` WRITE;
/*!40000 ALTER TABLE `event` DISABLE KEYS */;

INSERT INTO `event` (`id`, `action_id`, `teacher_id`, `created_at`)
VALUES
	(1,1,1,'2013-09-26 21:41:07'),
	(2,1,1,'2013-09-26 21:42:20'),
	(3,1,1,'2013-09-26 21:42:58'),
	(4,1,1,'2013-09-26 21:43:12'),
	(5,1,1,'2013-09-26 22:03:51'),
	(6,1,1,'2013-09-26 22:07:25'),
	(17,1,1,'2013-09-26 22:19:36'),
	(18,1,1,'2013-09-26 22:19:48'),
	(19,1,1,'2013-09-26 22:20:05'),
	(20,1,1,'2013-09-26 22:46:53'),
	(21,1,1,'2013-09-26 22:50:23'),
	(22,1,1,'2013-09-26 22:53:35'),
	(23,1,1,'2013-09-26 22:53:54'),
	(24,1,1,'2013-09-26 22:55:57'),
	(25,1,1,'2013-09-26 22:56:39'),
	(26,1,1,'2013-09-26 22:57:09'),
	(27,1,1,'2013-09-26 22:58:50'),
	(28,1,1,'2013-09-26 22:59:05'),
	(29,1,1,'2013-09-26 22:59:44'),
	(30,1,1,'2013-09-26 23:02:02'),
	(31,1,1,'2013-09-26 23:02:20'),
	(32,1,1,'2013-09-26 23:04:02'),
	(33,1,1,'2013-09-26 23:04:04'),
	(34,1,1,'2013-09-26 23:04:17'),
	(35,1,1,'2013-09-26 23:04:52'),
	(36,1,1,'2013-09-26 23:05:32'),
	(37,1,1,'2013-09-26 23:06:06'),
	(38,1,1,'2013-09-26 23:06:15'),
	(39,1,1,'2013-09-26 23:06:23'),
	(40,1,1,'2013-09-26 23:06:42'),
	(41,1,1,'2013-09-26 23:07:19'),
	(42,1,1,'2013-09-26 23:07:51'),
	(43,1,1,'2013-09-26 23:07:57'),
	(44,1,1,'2013-09-26 23:08:05');

/*!40000 ALTER TABLE `event` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event_property
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event_property`;

CREATE TABLE `event_property` (
  `id` smallint(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `event_property_type_id` smallint(6) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `event_property` WRITE;
/*!40000 ALTER TABLE `event_property` DISABLE KEYS */;

INSERT INTO `event_property` (`id`, `name`, `created_at`, `event_property_type_id`)
VALUES
	(1,'title','2013-09-26 20:21:57',1),
	(2,'message','2013-09-26 20:25:06',2);

/*!40000 ALTER TABLE `event_property` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event_property_type
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event_property_type`;

CREATE TABLE `event_property_type` (
  `id` smallint(6) NOT NULL,
  `name` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `event_property_type` WRITE;
/*!40000 ALTER TABLE `event_property_type` DISABLE KEYS */;

INSERT INTO `event_property_type` (`id`, `name`)
VALUES
	(3,'date'),
	(2,'multiline-text'),
	(1,'one-line-text');

/*!40000 ALTER TABLE `event_property_type` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event_property_value
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event_property_value`;

CREATE TABLE `event_property_value` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_property_id` smallint(6) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `value` text NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `event_property_value` WRITE;
/*!40000 ALTER TABLE `event_property_value` DISABLE KEYS */;

INSERT INTO `event_property_value` (`id`, `event_property_id`, `created_at`, `value`)
VALUES
	(1,1,'2013-09-26 21:41:07','some title'),
	(2,2,'2013-09-26 21:41:07','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(3,1,'2013-09-26 21:42:20','some title'),
	(4,2,'2013-09-26 21:42:20','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(5,1,'2013-09-26 21:42:58','some title'),
	(6,2,'2013-09-26 21:42:58','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(7,1,'2013-09-26 21:43:12','some title'),
	(8,2,'2013-09-26 21:43:12','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(9,1,'2013-09-26 22:03:51','some title'),
	(10,2,'2013-09-26 22:03:51','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(11,1,'2013-09-26 22:07:25','some title'),
	(12,2,'2013-09-26 22:07:25','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(33,1,'2013-09-26 22:19:36','some title'),
	(34,2,'2013-09-26 22:19:36','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(35,1,'2013-09-26 22:19:48','some title'),
	(36,2,'2013-09-26 22:19:48','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(37,1,'2013-09-26 22:20:05','some title'),
	(38,2,'2013-09-26 22:20:05','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(39,1,'2013-09-26 22:46:53','some title'),
	(40,2,'2013-09-26 22:46:53','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(41,1,'2013-09-26 22:50:23','some title'),
	(42,2,'2013-09-26 22:50:23','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(43,1,'2013-09-26 22:53:35','some title'),
	(44,2,'2013-09-26 22:53:35','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(45,1,'2013-09-26 22:53:54','some title'),
	(46,2,'2013-09-26 22:53:54','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(47,1,'2013-09-26 22:55:57','some title'),
	(48,2,'2013-09-26 22:55:57','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(49,1,'2013-09-26 22:56:39','some title'),
	(50,2,'2013-09-26 22:56:39','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(51,1,'2013-09-26 22:57:09','some title'),
	(52,2,'2013-09-26 22:57:09','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(53,1,'2013-09-26 22:58:50','some title'),
	(54,2,'2013-09-26 22:58:50','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(55,1,'2013-09-26 22:59:05','some title'),
	(56,2,'2013-09-26 22:59:05','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(57,1,'2013-09-26 22:59:44','some title'),
	(58,2,'2013-09-26 22:59:44','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(59,1,'2013-09-26 23:02:02','some title'),
	(60,2,'2013-09-26 23:02:02','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(61,1,'2013-09-26 23:02:20','some title'),
	(62,2,'2013-09-26 23:02:20','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(63,1,'2013-09-26 23:04:02','some title'),
	(64,2,'2013-09-26 23:04:02','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(65,1,'2013-09-26 23:04:04','some title'),
	(66,2,'2013-09-26 23:04:04','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(67,1,'2013-09-26 23:04:17','some title'),
	(68,2,'2013-09-26 23:04:17','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(69,1,'2013-09-26 23:04:52','some title'),
	(70,2,'2013-09-26 23:04:52','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(71,1,'2013-09-26 23:05:32','some title'),
	(72,2,'2013-09-26 23:05:32','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(73,1,'2013-09-26 23:06:06','some title'),
	(74,2,'2013-09-26 23:06:06','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(75,1,'2013-09-26 23:06:15','some title'),
	(76,2,'2013-09-26 23:06:15','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(77,1,'2013-09-26 23:06:23','some title'),
	(78,2,'2013-09-26 23:06:23','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(79,1,'2013-09-26 23:06:42','some title'),
	(80,2,'2013-09-26 23:06:42','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(81,1,'2013-09-26 23:07:19','some title'),
	(82,2,'2013-09-26 23:07:19','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(83,1,'2013-09-26 23:07:51','some title'),
	(84,2,'2013-09-26 23:07:51','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(85,1,'2013-09-26 23:07:57','some title'),
	(86,2,'2013-09-26 23:07:57','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red'),
	(87,1,'2013-09-26 23:08:05','some title'),
	(88,2,'2013-09-26 23:08:05','Hello Joe Doe, Please bring diapers tomorrow. We ran out. And a cream for small ass - it is red');

/*!40000 ALTER TABLE `event_property_value` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table event_property_value_bag
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event_property_value_bag`;

CREATE TABLE `event_property_value_bag` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `event_property_value_id` int(11) NOT NULL,
  `event_property_id` smallint(6) NOT NULL,
  `value` text NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table event2child
# ------------------------------------------------------------

DROP TABLE IF EXISTS `event2child`;

CREATE TABLE `event2child` (
  `event_id` int(11) NOT NULL,
  `child_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`event_id`,`child_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `event2child` WRITE;
/*!40000 ALTER TABLE `event2child` DISABLE KEYS */;

INSERT INTO `event2child` (`event_id`, `child_id`, `created_at`)
VALUES
	(17,1,'2013-09-26 22:19:36'),
	(17,2,'2013-09-26 22:19:36'),
	(18,1,'2013-09-26 22:19:48'),
	(18,2,'2013-09-26 22:19:48'),
	(19,1,'2013-09-26 22:20:05'),
	(19,2,'2013-09-26 22:20:05'),
	(20,1,'2013-09-26 22:46:53'),
	(20,2,'2013-09-26 22:46:53'),
	(21,1,'2013-09-26 22:50:23'),
	(21,2,'2013-09-26 22:50:23'),
	(22,1,'2013-09-26 22:53:35'),
	(22,2,'2013-09-26 22:53:35'),
	(23,1,'2013-09-26 22:53:54'),
	(23,2,'2013-09-26 22:53:54'),
	(24,1,'2013-09-26 22:55:57'),
	(24,2,'2013-09-26 22:55:57'),
	(25,1,'2013-09-26 22:56:39'),
	(25,2,'2013-09-26 22:56:39'),
	(26,1,'2013-09-26 22:57:09'),
	(26,2,'2013-09-26 22:57:09'),
	(27,1,'2013-09-26 22:58:50'),
	(27,2,'2013-09-26 22:58:50'),
	(28,1,'2013-09-26 22:59:05'),
	(28,2,'2013-09-26 22:59:05'),
	(29,1,'2013-09-26 22:59:44'),
	(29,2,'2013-09-26 22:59:44'),
	(30,1,'2013-09-26 23:02:02'),
	(30,2,'2013-09-26 23:02:02'),
	(31,1,'2013-09-26 23:02:20'),
	(31,2,'2013-09-26 23:02:20'),
	(32,1,'2013-09-26 23:04:02'),
	(32,2,'2013-09-26 23:04:02'),
	(33,1,'2013-09-26 23:04:04'),
	(33,2,'2013-09-26 23:04:04'),
	(34,1,'2013-09-26 23:04:17'),
	(34,2,'2013-09-26 23:04:17'),
	(35,1,'2013-09-26 23:04:52'),
	(35,2,'2013-09-26 23:04:52'),
	(36,1,'2013-09-26 23:05:32'),
	(36,2,'2013-09-26 23:05:32'),
	(37,1,'2013-09-26 23:06:06'),
	(37,2,'2013-09-26 23:06:06'),
	(38,1,'2013-09-26 23:06:15'),
	(38,2,'2013-09-26 23:06:15'),
	(39,1,'2013-09-26 23:06:23'),
	(39,2,'2013-09-26 23:06:23'),
	(40,1,'2013-09-26 23:06:42'),
	(40,2,'2013-09-26 23:06:42'),
	(41,1,'2013-09-26 23:07:19'),
	(41,2,'2013-09-26 23:07:19'),
	(42,1,'2013-09-26 23:07:51'),
	(42,2,'2013-09-26 23:07:51'),
	(43,1,'2013-09-26 23:07:57'),
	(43,2,'2013-09-26 23:07:57'),
	(44,1,'2013-09-26 23:08:05'),
	(44,2,'2013-09-26 23:08:05');

/*!40000 ALTER TABLE `event2child` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table group
# ------------------------------------------------------------

DROP TABLE IF EXISTS `group`;

CREATE TABLE `group` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kindergarten_id` int(11) NOT NULL,
  `name` varchar(50) NOT NULL,
  `external_id` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `photo_id` int(11) DEFAULT NULL,
  `color_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `group` WRITE;
/*!40000 ALTER TABLE `group` DISABLE KEYS */;

INSERT INTO `group` (`id`, `kindergarten_id`, `name`, `external_id`, `created_at`, `updated_at`, `photo_id`, `color_id`)
VALUES
	(1,1,'Mel','test-group-01','2013-09-26 22:09:03','0000-00-00 00:00:00',NULL,NULL);

/*!40000 ALTER TABLE `group` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table image
# ------------------------------------------------------------

DROP TABLE IF EXISTS `image`;

CREATE TABLE `image` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `path` blob NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table kindergarten
# ------------------------------------------------------------

DROP TABLE IF EXISTS `kindergarten`;

CREATE TABLE `kindergarten` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password` varchar(200) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `logo` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `email_UNIQUE` (`email`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `kindergarten` WRITE;
/*!40000 ALTER TABLE `kindergarten` DISABLE KEYS */;

INSERT INTO `kindergarten` (`id`, `name`, `email`, `password`, `created_at`, `updated_at`, `logo`)
VALUES
	(1,'4786e852-5267-401f-8e9b-4185721ac072@gmail.com','2a8f48da-49e8-428a-92a4-b549019ff08c','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 19:14:37','2013-09-25 19:14:37',NULL),
	(2,'00192aa1-438f-496a-87a9-12f55faa5934@gmail.com','42d9eea2-b58e-4021-a51f-eff868b57fb8','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 19:14:53','2013-09-25 19:14:53',NULL),
	(3,'a31c1890-141b-4e6c-addb-ce086b8a8c40@gmail.com','df2df0ba-5874-4b1e-ae05-e1d364f0b09d','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 19:15:00','2013-09-25 19:15:00',NULL),
	(4,'a7fb32ef-0096-414c-a1f6-f6f4f99b9779@gmail.com','8385d7a2-c4ba-423e-ae9f-58a28db043e0','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 19:15:08','2013-09-25 19:15:08',NULL),
	(5,'d67e7ae3-4637-4bc4-9552-9ad4e609c437@gmail.com','3960fe41-25ba-444e-9b4a-118f048f2ff3','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 21:50:17','2013-09-25 21:50:17',NULL),
	(6,'589a1666-6e4e-4319-a60c-09c9e220acdf@gmail.com','237bca9f-cdc4-435d-bbfb-f72c5c9e0d87','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:01:01','2013-09-25 22:01:01',NULL),
	(7,'97c9b6eb-145c-48e2-b6cd-53f854702396@gmail.com','a5ef0409-8a5f-4ea5-afd6-fe1c92f49b5a','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:01:23','2013-09-25 22:01:23',NULL),
	(8,'7ae9db84-6a9e-4669-989a-47c27e19ec0a@gmail.com','6d31f790-ecef-455c-9025-016618060496','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:01:42','2013-09-25 22:01:42',NULL),
	(9,'097d6767-470c-4c3d-b095-47392d00be33@gmail.com','0db5a9d5-df89-454f-b5cf-2574479eac13','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:02:14','2013-09-25 22:02:14',NULL),
	(10,'065298c6-e5b5-4372-907e-8238c606e661@gmail.com','09b43a07-d9d8-44c5-907e-f82f72f039dd','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:03:49','2013-09-25 22:03:49',NULL),
	(11,'ba5c0289-0626-4072-bf18-c3a244c678aa@gmail.com','b9bb40f9-0c67-4620-bd75-38518afcecee','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:03:49','2013-09-25 22:03:49',NULL),
	(12,'0f6586be-c2f0-4435-ac5a-a24f043d374e@gmail.com','8135122d-f749-4760-a92d-9f10a2490ff7','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:07:49','2013-09-25 22:07:49',NULL),
	(13,'237e4763-7784-4ca3-a567-0ef2a0379969@gmail.com','36a8560a-fb78-4c6b-965f-2a8f760fbc2f','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:07:49','2013-09-25 22:07:49',NULL),
	(14,'e0958a5d-f13e-43c1-ba8d-5147ccbbaf93@gmail.com','df585d33-5c2c-4aae-8fa4-3e3a5f56603d','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:08:21','2013-09-25 22:08:21',NULL),
	(15,'ff772fd6-ac13-421d-a760-0116d09e1752@gmail.com','9d8f7355-8f92-48c7-9972-9aa20547455e','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:08:21','2013-09-25 22:08:21',NULL),
	(16,'df14ad09-30a9-4ec3-a787-114cb941ccf2@gmail.com','8cfab4f2-0553-422b-aa50-674bf90ffaf3','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:08:44','2013-09-25 22:08:44',NULL),
	(17,'93b9fba4-94e0-4cc7-b865-44023b893bef@gmail.com','6b667ea0-6374-4b77-9e0c-e1c18b0399a4','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:08:44','2013-09-25 22:08:44',NULL),
	(18,'0c04f8ed-e628-4f51-87da-4b479903ec83@gmail.com','1d9db75a-a62f-4b89-8645-00a0905ff9ef','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:10:17','2013-09-25 22:10:17',NULL),
	(19,'d0d8d841-f1ed-4728-a28c-5cbf63974903@gmail.com','384d0521-c778-4dbb-978a-90427bb99587','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:10:17','2013-09-25 22:10:17',NULL),
	(20,'368bc184-f49e-4a8a-940f-5e484b9f5ba7@gmail.com','76fda05f-02ce-4a57-908b-45c106fff4b6','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:11:10','2013-09-25 22:11:10',NULL),
	(21,'f47d9c59-f326-44c7-a158-e09213fc3bbb@gmail.com','c6ec6178-3a1c-4d96-b737-9a12d6698e61','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:11:10','2013-09-25 22:11:10',NULL),
	(22,'dd9f9afc-2c3d-4f11-9f7d-f9572c51ea82@gmail.com','6e42c7c3-b0db-4b00-b375-ecfb24779930','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:13:32','2013-09-25 22:13:32',NULL),
	(23,'b903c2f5-5c4c-437f-893b-77ab9cf945cf@gmail.com','91469ee2-83ee-44de-ab17-e5aac2ff62f1','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:13:32','2013-09-25 22:13:32',NULL),
	(24,'384c8079-3404-4e0a-ac21-27e141cb9e23@gmail.com','d271571f-4697-4e33-95f5-4a0c07267575','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:14:44','2013-09-25 22:14:44',NULL),
	(25,'2528e2c9-a76b-4adc-b8f3-4081759503a4@gmail.com','df203fc4-d814-4ffe-b8e2-50f98a9cd203','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:14:44','2013-09-25 22:14:44',NULL),
	(26,'77baa5be-cff0-4fa9-b927-76ab1312e4ed@gmail.com','f4c207f4-f32a-4fd6-a707-dc8d69732705','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:15:24','2013-09-25 22:15:24',NULL),
	(27,'b3a3bc7d-a1c7-4426-8bda-c6d020904607@gmail.com','cb29ffba-7285-40c8-bc30-074d97cc600f','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:15:24','2013-09-25 22:15:24',NULL),
	(28,'8f454640-624e-43c3-b2d4-16996a853f0f@gmail.com','08c0a795-9da4-4f33-9f65-bcd188c919c8','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:18:39','2013-09-25 22:18:39',NULL),
	(29,'c3f5c24c-7c8f-475f-8879-886f20b82c67@gmail.com','9563d323-a3e9-4c23-a533-1d7c197799bb','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:18:40','2013-09-25 22:18:40',NULL),
	(30,'1ee984e1-c467-4809-ae38-c8c53c38b146@gmail.com','0abc4d6a-97d7-4252-9d6f-fa6fb055e45c','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:20:01','2013-09-25 22:20:01',NULL),
	(31,'61e0f128-9a55-48fd-8e6c-c8ff07f1c571@gmail.com','e40ff1be-3711-4ae2-8de1-5765c2dbcbd3','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:20:01','2013-09-25 22:20:01',NULL),
	(32,'3dea7e43-57e0-4dfd-8583-7225e525a17a@gmail.com','ad4c1cca-5e25-4e4c-967b-be686f17dd94','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:20:40','2013-09-25 22:20:40',NULL),
	(33,'f4331d4c-bfb9-4ffc-804c-8183af950766@gmail.com','64b58f2d-120e-4185-88cf-584e6fcb115e','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:20:40','2013-09-25 22:20:40',NULL),
	(34,'2d41f6bb-939f-4a2d-a611-2836a2bce6a4@gmail.com','bd369898-70cf-4ca7-aa05-ace909852bb2','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:22:14','2013-09-25 22:22:14',NULL),
	(35,'2775e625-6935-4ab3-a9f9-ce4ab3a78129@gmail.com','761f13a3-560c-4d28-ae79-f10dc47b9f5f','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:22:14','2013-09-25 22:22:14',NULL),
	(36,'f3e4dad9-378e-478d-956a-a17ccd695a5d@gmail.com','e93cf1e8-5432-44da-a4c7-b1db87b733e5','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:22:20','2013-09-25 22:22:20',NULL),
	(37,'c1c08099-2a30-4095-a262-b30572cace40@gmail.com','b53dee2b-9add-43ed-8e22-8c3e016905f2','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:22:20','2013-09-25 22:22:20',NULL),
	(38,'7c5ea64e-4ae5-4a2e-985a-001820a9a4e0@gmail.com','b692d3d3-71e6-42c8-97fb-7a1fdcd1d24f','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:22:45','2013-09-25 22:22:45',NULL),
	(39,'7e94225f-3cec-4da9-b1e6-826feec86aef@gmail.com','d13adedb-7ff5-4cfa-a7cc-271aa4bcf576','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:22:45','2013-09-25 22:22:45',NULL),
	(40,'2495e6fb-943c-4965-9884-2d42cf24d65d@gmail.com','0c1cfaba-5a05-4759-b99a-ed00f626fea5','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:22:53','2013-09-25 22:22:53',NULL),
	(41,'ab9ff4de-6e2d-4729-865e-3e68bf8ffc3d@gmail.com','1a9305f4-b7bf-4c17-8086-a772fe0aa884','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:22:53','2013-09-25 22:22:53',NULL),
	(42,'790d0659-dae7-4431-9be7-2cac220cdbd1@gmail.com','681dcea5-e0a9-4627-953a-dbf9429df165','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:26:35','2013-09-25 22:26:35',NULL),
	(43,'78335990-8d8a-405a-8c3b-61db01c35c1f@gmail.com','35475ed5-d4d4-44c5-b726-7faefcdaeaa4','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:26:36','2013-09-25 22:26:36',NULL),
	(44,'0c53330b-010a-4b5b-9810-08582526a0ad@gmail.com','68933a0e-dd47-482b-bea4-5c9ad858891b','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:26:41','2013-09-25 22:26:41',NULL),
	(45,'526ce5f8-2fee-4e26-a26b-b7239bfbc675@gmail.com','2053f8ee-8154-4893-ba8d-fbb4522fd47b','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:26:41','2013-09-25 22:26:41',NULL),
	(46,'33e4c394-bfcb-46f2-b537-617a3f106cf3','f296ee43-ee11-4601-bc85-83a837b83e42@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:28:33','2013-09-25 22:28:33',NULL),
	(47,'427027fa-01bd-4cfe-ad5f-cd9833126850','90ba39ec-865b-4316-972c-f0f0837782f1@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:28:33','2013-09-25 22:28:33',NULL),
	(48,'755a66fd-5d1e-4876-8f28-9d894c6e4d70','1f40b4cf-aca4-490e-894a-27341084391e@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:28:44','2013-09-25 22:28:44',NULL),
	(49,'7d7231a3-bc29-47ee-8e60-f46239afa90f','7ee5dc79-2b95-493a-9b71-00d14ed636af@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:28:44','2013-09-25 22:28:44',NULL),
	(50,'154efcc4-0334-4e45-a6fc-f4c86a14cf67','7fba33cb-2ba4-4547-a669-fe233ae146ec@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:29:22','2013-09-25 22:29:22',NULL),
	(51,'5fc9f107-633a-4162-b2ca-8d218cd68dab','3d55f3dc-205e-4e39-8283-6df572f8f676@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-25 22:29:22','2013-09-25 22:29:22',NULL),
	(52,'860cd8b3-2f82-4193-bb39-4c4c2dc8e080','bb37c515-b43c-4583-b75f-9d9adb846afc@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:25:31','2013-09-26 21:25:31',NULL),
	(53,'fe2bc2da-0c75-4bee-94ab-44e909f48fa7','f2cccb22-a3ed-4248-b4ca-8c3464277fd8@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:25:32','2013-09-26 21:25:32',NULL),
	(54,'3e570926-cc5f-45ad-8ed0-034458672076','70a4aad0-2d63-4125-b0ac-c10726bd47aa@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:25:46','2013-09-26 21:25:46',NULL),
	(55,'85ee4da1-f8eb-42f4-9c76-48fe5c4f55a5','0e60dde4-48ea-48ae-a7e5-177219387149@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:25:46','2013-09-26 21:25:46',NULL),
	(56,'a96b8d23-e4c8-482c-aa6e-f6cb5eccdf40','713eb2f9-1f0b-4014-98ce-0363cff3d1f5@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:26:27','2013-09-26 21:26:27',NULL),
	(57,'de8e3466-8f53-4ccd-b00d-d4ba81437b2d','17034d6f-e472-43db-b8d2-d8a2eaae8e1e@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:26:27','2013-09-26 21:26:27',NULL),
	(58,'ae7345af-aa11-4521-8e49-8675c7d33e29','33a9db30-d10a-4bea-aa69-b93b66d3c018@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:27:02','2013-09-26 21:27:02',NULL),
	(59,'466e1dfe-d4e3-4373-afe0-1007501d0f81','8e99d086-90f0-473a-ab6b-7a7a03adcc0c@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:27:02','2013-09-26 21:27:02',NULL),
	(60,'76f32c85-6800-4075-b02c-15fcbf1f3348','1d222d51-604f-4678-a13f-427afeda394f@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:28:37','2013-09-26 21:28:37',NULL),
	(61,'bdcce061-78e5-4185-b7bf-726369830832','ad543e99-f099-4b7e-a48d-103eae1cb954@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:28:37','2013-09-26 21:28:37',NULL),
	(62,'d015fd9c-5e05-4cbe-8240-274bf4c5ca65','66694975-c8f1-4129-8fad-91132c5c1ea3@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:28:41','2013-09-26 21:28:41',NULL),
	(63,'b94e82c3-342a-400d-bfd1-c10d83d9a65c','a46ef7c1-da57-48e6-8ab2-661dcf7274f8@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:28:41','2013-09-26 21:28:41',NULL),
	(64,'2b61f882-8fcd-4095-ba04-d680d3c7f0ec','d31860ec-fca4-4dd2-948c-c7a8d5f39303@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:28:52','2013-09-26 21:28:52',NULL),
	(65,'396f4912-7501-4827-87dd-0602d625c8e1','23c370ec-997f-46d9-a53f-2cc58bcc776a@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:28:52','2013-09-26 21:28:52',NULL),
	(66,'b1a107fe-4edf-45b5-9e1e-c4d7da742c32','a4d916c9-22af-47e1-943a-9e474eada2c4@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:28:55','2013-09-26 21:28:55',NULL),
	(67,'bbdc2fe0-39d3-42e0-b660-3b20925f6d39','8179b2b8-17af-4d84-a77f-c76034583e16@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:28:55','2013-09-26 21:28:55',NULL),
	(68,'559a6bb3-eabe-45e6-a94b-f1283c89a165','c689e261-45f3-43d9-8e72-7b6cf7f81544@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:30:22','2013-09-26 21:30:22',NULL),
	(69,'b84db11e-16bd-48aa-9efd-69e531db70f2','40654555-160b-4c1d-8f8e-749a3d0328fd@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:30:22','2013-09-26 21:30:22',NULL),
	(70,'575fd0d1-acc6-4483-ad3a-448767ce30bb','11f81669-29f1-4d26-bbf2-d1ca53f52d04@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:30:24','2013-09-26 21:30:24',NULL),
	(71,'4706008c-8b97-4e23-84b0-9e6f4a996b17','98e49b64-3f0a-4538-a811-8a201e9164a4@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:30:24','2013-09-26 21:30:24',NULL),
	(72,'d9e30c80-390e-494b-9e0f-338301f93f9f','7e1fa30b-4d25-43e1-957e-13879ddb2d47@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:36:20','2013-09-26 21:36:20',NULL),
	(73,'787f47d4-70cb-4e79-9e30-86649742484a','b15d73e6-63fa-487e-a70d-c766c2786ed8@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:36:20','2013-09-26 21:36:20',NULL),
	(74,'98ddc7ea-96c1-4944-bf6c-f97474951231','0bcec28b-b0be-469d-bf8b-877078044aa6@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:36:23','2013-09-26 21:36:23',NULL),
	(75,'f957b26e-1f72-42ae-9536-ce81e4bd7cd7','3ee72ab5-2343-47c9-a5f1-f07c86e9ff02@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:36:23','2013-09-26 21:36:23',NULL),
	(76,'92578008-c16e-4d16-8fe0-18e91fd0ef7d','718673d3-9212-4d4e-9e53-76ca8c9f81dc@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:36:33','2013-09-26 21:36:33',NULL),
	(77,'ea4057ee-64c1-494c-a2e7-3159f057e73f','655921e7-a5af-4cbc-9c0a-380967fb7b18@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:36:33','2013-09-26 21:36:33',NULL),
	(78,'f6c0c141-a370-4f3a-b6fa-c95e9c3c837e','1a269ad1-2da0-44a4-9e5c-8b0fd3cc6d3b@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:36:36','2013-09-26 21:36:36',NULL),
	(79,'8163cff2-3845-4686-8087-b61def225e1e','2a48009c-a19b-4dc2-b6ca-32c6cba6cc31@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:36:36','2013-09-26 21:36:36',NULL),
	(80,'1303479f-a2ea-4f4d-95d9-4efd64d4b127','3d93d5cb-0a4c-475f-8a7b-0458c99f6105@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:40:45','2013-09-26 21:40:45',NULL),
	(81,'392c6f59-07e3-4845-8304-6d7e3c323f20','edba8270-217e-416b-b02c-c7abbfa27d32@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:40:45','2013-09-26 21:40:45',NULL),
	(82,'d1d5dad7-8a7d-44f3-917d-fcc6184ed7e0','04dee298-8038-4521-86e6-7698fd47d6c9@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:41:07','2013-09-26 21:41:07',NULL),
	(83,'a4559b1c-316d-4efd-9ee4-adcbaeaae611','de5a3ca0-3ca0-4ad9-a3c3-3a6ee8795ff0@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:41:07','2013-09-26 21:41:07',NULL),
	(84,'2a25e44c-3edd-48f6-8544-0b9046e14fc6','d586d1f0-5bef-455a-a489-ad2a039272ef@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:42:20','2013-09-26 21:42:20',NULL),
	(85,'0e5fe9a8-54f1-4e6e-b11c-913044ea70c1','0af32fcf-7381-463e-8d2e-704b1fd5bcb6@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:42:20','2013-09-26 21:42:20',NULL),
	(86,'e0bda5f2-66aa-402d-b799-c105c24fd049','c25e7873-a45b-4c40-9fef-f284007ca233@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:42:58','2013-09-26 21:42:58',NULL),
	(87,'2dc2a0a4-a417-4aa3-b22c-69fbca51a9d7','fac222f6-e7ff-4cb2-8a14-9ff6ba19c629@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:42:58','2013-09-26 21:42:58',NULL),
	(88,'44b2a3aa-1678-4cae-91b6-c2a4b0fa1c24','6bade8e0-3001-4ea1-9f19-bacca25248ed@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:43:12','2013-09-26 21:43:12',NULL),
	(89,'ca725998-cea8-484c-b65c-9935d71ab13e','f4687ba8-4e6f-4059-b1be-06e03184aed5@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 21:43:12','2013-09-26 21:43:12',NULL),
	(90,'2c7106c0-c9aa-4a4b-ada8-1f561901c566','46a49717-5723-44c0-8be0-5d44628f7931@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:07:25','2013-09-26 22:07:25',NULL),
	(91,'5e0b3add-d374-4144-a136-a87594e252b5','cfeb8610-6cd3-48d4-bcf8-4b03613c0bf6@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:07:25','2013-09-26 22:07:25',NULL),
	(92,'5c7340d3-25cb-448d-954d-7bd884527ac9','c39b2ffb-af4f-4e4f-aa92-6fbaa312fe1f@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:09:20','2013-09-26 22:09:20',NULL),
	(93,'9419f4bf-3180-46f2-b585-3b1f53b7794f','7244c4e0-3f96-4c6a-a634-1e0a566768a0@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:09:20','2013-09-26 22:09:20',NULL),
	(94,'3ac382ac-a02b-4820-90a7-002f97c7da86','20cbf351-3294-4f2e-9ad4-bb9d266d6eeb@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:09:25','2013-09-26 22:09:25',NULL),
	(95,'31124b6c-9c25-4c13-bcf5-83cd374f3aba','8ece1682-9dad-4fd7-849a-0f34af3afe1e@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:09:25','2013-09-26 22:09:25',NULL),
	(96,'d660c585-260c-45e0-be8f-a16ac2b6a368','6ca568b0-56b9-4438-86cf-e22bd3d9cf2b@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:11:04','2013-09-26 22:11:04',NULL),
	(97,'d4856b3b-0087-4b25-a554-d083825792b1','a887e9c1-8005-4de9-809b-047333bdd270@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:11:04','2013-09-26 22:11:04',NULL),
	(98,'b715ccd0-3a25-407c-8de1-aa8e591e3dc6','6c55c9e6-fad4-4c49-b3f6-9e35aef43c93@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:11:06','2013-09-26 22:11:06',NULL),
	(99,'7686560c-8a8a-409d-b49c-ea63376ef41b','c13da55b-be52-4b28-b1f3-556f0a946a52@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:11:06','2013-09-26 22:11:06',NULL),
	(100,'e5be740d-c96b-4d10-a1a0-1a8c2383c944','06ea2dd0-d26a-4506-9a2a-9fd50b46548a@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:11:33','2013-09-26 22:11:33',NULL),
	(101,'75694ec3-c9a3-4a11-990f-e1a600354011','11dcf95a-5444-4cc3-8def-e1f2e76f2c07@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:11:33','2013-09-26 22:11:33',NULL),
	(102,'f01c9948-f5e1-4705-ae4d-1f169ef39b59','f90c038d-c830-45df-abed-b041870c3075@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:11:35','2013-09-26 22:11:35',NULL),
	(103,'8150fa5f-c62f-476a-9e08-fa08cb1bc120','5ce2b4e3-8982-499c-b4b5-ec49da160d01@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:11:35','2013-09-26 22:11:35',NULL),
	(104,'81de5a6b-10f7-4de7-a3f7-6d705bebcb8c','e475a8ff-7bc2-4093-889b-922f526e2720@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:13:21','2013-09-26 22:13:21',NULL),
	(105,'7908ce5e-aa66-4051-b9ee-42e9500d1044','3a863e76-7522-4410-8824-192ccbd7b0d3@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:13:21','2013-09-26 22:13:21',NULL),
	(106,'4f1d4204-bda9-4d0a-b030-3ec92cb7ccbe','39b12d04-298d-432b-9161-6ff12a6bb058@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:14:40','2013-09-26 22:14:40',NULL),
	(107,'8535b029-4963-446d-a04b-89db5ac18240','8f5939c8-93c4-4c0a-94ba-05498fe5cc47@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:14:40','2013-09-26 22:14:40',NULL),
	(108,'46d2b72d-ced8-4afc-98e9-dbe69cd30b10','917d9657-aa58-47bd-872f-fac9a7062eca@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:16:16','2013-09-26 22:16:16',NULL),
	(109,'458cbb73-e483-4847-9368-7a160cd30cfc','f8d04e26-0f04-499d-bed5-3f320a990331@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:16:16','2013-09-26 22:16:16',NULL),
	(110,'8ad525f7-dce5-434e-a260-daf58d6b3eed','def93dab-3abc-4f37-a85a-34acff8c2976@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:18:35','2013-09-26 22:18:35',NULL),
	(111,'21f30c3a-a3e4-48e7-9022-273cbecd0c99','b95247dc-bfe3-4aed-accd-035bbe19ab2b@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:18:35','2013-09-26 22:18:35',NULL),
	(112,'88237acf-9c3a-426a-8852-fa1a4cdff5c9','ecbeabe8-4623-4c67-85d3-4c5c0c9e1c5a@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:19:36','2013-09-26 22:19:36',NULL),
	(113,'9263a240-6ecf-457d-88d6-6dd2ed4cf28c','51b346f8-14c0-4f33-aece-3a4ff8984851@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:19:37','2013-09-26 22:19:37',NULL),
	(114,'29dfe387-9d92-4ca5-9ff8-e0a9d6f2a75e','9f125a28-7d26-49fc-b099-9203da11189d@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:19:48','2013-09-26 22:19:48',NULL),
	(115,'377b26f3-66a3-498f-ae0b-7b1650dd7789','3a79b91b-b963-4701-9bb7-9bd6fc1cfd67@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:19:48','2013-09-26 22:19:48',NULL),
	(116,'0c8d0eed-ae3a-42c1-bca6-d243b07dca96','54b2a1ed-b3db-4429-b935-097f919be0b3@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:20:05','2013-09-26 22:20:05',NULL),
	(117,'f9b2b7a4-19dd-42f0-a41a-c7e098202981','4bddb680-d5a1-4c5b-ab87-a30158acf13b@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:20:05','2013-09-26 22:20:05',NULL),
	(118,'dd5a9095-1fd2-415f-a20a-33baac3b9ccc','132949da-e21b-4619-b8f1-b24571771708@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:46:53','2013-09-26 22:46:53',NULL),
	(119,'3d19a7e4-5bdd-4456-ba6c-6708e8acff36','8a752dc0-39ac-4fcf-a42c-c726d4d76721@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:46:53','2013-09-26 22:46:53',NULL),
	(120,'0a02eb8b-b9a2-4acd-9e19-451230f7bde7','b3198347-6203-4686-affe-1e9668ceef6e@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:53:35','2013-09-26 22:53:35',NULL),
	(121,'55e5fd3c-8b0d-4d4d-a979-43fa51d4a26c','1cb22aa6-a231-4347-8fda-38539bf028e4@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:53:35','2013-09-26 22:53:35',NULL),
	(122,'93b69aad-621c-4982-baeb-0744d33fe5a0','8e57d26b-cefe-4321-8e35-f620ee248d07@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:53:54','2013-09-26 22:53:54',NULL),
	(123,'5637167f-5275-477f-a90c-a5aa5e6038fe','69c9c6c5-5845-4507-b86c-3629d1a84ccc@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:53:54','2013-09-26 22:53:54',NULL),
	(124,'7f722477-e2ae-4a09-bdbf-0328b4e853f8','fe46ba1e-fda3-4e6d-8649-b36cd408282f@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:55:57','2013-09-26 22:55:57',NULL),
	(125,'99b4648b-088d-4847-8fa3-ae30bb652e77','e691c450-9bc8-4868-a265-66e8698d51e4@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:55:57','2013-09-26 22:55:57',NULL),
	(126,'0ee4a589-4fd8-4044-8a8e-ec19da4da43c','e0082e10-5927-41a2-8990-e2155560b6ac@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:56:39','2013-09-26 22:56:39',NULL),
	(127,'fa63b6f7-6414-495e-b335-402309e55e34','dcb171e5-21e2-4124-8184-32a5d2de2a38@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:56:39','2013-09-26 22:56:39',NULL),
	(128,'c2566f08-9f2c-43e8-9543-43c8a195dcf8','2b833b2f-65a2-4bc1-8aff-58b3c3cb813c@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:57:09','2013-09-26 22:57:09',NULL),
	(129,'22957c21-253f-4217-99c4-4b92bed54972','3f8a4b01-b0df-440a-89f3-6d3d39f5f4c6@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:57:09','2013-09-26 22:57:09',NULL),
	(130,'a0009c44-02fd-4a10-b673-6e5bd1de7252','e0c55e19-40c5-4823-9480-e06e1f0ebe25@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:58:50','2013-09-26 22:58:50',NULL),
	(131,'997cf94c-3ffb-4950-8a82-ff4538e9ef10','6fdb12a0-f641-4b3a-b069-6bf8960a4ed6@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 22:58:51','2013-09-26 22:58:51',NULL),
	(132,'60ed933e-a044-4ada-a6bc-f9151365fd59','5f6fb471-17c4-4119-8997-5e3b1fbac6d1@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 23:04:52','2013-09-26 23:04:52',NULL),
	(133,'f0c9ae07-88da-4c14-a25c-a951a50efd7a','27c5345e-0398-4648-bc6a-62135c5243fc@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 23:04:52','2013-09-26 23:04:52',NULL),
	(134,'7ce88ebf-b8fc-47df-864a-5185c310ac22','a0542694-875b-4ec3-a063-c26d57730e0c@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 23:05:32','2013-09-26 23:05:32',NULL),
	(135,'895d4ec3-6f9b-4e03-9ffa-b1a3d7dc792f','407eb7df-00ad-49f5-ba91-afa404a5bafd@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 23:05:32','2013-09-26 23:05:32',NULL),
	(136,'8d5a9bf5-63b0-4668-a457-ed65c4d2a3ee','b4e2cf84-a562-4fdd-b63f-8188a8198442@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 23:06:15','2013-09-26 23:06:15',NULL),
	(137,'9b587ddb-92de-4d18-bbd8-6097a9f39189','abe00378-6c1a-493d-afd3-b18b20bc55fc@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 23:06:15','2013-09-26 23:06:15',NULL),
	(138,'4686fe9a-de6c-44a6-8dce-29ba60ad8705','99dcccdc-b05f-4f8a-959e-052ba2075f3a@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 23:06:23','2013-09-26 23:06:23',NULL),
	(139,'2e8e7c0e-1d09-41b0-8f36-56a680a7b31d','d652f047-b260-4236-b492-da1ddf9e4e90@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 23:06:23','2013-09-26 23:06:23',NULL),
	(140,'590c03fa-38fa-4ab7-afb3-117d74e622f9','2940a102-ce85-401c-8319-14ad800e944a@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 23:06:42','2013-09-26 23:06:42',NULL),
	(141,'ede7b7d5-9d1f-44e2-8120-322d8dd1de22','f14d8380-b29b-4a55-80c2-22716ca513ad@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 23:06:42','2013-09-26 23:06:42',NULL),
	(142,'3e91d7a7-3d5f-442c-94fc-0f8f9ea874a9','a5104a5b-79a8-4a14-94b2-bc3fefdd2516@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 23:08:05','2013-09-26 23:08:05',NULL),
	(143,'908bbd38-1820-4d3e-83e8-c3c45f125baf','6de720c8-62d9-405c-9945-b2b3437d8c88@gmail.com','5f4dcc3b5aa765d61d8327deb882cf99','2013-09-26 23:08:05','2013-09-26 23:08:05',NULL);

/*!40000 ALTER TABLE `kindergarten` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table log
# ------------------------------------------------------------

DROP TABLE IF EXISTS `log`;

CREATE TABLE `log` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `channel` varchar(255) DEFAULT NULL,
  `level` int(11) DEFAULT NULL,
  `levelname` varchar(9) DEFAULT NULL,
  `message` text,
  `data` longtext,
  `file` varchar(500) DEFAULT NULL,
  `host` varchar(50) DEFAULT NULL,
  `port` varchar(4) DEFAULT NULL,
  `time` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table parent
# ------------------------------------------------------------

DROP TABLE IF EXISTS `parent`;

CREATE TABLE `parent` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `external_id` varchar(100) NOT NULL,
  `email` varchar(100) NOT NULL,
  `password_id` int(11) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `photo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `parent` WRITE;
/*!40000 ALTER TABLE `parent` DISABLE KEYS */;

INSERT INTO `parent` (`id`, `name`, `external_id`, `email`, `password_id`, `created_at`, `updated_at`, `photo_id`)
VALUES
	(1,'Max Balychev','max-tester-01','max-tester@parentmgr.com',0,'0000-00-00 00:00:00','0000-00-00 00:00:00',NULL);

/*!40000 ALTER TABLE `parent` ENABLE KEYS */;
UNLOCK TABLES;


# Dump of table password
# ------------------------------------------------------------

DROP TABLE IF EXISTS `password`;

CREATE TABLE `password` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `value` varchar(250) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Dump of table teacher
# ------------------------------------------------------------

DROP TABLE IF EXISTS `teacher`;

CREATE TABLE `teacher` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `kindergarten_id` int(11) NOT NULL,
  `name` varchar(100) NOT NULL,
  `external_id` varchar(100) NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT '0000-00-00 00:00:00',
  `photo_id` int(11) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `teacher` WRITE;
/*!40000 ALTER TABLE `teacher` DISABLE KEYS */;

INSERT INTO `teacher` (`id`, `kindergarten_id`, `name`, `external_id`, `created_at`, `updated_at`, `photo_id`)
VALUES
	(1,1,'Teacher','test_teacher_01','2013-09-26 20:17:15','0000-00-00 00:00:00',NULL);

/*!40000 ALTER TABLE `teacher` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
